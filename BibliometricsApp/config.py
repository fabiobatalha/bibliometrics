#!/usr/bin/env python

import os

#HOSTNAME
HOSTNAME = os.environ.get('HOSTNAME', "http://fractalnet-works.ddns.net:5000")

#Cache folder path+
CACHE_FOLDER_PATH = "/app/BibliometricsApp/app/cache/"

# Statement for enabling the development environment
DEBUG = os.environ.get('DEBUG', False)

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = os.environ.get('THREADS_PER_PAGE', 2)

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED     = os.environ.get('CSRF_ENABLED', True)

# Use a secure, unique and absolutely secret key for
# signing the data.
CSRF_SESSION_KEY = os.environ.get('CSRF_SESSION_KEY', "secret")

# Secret key for signing cookies
SECRET_KEY = os.environ.get('SECRET_KEY', "secret")

# Articlemeta Server
SERVER = os.environ.get('SERVER', "fractalnet-works.ddns.net")

# Articlemeta Ports
ACCESSSTATS = os.environ.get('ACCESSSTATS', 11660)

PUBLICATIONSTATS = os.environ.get('PUBLICATIONSTATS', 11620)

ARTICLEMETA = os.environ.get('ARTICLEMETA', 11720)

CITEDBY = os.environ.get('CITEDBY', 11610)