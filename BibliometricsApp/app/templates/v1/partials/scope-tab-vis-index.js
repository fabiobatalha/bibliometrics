<script type="text/javascript">
  var JSON_CURRENT_FILTERS = {{ filters.serialize|tojson }};
  var LANG = "{{ filters.lang|safe }}";

  $('.scope-tabulation-fe').html(stFilters["{{filters.scope}}"]["text"][LANG]+" "+stFilters["{{filters.scope}}"]["tabulations"]["{{filters.tabulation}}"]["text"][LANG]);
  /*FIRST BLOCK FILTERS*/
  //Scope
  scope();
  //Tabulations
  scope_tabulations();
  //Visualizations
  scope_tabulations_visualizations();

  //FUNCTIONS
  function scope(){
    $('#scope-select').find('option').remove().end();
    $('#scope-select').append($('<option>', {text: "{{ gettext('Select scope') }}", disabled: true, selected: true }));
    for(var scope in stFilters){
      $('#scope-select').append($('<option>', {value:scope, text: stFilters[scope]["text"][LANG]}));
    }
    //Change form action
    scope = $('#scope-select').val();
    (stFilters[scope]) ? $('#filters-form').attr('action', stFilters[scope]["endpoint"]) : $('#filters-form').attr('action', "/v1/");
  	enable_disable_visualize_btn();
  	$('select').material_select();
  }

  function scope_tabulations(){
    var scope = $('#scope-select').val();
		$('#scope-tabulations-select').prop('disabled', true).find('option').remove().end();
		$('#scope-tabulations-select').append($('<option>', {text: "{{ gettext('Select tabulation') }}", disabled: true, selected: true }));
    if(scope){
    	$('#scope-tabulations-select').prop('disabled', false);
	    for(tabulation in stFilters[scope]["tabulations"]){
	      $('#scope-tabulations-select').append($('<option>', {value:tabulation, text: stFilters[scope]["tabulations"][tabulation]["text"][LANG]}))
	    }
    }
    enable_disable_visualize_btn();
    $('select').material_select();
  }

  function scope_tabulations_visualizations(){
    var scope = $('#scope-select').val();
    var tabulation = $('#scope-tabulations-select').val();
    $('#scope-tabulation-visualizations-select').prop('disabled', true).find('option').remove().end();
		$('#scope-tabulation-visualizations-select').append($('<option>', {text: "{{ gettext('Select visualization') }}", disabled: true, selected: true }));
    if(scope && tabulation){
    	$('#scope-tabulation-visualizations-select').prop('disabled', false);
	    for(visualization in stFilters[scope]["tabulations"][tabulation]["visualizations"][LANG]){
	      $('#scope-tabulation-visualizations-select').append($('<option>', {value: stFilters[scope]["tabulations"][tabulation]["visualizations"][LANG][visualization][0], text: stFilters[scope]["tabulations"][tabulation]["visualizations"][LANG][visualization][1]}))
	    }    	
    }
    enable_disable_visualize_btn();
    $('select').material_select();
  }

  //CHANGE SCOPE
  $('#scope-select').on('change', function(){
    var scope = $('#scope-select').val();
    //Change form action
    $('#filters-form').attr('action', stFilters[scope]["endpoint"]);
    $('#filters-form-url').val(stFilters[scope]["endpoint"]);
    //Update selects
    scope_tabulations();
    scope_tabulations_visualizations();
    enable_disable_visualize_btn();
  });

  //CHANGE TABULATION
  $('#scope-tabulations-select').on('change', function(){
    var scope = $('#scope-select').val();
    var tabulation = $('#scope-tabulations-select').val();
    //Update selects
    scope_tabulations_visualizations();
    enable_disable_visualize_btn();
  });

  //CHANGE VISUALIZATION
  $("#scope-tabulation-visualizations-select").on('change', function(){
    enable_disable_visualize_btn();
  });

  function enable_disable_visualize_btn(){
    var scope = $('#scope-select').val();
    var tabulation = $('#scope-tabulations-select').val();
    var visualization = $('#scope-tabulation-visualizations-select').val();
  	if(scope && tabulation && visualization){
  		$("#visulize-btn").removeClass("disabled").attr('disabled', false);
  	}else{
  		$("#visulize-btn").addClass("disabled").attr('disabled', true);
  	}
  }
  /*END FIRST BLOCK FILTERS*/
</script>