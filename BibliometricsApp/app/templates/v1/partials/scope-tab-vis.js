<script type="text/javascript">
  var JSON_CURRENT_FILTERS = {{ filters.serialize|tojson }};
  var LANG = "{{ filters.lang|safe }}";

  $('.scope-tabulation-fe').html(stFilters["{{filters.scope}}"]["text"][LANG]+" "+stFilters["{{filters.scope}}"]["tabulations"]["{{filters.tabulation}}"]["text"][LANG]);
  /*FIRST BLOCK FILTERS*/
  //Scope
  scope();
  //Tabulations
  scope_tabulations();
  //Visualizations
  scope_tabulations_visualizations();

  //FUNCTIONS
  function scope(){
    $('#scope-select').find('option').remove().end();
    $('#scope-select').append($('<option>', {text: "{{ gettext('Select scope') }}", disabled: true, selected: true }));
    for(var scope in stFilters){
      $('#scope-select').append($('<option>', {value:scope, text: stFilters[scope]["text"][LANG]}));
    }
    //Change form action
    scope = $('#scope-select').val();
    (stFilters[scope]) ? $('#filters-form').attr('action', stFilters[scope]["endpoint"]) : $('#filters-form').attr('action', "/v1/");
  	$('select').material_select();
  }

  function scope_tabulations(){
    var scope = $('#scope-select').val();
		$('#scope-tabulations-select').prop('disabled', true).find('option').remove().end();
		$('#scope-tabulations-select').append($('<option>', {text: "{{ gettext('Select tabulation') }}", disabled: true, selected: true }));
    if(scope){
    	$('#scope-tabulations-select').prop('disabled', false);
	    for(tabulation in stFilters[scope]["tabulations"]){
	      $('#scope-tabulations-select').append($('<option>', {value:tabulation, text: stFilters[scope]["tabulations"][tabulation]["text"][LANG]}))
	    }
    }
    $('select').material_select();
  }

  function scope_tabulations_visualizations(){
    var scope = $('#scope-select').val();
    var tabulation = $('#scope-tabulations-select').val();
    $('#scope-tabulation-visualizations-select').prop('disabled', true).find('option').remove().end();
		$('#scope-tabulation-visualizations-select').append($('<option>', {text: "{{ gettext('Select visualization') }}", disabled: true, selected: true }));
    if(scope && tabulation){
    	$('#scope-tabulation-visualizations-select').prop('disabled', false);
	    for(visualization in stFilters[scope]["tabulations"][tabulation]["visualizations"][LANG]){
	      $('#scope-tabulation-visualizations-select').append($('<option>', {value: stFilters[scope]["tabulations"][tabulation]["visualizations"][LANG][visualization][0], text: stFilters[scope]["tabulations"][tabulation]["visualizations"][LANG][visualization][1]}))
	    }    	
    }
    $('select').material_select();
  }

  //CHANGE SCOPE
  $('#scope-select').on('change', function(){
    var scope = $('#scope-select').val();
    //Change form action
    $('#filters-form').attr('action', stFilters[scope]["endpoint"]);
    $('#filters-form-url').val(stFilters[scope]["endpoint"]);
    //Update selects
    scope_tabulations();
    scope_tabulations_visualizations();
    enable_disable_visualize_btn();
  });

  //CHANGE TABULATION
  $('#scope-tabulations-select').on('change', function(){
    var scope = $('#scope-select').val();
    var tabulation = $('#scope-tabulations-select').val();
    //Update selects
    scope_tabulations_visualizations();
    enable_disable_visualize_btn();
  });

  $("#scope-tabulation-visualizations-select").on('change', function(){
    $('#visualization-hidden').val($(this).val());
    $('#visualization-hidden-aux').val($(this).val());
    $("#vis-label").addClass("parpadea");
    
    $("#animate-btn").attr('onclick', "change_visualization('"+$(this).val()+"', true);");
    save_to_history('#advanced-filters-form-aux');
    
    change_visualization($(this).val(), false);
    
    //Cambiamos div de exportar visualizaciones
    set_exporter_options();
    enable_disable_visualize_btn()
  });

  //Cambio de visualización desde select
  var scope1_containers = {'map': '#world-map',
                           'treemap': '#treemap-by',
                           'linechart': '#linechart-by'}
  var scope2_containers = {'map': '#world-map',
                           'treemap': '#treemap-by',
                           'linechart': '#linechart-by'}
  var scope7_containers = {'circular layout': '#circular-layout',
                           'map': '#world-map',
                           'force-direct layout': '#force-direct-layout'}
  function set_exporter_options(){
    var vistype = $("#scope-tabulation-visualizations-select").val();
    if(vistype == "table"){
      $('.print-export-option').hide();
      $('.image-export-option').hide();
      $('.pdf-export-option').hide();
    }else if("{{filters.scope}}" == "1"){
      var container = scope1_containers[vistype];
      $('.print-export-option').show();
      $('.image-export-option').show();
      $('.pdf-export-option').show();
    }else if("{{filters.scope}}" == "2"){
      var container = scope2_containers[vistype];
      $('.print-export-option').show();
      $('.image-export-option').show();
      $('.pdf-export-option').show();
    }else if("{{filters.scope}}" == "7"){
      var container = scope7_containers[vistype];
      $('.print-export-option').show();
      $('.image-export-option').show();
      $('.pdf-export-option').show();
    }
    $('#eo-png').attr('onclick', "export_visualization('"+ container +" svg', 'png');");
    $('#eo-jpeg').attr('onclick', "export_visualization('"+ container +" svg', 'jpeg');");
    $('#eo-svg').attr('onclick', "export_visualization('"+ container +" svg', 'svg');");
    $('#eo-ppt').attr('onclick', "export_visualization('"+ container +" svg', 'pptx');"); 
    $('#eo-pdf').attr('onclick', "export_visualization('"+ container +" svg', 'pdf');"); 
  }

  function new_btn_action(save){
  	if(save){
  		save_to_user_history('#advanced-filters-form-aux');
  	}
  	scope();
  	scope_tabulations();
  	scope_tabulations_visualizations();
  	
  	$('#scope-select').removeClass("disabled").prop("disabled", false);

  	$("#visualization-block").hide();
  	$("#advanced-filters-form").hide();
  	$("#advanced-filters-form-aux").hide();
  	$("#advanced-filters-form-submit-btn").addClass('disabled').prop('disabled', true);
  	$("#refresh-filters-btn").addClass('disabled').prop('disabled', true);
  	$("#advanced-filters-save-btn").addClass('disabled').prop('disabled', true);
  	$("#advanced-filters-forward-btn").addClass('disabled').prop('disabled', true);
  	$("#advanced-filters-back-btn").hide();
  	$("#advanced-filters-reactivate-display-btn").show();

  	$("#current-visualization-info").hide();
  	$("#fp-ss").removeClass("s2 m2 l2").addClass(" s3 m3 l3");
  	$("#fp-sts").removeClass("s3 m3 l3").addClass(" s5 m5 l5");


  	$('select').material_select();
  }

  function reactivate_display(){
  	$("#visulize-btn").addClass("disabled").prop('disabled', true);
  	$("#visualization-block").show();
  	$("#advanced-filters-form").show();
  	$("#advanced-filters-form-aux").show();
  	$("#advanced-filters-form-submit-btn").removeClass('disabled').prop('disabled', false);
  	$("#refresh-filters-btn").removeClass('disabled').prop('disabled', false);
  	$("#advanced-filters-save-btn").removeClass('disabled').prop('disabled', false);
  	$("#advanced-filters-forward-btn").removeClass('disabled').prop('disabled', false);
  	$("#advanced-filters-back-btn").show();
  	$("#advanced-filters-reactivate-display-btn").hide();

  	$("#fp-ss").removeClass(" s3 m3 l3").addClass("s2 m2 l2");
  	$("#fp-sts").removeClass(" s5 m5 l5").addClass("s3 m3 l3");
  	$("#current-visualization-info").show();

  	initalize_filters();
  }

  function enable_disable_visualize_btn(){
    var scope = $('#scope-select').val();
    var tabulation = $('#scope-tabulations-select').val();
    var visualization = $('#scope-tabulation-visualizations-select').val();
    if(scope && tabulation && visualization){
      $("#visulize-btn").removeClass("disabled").attr('disabled', false);
    }else{
      $("#visulize-btn").addClass("disabled").attr('disabled', true);
    }
  }



  function initalize_filters(){
	  //Scope
	  $('#scope-select').val('{{ filters.scope }}').trigger('change');
	  $('#filters-form').attr('action', stFilters[{{ filters.scope }}]["endpoint"]);
	  $('#filters-form-url').val(stFilters[{{ filters.scope }}]["endpoint"]);
	  $('#advanced-filters-form').attr('action', stFilters[{{ filters.scope }}]["endpoint"]);
	  $('#advanced-filters-form-url').val(stFilters[{{ filters.scope }}]["endpoint"]);
	  $('#advanced-filters-form-url-aux').val(stFilters[{{ filters.scope }}]["endpoint"]);
	  //Tabulation
	  $('#scope-tabulations-select').val('{{ filters.tabulation }}').trigger('change');
	  //visualization
	  $('#scope-tabulation-visualizations-select').val('{{ filters.visualization }}');

	  //Desactivamos selects
	  $('#scope-select').addClass("disabled").prop("disabled", true);
	  $('#scope-tabulations-select').addClass("disabled").prop("disabled", true);

	  $('select').material_select();

    set_exporter_options();
  }
  /*END FIRST BLOCK FILTERS*/
</script>