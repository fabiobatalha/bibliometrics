<script type="text/javascript">
  var JSON_CURRENT_FILTERS = {{ filters.serialize|tojson }};
  var HASH_CHECK = {};
  var LANG = "{{ filters.lang|safe }}";
  var D3PLUS_LANG = "en_US";
  if(LANG == "es"){
    D3PLUS_LANG = "es_ES"
  }else if(LANG == "pt"){
    D3PLUS_LANG = "pt_PT"
  }


  /*ADVANCED FILTERS BLOCK*/
  
  //COLLECTIONS
  collections = filtersJson['collections']
  filters_c = {{ filters.collections|tojson }}.map(function(c){ return c[0]});
  prepare_checkboxes(collections, filters_c, 1, "collection", "coll", LANG);
  
  //JOURNALS
  journals = filtersJson['journals']
  prepare_checkboxes(journals, {{ filters.journals|tojson }}, 0, "journal", "jour", LANG);
  
  //TEMATIC AREAS
  thematic_areas = filtersJson['thematicAreas'];
  filters_ta = {{ filters.thematic_areas|tojson }}.map(function(ta){ return ta[0]});
  prepare_checkboxes(thematic_areas, filters_ta, 1, "thematic-area", "ta", LANG);

  //WOS TEMATIC AREAS
  wos_thematic_areas = filtersJson['wosThematicAreas']
  filters_wta = {{ filters.wos_thematic_areas|tojson }}.map(function(wta){ return wta[0]});
  prepare_checkboxes(wos_thematic_areas, filters_wta, 1, "wos-thematic-area", "wta", LANG);
  
  //
  $("#document-type-select").val('{{ filters.document_type }}')
  //DOCUMENT TYPE DETAILS
  document_types = filtersJson['documentTypes']
  filters_dtd = {{ filters.document_type_details|tojson }}.map(function(dtd){ return dtd[0]});
  prepare_checkboxes(document_types, filters_dtd, 2, "document-type-detail", "dtd", LANG);
  
  //LANGUAGES
  languages = filtersJson['languages']
  filters_l = {{ filters.languages|tojson }}.map(function(l){ return l[0]});
  prepare_checkboxes(languages, filters_l, 1, "language", "lang", LANG);
  

  //YEARS
  current_year = new Date().getFullYear();
  //Mantener concordancia de los años
  $('#year-select-from').on('change', function(){
    activate_filter_deactivate_save_btns();
    if( $('#year-select-to').val() < $(this).val()){
      $('#year-select-to').val($(this).val());
    }
  });
  $('#year-select-to').on('change', function(){
    activate_filter_deactivate_save_btns();
    if( $('#year-select-from').val() > $(this).val()){
      $('#year-select-from').val($(this).val());
    }
  });

  //Journal status

  $('#journal-status').on('change', function(){
    activate_filter_deactivate_save_btns();
  });

  function prepare_checkboxes(data, filters, type, prefix, txt_id, lang){  
    (filters.length == 0) ? checked = true : checked = false;
    var checkbox_all = $('<div>');
    checkbox_all.append($('<input />', { type: 'checkbox',
                                         id: prefix+'-checkbox-all',
                                         class: 'filled-in checkbox-deep-purple',
                                         name: prefix+'[]',
                                         checked: checked, 
                                         value: 'ALL',
                                         'data-label': 'ALL',
                                         onchange: "check_all_filter($(this), '"+prefix+"');"}));
    checkbox_all.append($('<label />', { 'for': prefix+'-checkbox-all', text: "{{ gettext('All') }}", class: "dtd-all-checkbox" }));
    
    if(type == 2){
      //All citable
      checkbox_all.append($('<input />', { type: 'checkbox',
                                           id: prefix+'-checkbox-citable',
                                           class: 'filled-in checkbox-deep-purple',
                                           'data-label': 'All Citable',
                                           onchange: "check_all_dtd_filter($(this), '"+prefix+"', 'Citable');"}));
      checkbox_all.append($('<label />', { 'for': prefix+'-checkbox-citable', text: "{{ gettext('All Citable') }}", class: "dtd-all-checkbox" }));
      //All non citable
      checkbox_all.append($('<input />', { type: 'checkbox',
                                           id: prefix+'-checkbox-non-citable',
                                           class: 'filled-in checkbox-deep-purple',
                                           'data-label': 'All Non Citable',
                                           onchange: "check_all_dtd_filter($(this), '"+prefix+"', 'Non citable');"}));
      checkbox_all.append($('<label />', { 'for': prefix+'-checkbox-non-citable', text: "{{ gettext('All Non Citable') }}", class: "dtd-all-checkbox" }));  
    }
    
    $('#'+prefix+'-checkbox-all-container').append(checkbox_all);

    for(i in data){
      if(type == 0){
        var text = data[i];
        var value = data[i];
        var column2 = '';
      }else if(type == 1){
        var value = data[i][0];
        var text = data[i][1][lang];
        var column2 = '';
      }else{
        var value = data[i][0];
        var text = data[i][1][lang];
        var column2 = data[i][2];
      }
      
      (filters.indexOf(value) > -1 || checked) ? checked_2 = true : checked_2 = false;
      var tr = $('<tr>');
      var td = $('<td>');
      td.append($('<input />', { type: 'checkbox', 
                                 id: prefix+'-'+i, 
                                 class: 'filled-in checkbox-deep-purple '+prefix+'-checkbox', 
                                 name: prefix+'[]', 
                                 value: value, 
                                 checked: checked_2,
                                 'data-label': text,
                                 'data-dtd': column2,
                                 onchange: "filter_checks($(this), '"+prefix+"','"+value+"','"+text+"');" }));
      td.append($('<label />', { 'for': prefix+'-'+i, text: text }));
      tr.append(td);
      if(type == 2){
        var td2 = $('<td>').attr("data-dtd", column2).text(column2);
        tr.append(td2);
      }
      $('#af-'+prefix+'-modal-checkboxes').append(tr);
    }
  }

  function hash_check_reset(prefix){
    HASH_CHECK = {};
    HASH_CHECK[prefix] = [];
  }
  function filter_checks(checkbox, prefix, value, text){
    HASH_CHECK[prefix].push([checkbox, prefix, value, text]);
    ($("."+prefix+"-checkbox:checkbox:not(:checked)").size() == 0) ? $("#"+prefix+"-checkbox-all").prop('checked', true) : $("#"+prefix+"-checkbox-all").prop('checked', false);
    if(prefix == "document-type-detail"){
      ($("[data-dtd='Citable']:checkbox:not(:checked)").size() == 0) ? $("#"+prefix+"-checkbox-citable").prop('checked', true) : $("#"+prefix+"-checkbox-citable").prop('checked', false);
      ($("[data-dtd='Non citable']:checkbox:not(:checked)").size() == 0) ? $("#"+prefix+"-checkbox-non-citable").prop('checked', true) : $("#"+prefix+"-checkbox-non-citable").prop('checked', false);
    }
  }
  function accept_filters(prefix){
    //Verificar si estan todos o ninguno marcado.
    n_checkboxes = $('.'+prefix+'-checkbox').size();
    n_checked = $('.'+prefix+'-checkbox:checkbox:checked').size();
    //n_special_remove = $('#'+prefix+'-to-filter li.special-remove').size();
    if(n_checkboxes == n_checked || n_checked == 0){
      (n_checkboxes == n_checked) ? $('#'+prefix+'-checkbox-all').prop('checked', true) : $('#'+prefix+'-checkbox-all').prop('checked', false);
      $("#"+prefix+"-to-filter li").remove();

      li = $('<li>').attr('data-liid', prefix+"-all").addClass('collection-item-2');
      li.append("{{ gettext('All') }}");
      $('#'+prefix+'-to-filter').append(li);
      //$('#'+prefix+'-to-filter').height();
    }else{
      $('#'+prefix+'-checkbox-all').prop('checked', false);

      $('.'+prefix+'-checkbox').each(function(i, obj){
        if($(this).is(':checked')){
          check_filter($(this), prefix, $(this).val(), $(this).data('label'));
        }else{
          uncheck_filter($(this), prefix, $(this).val(), $(this).data('label'));
        }
      });
    }
    activate_filter_deactivate_save_btns();
    $('.x-filter').remove();
    $('#'+prefix+'-to-filter .collection-item-2').size() > 1 ? $('#'+prefix+'-expand-btn').show() : $('#'+prefix+'-expand-btn').hide()
  }
  function cancel_filters(prefix){
    for(i in HASH_CHECK[prefix]){
      if(HASH_CHECK[prefix][i][0].is(':checked')){
        HASH_CHECK[prefix][i][0].prop('checked',false);
      }else{
        HASH_CHECK[prefix][i][0].prop('checked',true);
      }
    }
  }
  function check_filter(checkbox, prefix, value, text){
    if( $("#"+prefix+"-to-filter li[data-liid='"+value+"']").length == 0){

      $("#"+prefix+"-to-filter li[data-liid='"+prefix+"-all']").remove();

      ($('#'+prefix+'-operation').prop('checked')) ? operator = "{{ gettext('AND') }}" : operator = "{{ gettext('OR') }}";
          
      li = $('<li>').attr('data-liid', value).addClass('collection-item-2');
      li.append("<span class='badge2 s-badge right green'>{{ gettext('selected') }}</span>")
      li.append("<b class='operation-text'>"+operator+"</b> "+text);
      
      $('#'+prefix+'-to-filter').append(li);
    }else{
      li = $("#"+prefix+"-to-filter li[data-liid='"+value+"']")
      li.css('text-decoration', 'none');
      li.children('.r-badge').remove();
      if(li.children('.s-badge').size() == 0){
        li.append("<span class='badge2 s-badge right green'>{{ gettext('selected') }}</span>")
      }
    }
    count_badges(prefix);    
    activate_filter_deactivate_save_btns();
  }
  function uncheck_filter(checkbox, prefix, value, text){
    r_li = $("li[data-liid='"+value+"']");
    if(r_li.hasClass('special-remove')){
      r_li.children('.s-badge').remove();
      if(r_li.children('.r-badge').size() == 0){
        r_li.css('text-decoration', 'line-through');
        r_li.children('input').remove();
        r_li.append("<span class='badge2 r-badge right red'>{{ gettext('removed') }}</span>");
      }
    }else{
      r_li.remove();
    }
    count_badges(prefix);
    activate_filter_deactivate_save_btns();
  }

  function check_all_filter(checkbox, prefix){
    checked = $(checkbox).prop('checked');
    $("."+prefix+"-checkbox").each(function(i, obj){
      if($(this).prop('checked') != checked){
        $(this).prop('checked', checked);
        filter_checks($(this), prefix, $(this).val(), $(this).data('label'));        
      }
    });
  }

  function check_all_dtd_filter(checkbox, prefix, type){
    checked = $(checkbox).prop('checked');
    //Uncheck checkbox contrario
    $("."+prefix+"-checkbox-all").prop('checked', false);
    (type == 'Citable') ? ncc = $("#"+prefix+"-checkbox-non-citable") : ncc = $("#"+prefix+"-checkbox-citable");
    if(ncc.prop('checked') != false){
      ncc.prop('checked', false);
      //filter_checks(ncc, prefix, ncc.val(), ncc.data('label'));
    }
    //Check por tipo
    $("."+prefix+"-checkbox").each(function(i, obj){
      if($(this).data('dtd') == type){
        if($(this).prop('checked') != checked){
          $(this).prop('checked', checked);
          filter_checks($(this), prefix, $(this).val(), $(this).data('label'));        
        }
      }else{
        if($(this).prop('checked') != false){
          $(this).prop('checked', false);
          filter_checks($(this), prefix, $(this).val(), $(this).data('label'));        
        }
      }
    });
  }

  function current_filter_uncheck(input_name, value){
    checkbox = $("#advanced-filters-form input[name='"+input_name+"'][value='"+value+"']");
    checkbox.prop('checked', false);
    $("#advanced-filters-form").submit();
  }

  function operation_change(checkbox, container_id){
    (checkbox.prop('checked')) ? operator = "{{ gettext('AND') }}" : operator = "{{ gettext('OR') }}";
    $(container_id).find(".operation-text").html(operator);
    activate_filter_deactivate_save_btns();
  }

  function count_badges(prefix){
    var s_badges = $('#'+prefix+'-to-filter .s-badge').size();
    s_badges > 0 ? $('#'+prefix+'-total-s-badges').text(s_badges+" {{ gettext('selected') }}").show() : $('#'+prefix+'-total-s-badges').hide();
    
    var r_badges = $('#'+prefix+'-to-filter .r-badge').size();
    r_badges > 0 ? $('#'+prefix+'-total-r-badges').text(r_badges+" {{ gettext('removed') }}").show() : $('#'+prefix+'-total-r-badges').hide();
  }
  function reset_filters(){
    $('.collection-checkbox').prop('checked', true);
    accept_filters('collection');
    $('.journal-checkbox').prop('checked', true);
    accept_filters('journal');
    $('.thematic-area-checkbox').prop('checked', true);
    accept_filters('thematic-area');
    $('.wos-thematic-area-checkbox').prop('checked', true);
    accept_filters('wos-thematic-area');
    $('.language-checkbox').prop('checked', true);
    accept_filters('language');
    $('.document-type-detail-checkbox').prop('checked', true);
    accept_filters('document-type-detail');
    
    $('#year-select-from').val("1909");
    $('#year-select-to').val(parseInt(current_year));

    $("#frequency").val(1);

    $('#journal-status').val('ALL');
    $('select').material_select();

    $('#advanced-filters-form').submit();
    
  }

  visualization_clicked = {'collection': false, 'thematic-area': false, 'language': false, 'journal': false }
  function select_from_visualization2(prefix, data_label){
    hash_check_reset('collection');
    if(data_label != false){
      checkboxes = $("input[name='"+prefix+"[]']");
      $.each(checkboxes, function(i){
        var checkbox = $('#'+checkboxes[i].id);
        var label = $('label[for="'+ checkbox.attr('id') +'"]');
        if(label.text() == data_label){
          if($("."+prefix+"-checkbox:checkbox:not(:checked)").size() == 0  || !visualization_clicked[prefix]){
            $("#"+prefix+"-to-filter li").remove();
            $("#"+prefix+"-checkbox-all").prop('checked', false);
            $("."+prefix+"-checkbox").prop('checked', false);
            visualization_clicked[prefix] = true;
          }
          checkbox.prop('checked', true);
          check_filter(checkbox, prefix, checkbox.val(), checkbox.data('label'));
          $('#'+prefix+'-to-filter .collection-item-2').size() > 1 ? $('#'+prefix+'-expand-btn').show() : $('#'+prefix+'-expand-btn').hide();
          $('.x-filter').remove();
        }
      });
    }
  }

  function toggle_filter_container(label_class, container_id, height){
    $(label_class).toggle();
    ($(container_id).height() >= 50) ? $(container_id).css({ 'min-height': '50px', height: height }) : $(container_id).css({ 'min-height': '50px', height: "100%" });
  }



  function check_all(checkbox_class, checkbox_all){
    $(checkbox_class).prop('checked', $(checkbox_all).prop('checked'));
  }
  function current_filter_check(input_name, value){
    checkbox = $("#advanced-filters-form input[name='"+input_name+"'][value='"+value+"']");
    checkbox.prop("checked", false);
    $(checkbox.attr('onchange').replace("check('","").replace("');","")).prop("checked", false);
    $("#advanced-filters-form").submit();
  }
  function current_filter_years(){
    $('#year-select-from').val("1909");
    $('#year-select-to').val(parseInt(current_year));
    $("#advanced-filters-form").submit();
  }
  function current_filter_document_type(){
    $("#document-type-select").val('ALL').trigger('change');
    $("#advanced-filters-form").submit();
  }


  //SCROLL FIJAR BOTON
  $('#menu-buttons-filters').css({ width: $("#advanced-filters-form").width() });
  var fp_top = $('#filters-form').position().top;
  var fp_height = $('#filters-form').height();
  var fp_bottom = fp_top + fp_height;
  var affsb_top = $('#menu-buttons-filters').position().top;
  if($(window).scrollTop() >= affsb_top - fp_bottom){
      $('#menu-buttons-filters').css({ position: 'fixed', top: fp_bottom + 3 });
    }
  $(window).scroll(function(){
    $('#menu-buttons-filters').css({ width: $("#advanced-filters-form").width() });
    if($(this).scrollTop() >= affsb_top - fp_bottom){
      $('#menu-buttons-filters').css({ position: 'fixed', top: fp_bottom + 3 });
    }else{
      $('#menu-buttons-filters').css({ position: 'relative', top: 'auto' });
    }
  });


  function set_lang(lang){
    $('#filters-form-lang').val(lang);
    $('#advanced-filters-form-lang').val(lang);
    $('#advanced-filters-form-lang-aux').val(lang);
    $('#advanced-filters-form-aux').submit();
  }

  
  //DataTable para checkbox
  $('.checkbox-datatable').DataTable({
    "paging": false,
    "info": false,
    "lengthMenu": [[-1], ["All"]]
  });
  //Materialize
  $('select').material_select();
  //Modals
  $('.modal-trigger').leanModal();
</script>