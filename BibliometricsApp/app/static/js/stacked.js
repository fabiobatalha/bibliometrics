function stacked(data,idDiv,disabled){
	var histcatexplong = data;
	var colors = d3.scale.category20();
	keyColor = function(d, i) {
		if (d.key != "Citations")
			return "#311B92"
		else
			return "#999999"
		};

	var chart;
	nv.addGraph(function() {
	  chart = nv.models.stackedAreaChart()
				    .width(600).height(400)
					.useInteractiveGuideline(true)
					.x(function(d) { return d[0] })
					.y(function(d) { return d[1] })
					.color(keyColor)
					.transitionDuration(300);
					//.clipEdge(true);
	// chart.stacked.scatter.clipVoronoi(false);

	  chart.xAxis
		  .tickFormat(function(d) { if (d/d.toFixed(0) == 1) return d3.time.format("%Y")(new Date(d,0)) });

	  chart.yAxis
		  .tickFormat(d3.format(',.0f'));

	  d3.select(idDiv)
		.datum(histcatexplong)
		.transition().duration(1000)
		.call(chart)
		// .transition().duration(0)
		.each('start', function() {
			setTimeout(function() {
				d3.selectAll(idDiv+' *').each(function() {
				  //~ console.log('start',this.__transition__, this)
				  // while(this.__transition__)
				  if(this.__transition__)
					this.__transition__.duration = 1;
				})
			  }, 0)
		  })
		// .each('end', function() {
		//         d3.selectAll('#chart1 *').each(function() {
		//           console.log('end', this.__transition__, this)
		//           // while(this.__transition__)
		//           if(this.__transition__)
		//             this.__transition__.duration = 1;
		//         })});

	  nv.utils.windowResize(chart.update);

	  // chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });
	  if (disabled == 1)
		$('.disabled').hide()
	  return chart;
	});
}
