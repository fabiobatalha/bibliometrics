//Evento click para d3
    jQuery.fn.d3Click = function () {
      this.each(function (i, e) {
        var evt = new MouseEvent("click");
        e.dispatchEvent(evt);
      });
    };
    
    function d3_autoplay(){
      $('#visualization-area').bind("DOMSubtreeModified",function(){
        if($('#d3plus_message').text() == "Drawing Visualization" && flag == 0){
          flag = 1
          setTimeout(function(){  $(".d3plus_timeline_play").d3Click(); }, 3500);
        }
      });        
    }