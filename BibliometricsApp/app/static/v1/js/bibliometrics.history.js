var UH_ID = 1;
// Check browser support
if (typeof(Storage) !== "undefined") {
	$("#search-history-btn").show();
} else {
	$("#search-history-btn").hide();
    console.log("Sorry, your browser does not support Web Storage...");
}

//-- -----------------------------------------------------------------

function save_to_history(form_id)
{
    //Aumentamos número de historial.
    if(localStorage["history"] && localStorage["current_history"]){
        if(localStorage["current_history"] < localStorage["history"]){
            localStorage["current_history"]++;
            localStorage["history"] = localStorage["current_history"];
        }else{
            localStorage["history"]++;
            localStorage["current_history"] = localStorage["history"];
        }
    }else{
       localStorage["history"] = 1;
       localStorage["current_history"] = 1;
    }
    //Registramos formulario
    form = $(form_id).serializeObject();
    localStorage["h"+localStorage["history"]] = JSON.stringify(form);
}

//-- -----------------------------------------------------------------

function save_to_user_history(form_id)
{
    //Aumentamos número de historial.
    localStorage["user_history"] ? localStorage["user_history"]++ : localStorage["user_history"] = 1;
    
    UH_ID = parseInt(localStorage["user_history"]) + 1;
    $("#user-history-id").text("#"+UH_ID);
    localStorage["current_user_history"] = UH_ID;
    $('#filters-form-uhid').val(UH_ID);
    $('#advanced-filters-form-uhid').val(UH_ID);
    $('#advanced-filters-form-uhid-aux').val(UH_ID);
    
    //Registramos formulario
    form = $(form_id).serializeObject();
    localStorage["uh"+localStorage["user_history"]] = JSON.stringify(form);
    //Creamos una copia en historial general
    save_to_history(form_id);
    //Desactivamos btn
    $("#advanced-filters-save-btn").addClass("disabled").attr('disabled', true);
    $("#advanced-filters-save-and-history-btn").prop("href", "/v3/user_history");
    //Desactivamos btn back - forward
    activate_back_forward_btns();
}

//-- -----------------------------------------------------------------

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function visualize_historical(h_id)
{
  data = jQuery.parseJSON(localStorage[h_id]);
  var form = $('<form></form>');
  form.attr("method", "post");
  form.attr("action", data['url']);

  $.each( data, function( key, value ){
      if( $.isArray(value)){
        for(v in value){
          var field = $('<input></input>');
          field.attr("type", "hidden");
          field.attr("name", key);
          field.attr("value", value[v]);
          form.append(field);
        }
      }else{
        var field = $('<input></input>');
        field.attr("type", "hidden");
        field.attr("name", key);
        field.attr("value", value);            
        form.append(field);
      }
  });
  $(form).appendTo('body').submit();
}


function remove_from_history(h_id)
{
  $("#"+h_id).hide('slow', function(){ $("#"+h_id).remove(); });
  localStorage.removeItem(h_id);
}

function back_to_historiy_saved()
{
    if(localStorage["current_history"] > 1){
        h_id = "h" + --localStorage["current_history"];
        visualize_historical(h_id);
    }
}

function forward_to_history_saved()
{
    id = parseInt(localStorage["current_history"]) + 1;
    if(localStorage["history"] >= id){
        localStorage["current_history"]++;
        visualize_historical("h"+id);
    }else{
       console.log("no existe history") 
    }
}

function activate_back_forward_btns(){
    $("#advanced-filters-back-btn").addClass("disabled").attr('disabled', true);
    $("#advanced-filters-forward-btn").addClass("disabled").attr('disabled', true);
    
    var ch_id = parseInt(localStorage["current_history"]);
    localStorage["user_history"] ? uh_id = parseInt(localStorage["user_history"]) + 1 : uh_id = 1;
    
    //Activate back
    var h_id_p = "h" + (ch_id-1);
    if(localStorage[h_id_p]){
        var data_p = jQuery.parseJSON(localStorage[h_id_p]);
        if(data_p['uhid'] == uh_id){
            $("#advanced-filters-back-btn").removeClass("disabled").attr('disabled', false);      
        }      
    }
    //Activate forward
    var h_id_n = "h" + (ch_id+1);
    if(localStorage[h_id_n]){
        var data_n = jQuery.parseJSON(localStorage[h_id_n]);
        if(localStorage['history'] > 1 && ch_id < localStorage['history'] && data_n['uhid'] == uh_id){
            $("#advanced-filters-forward-btn").removeClass("disabled").attr('disabled', false);
        }
    }
}

function activate_filter_deactivate_save_btns(){
    $("#advanced-filters-form-submit-btn").removeClass("disabled").attr('disabled', false);
    $("#refresh-filters-btn").removeClass("disabled").attr('disabled', false);
    $("#advanced-filters-save-btn").addClass("disabled").attr('disabled', true);
    $("#advanced-filters-save-and-history-btn").prop("href", "/v3/user_history");
}

function reset_history(){
  for(key in localStorage){
    if( key.substring(0,2) == "uh"){
      localStorage.removeItem(key);
    }
  }
  localStorage.removeItem('user_history');
  location.reload();
}

$(document).ready(function(){
    localStorage["user_history"] ? UH_ID = parseInt(localStorage["user_history"]) + 1 : UH_ID = 1;
    $("#user-history-id").text("#"+UH_ID);
    $('#filters-form-uhid').val(UH_ID);
    $('#advanced-filters-form-uhid').val(UH_ID);
    $('#advanced-filters-form-uhid-aux').val(UH_ID);
    activate_back_forward_btns();
});