function d3plus_linechart(container_id, data, id_tag, text_tag, x, y)
{
  var visualization = d3plus.viz()
		.container(container_id)  	// container DIV to hold the visualization
		.data(data)  				// data to use with the visualization
		.type("line")       		// visualization type
		.id(id_tag)         		// key for which our data is unique on
		.text(text_tag)      	 	// key to use for display text
		.y(y)         				// key to use for y-axis
		.x(x)          				// key to use for x-axis
		.y({"grid": false})
		.x({"grid": false, "label": "Years"})
	return visualization
}

function simple_linechart_with_time(container_id, data, id_tag, text_tag, x, y, time_tag)
{
  var visualization = d3plus.viz()
		.container(container_id)  	// container DIV to hold the visualization
		.data(data)  				// data to use with the visualization
		.type("line")       		// visualization type
		.id(id_tag)         		// key for which our data is unique on
		.text(text_tag)      	 	// key to use for display text
		.y(y)         				// key to use for y-axis
		.x(x)          				// key to use for x-axis
		.y({"grid": false})
		.x({"grid": false, "label": "Years"})
		.time({"value": time_tag})
		.draw()
}