function d3plus_geomap(container_id, data, id_tag, text_tag, color_tag, tooltip_tag)
{
  var visualization = d3plus.viz()
    .container(container_id)    // container DIV to hold the visualization
    .data(data)                 // data to use with the visualization
    .coords({"projection": "equirectangular", "value": "/static/js/d3plus/countries.json", "mute":["anata"]}) // pass topojson coordinates
    .type("geo_map")            // visualization type
    .id(id_tag)                 // key for which our data is unique on
    .text(text_tag)             // key to use for display text
    .color(color_tag)           // key for coloring countries
    .tooltip(tooltip_tag)
  return visualization
}

function simple_map(container_id, data, id_tag, text_tag, color_tag, tooltip_tag)
{
  var visualization = d3plus.viz()
    .container(container_id)    // container DIV to hold the visualization
    .data(data)        			    // data to use with the visualization
    .coords({"projection": "equirectangular", "value": "/static/js/d3plus/countries.json", "mute":["anata"]}) // pass topojson coordinates
    .type("geo_map")          	// visualization type
    .id(id_tag)            		  // key for which our data is unique on
    .text(text_tag)             // key to use for display text
    .color(color_tag)           // key for coloring countries
    .tooltip(tooltip_tag)
    .draw()                   	// finally, draw the visualization!
}

function simple_map_with_time(container_id, data, id_tag, text_tag, color_tag, tooltip_tag)
{
  var visualization = d3plus.viz()
    .container(container_id)    // container DIV to hold the visualization
    .data(data)                 // data to use with the visualization
    .coords({"projection": "equirectangular", "value": "/static/js/d3plus/countries.json", "mute":["anata"]}) // pass topojson coordinates
    .type("geo_map")            // visualization type
    .id(id_tag)                 // key for which our data is unique on
    .text(text_tag)             // key to use for display text
    .color(color_tag)           // key for coloring countries
    .tooltip(tooltip_tag)       // keys to place in tooltip
    .time({"value": "year"})
    .draw()                     // finally, draw the visualization!
}