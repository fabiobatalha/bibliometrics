//~ ESCALA DE COLOR INICIAL Y FINAL
var color1 = "#9fa8da";
var color2 = "#1a237e";

popupHash = {	'CHL':'',
							'BRA':'',
							'ARG':'',
							'ESP':'',
							'COL':'',
							'MEX':'',
							'PRT':'',
							'CRI':'',
							'VEN':'',
							'URY':'',
							'PER':'',
							'ZAF':'',
							'BOL':'',
							'PRY':''}

/*------------------------------------------------------------------*/

function draw_datamap()
{
	map = new Datamap({
			scope: 'world',
			projection: 'equirectangular',
			element: document.getElementById("world-map"),		
			responsive: true,
			geographyConfig: {
        borderWidth: 1,
        borderOpacity: 1,
        borderColor: '#eceff1',
        highlightFillColor: '#37474f',
        highlightBorderColor: '#eceff1',
        highlightBorderWidth: 1,
        highlightBorderOpacity: 1,
			  popupTemplate:  function(geography, data){
		 			if (geography.id in popupHash && popupHash[geography.id] != ''){
						return '<div class=hoverinfo><b>' + geography.properties.name +'</b><hr/>'+popupHash[geography.id]+'</div>';
					}else{
						return '<div class=hoverinfo><b>' + geography.properties.name +'</b></div>';							
					}
			  }  
			},	
			fills: {
			  defaultFill: '#90a4ae',
			  collectionFill : '#546e7a'
			},
			data: {	'CHL':{fillKey:'collectionFill'},
							'BRA':{fillKey:'collectionFill'},
							'ARG':{fillKey:'collectionFill'},
							'ESP':{fillKey:'collectionFill'},
							'COL':{fillKey:'collectionFill'},
							'MEX':{fillKey:'collectionFill'},
							'PRT':{fillKey:'collectionFill'},
							'CRI':{fillKey:'collectionFill'},
							'VEN':{fillKey:'collectionFill'},
							'URY':{fillKey:'collectionFill'},
							'PER':{fillKey:'collectionFill'},
							'ZAF':{fillKey:'collectionFill'},
							'BOL':{fillKey:'collectionFill'},
							'PRY':{fillKey:'collectionFill'}
			},
			done: function(datamap) {
				datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
				});
				datamap.svg.call(d3.behavior.zoom().on("zoom", redraw));
		    function redraw() {
		      datamap.svg.selectAll("g").attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
		    }
			}
		});
  return map
}

/*------------------------------------------------------------------*/

////Actualizar tamaño mapa
//window.addEventListener('resize', function() {
//	if(map !== undefined){
//  	map.resize();
//	}
//});

/*------------------------------------------------------------------*/

/*
	Calcula color a pintar cada país.
	@parametros
		data = {
			"scl": {
				"country": "Brazil",
				"collection": "BRA",
				"quantity": 10000
			},
		}
	@return
		fillsDatamap = {
			"BRA": "Hexadecimal color",
		}
*/
function fillsColor(data)
{
	maxval = 0; minval = 0
	for(i in data){
		if (maxval < data[i]['quantity'])
			maxval = data[i]['quantity']
	}
	var colorScale = d3.scale.linear()
													 .domain([minval,maxval])
													 .range([color1,color2])
	fillsDatamap = {}
	for (key in data){
		fillsDatamap[key] = colorScale(data[key]['quantity'])
	}
	return fillsDatamap;
}

/*------------------------------------------------------------------*/

/*
	Dibuja leyenda del mapa.
	@parametros
		data = {
			"scl": {
				"country": "Brazil",
				"collection": "BRA",
				"quantity": 10000
			},
		}
*/
function documents_legend(data, type)
{
	$("#world_legend").empty();
	//~ CALCULAR EL MÁXIMO VALOR PARA GRAFICAR
	maxval = 0; minval = 0
	for(i in data){
		if (maxval < data[i]['quantity'])
			maxval = data[i]['quantity']
	}
	//DEFINIMOS TAMAÑO DE LA LEYENDA
	var block_width = Math.round(0.34*$('#world-map').width());
	if(block_width <= 204){
		var block_height = 30;
		var font_size = '0.5em';
	}else if(block_width <= 338){
		var block_height = 50;
		var font_size = '0.7em';
	}else{
		var block_height = 70;
		var font_size = '0.75em';
	}

	var rect_w = 0.85*block_width, rect_h = 0.3*block_height;
	var margin_w = (block_width-rect_w)/2
	//~ GRAFICAMOS LEYENDA
	var key = d3.select("#world_legend").append("svg").attr("width", block_width).attr("height", block_height);
	var legend = key.append("defs").append("svg:linearGradient").attr("id", "gradient").attr("x1", "100%").attr("y1", "100%").attr("x2", "0%").attr("y2", "100%").attr("spreadMethod", "pad");
	legend.append("stop").attr("offset", "0%").attr("stop-color", color2).attr("stop-opacity", 1);
	legend.append("stop").attr("offset", "100%").attr("stop-color", color1).attr("stop-opacity", 1);
	//Titulo leyenda
	key.append("text").attr("y", 8).attr("x", margin_w).style("font-size", font_size).text(type);
	//Rectangulo de color
	key.append("rect").attr("width", rect_w).attr("height", rect_h).attr("transform", "translate("+ margin_w +","+ 0.3*block_height +")").style("fill", "url(#gradient)");
	//Valores leyenda
	var y = d3.scale.linear().range([0, rect_w]).domain([minval, maxval]);
	var yAxis = d3.svg.axis().scale(y).orient("bottom").tickValues([minval, (minval+maxval)/2, maxval]);
	key.append("g").attr("class", "y axis").attr("transform", "translate("+ margin_w +","+ 0.48*block_height +")").style("font-size", font_size).call(yAxis);
	$("path.domain").remove();
}

/*------------------------------------------------------------------*/


















function world_map(vistype, popup_info)
{
	map = new Datamap({
			scope: 'world',
			projection: 'equirectangular',
			element: document.getElementById("world-map"),		
			responsive: true,
			geographyConfig: {
        borderWidth: 1,
        borderOpacity: 1,
        borderColor: 'rgb(192, 192, 192)',
				strokeWidth: 1,
        highlightFillColor: '#37474f',
        highlightBorderColor: '#eceff1',
        highlightBorderWidth: 1,
        highlightBorderOpacity: 1,
			  popupTemplate:  function(geography, data){
			 		if(vistype == 0){ //Documents
			 			if (geography.id in popup_info){
							return '<div class=hoverinfo><strong><b>' + geography.properties.name +'</b><hr/>#Documents	: '+popup_info[geography.id]['quantity']+'</strong></div>';
						}else{
							return '<div class=hoverinfo><strong><b>' + geography.properties.name +'</b><hr/>#Documents	: 0</strong></div>';							
						}
					}			  	
			 		if(vistype == 1){ //Citations
			 			if (geography.id in popup_info){
							return '<div class=hoverinfo><strong><b>' + geography.properties.name +'</b><hr/>#Citations	: '+popup_info[geography.id]+'</strong></div>';
						}else{
							return '<div class=hoverinfo><strong><b>' + geography.properties.name +'</b><hr/>#Citations	: 0</strong></div>';							
						}
			  	}
			 		if(vistype == 2){ //Coauthors
						return '<div class=hoverinfo><strong><b>' + geography.properties.name +'</b></strong></div>';
			  	}			  				  	
			 		if(vistype == 3){ //Collection
						return '<div class=hoverinfo><strong><b>' + geography.properties.name +'</b><hr/>#Collections Choices: '+cc[1][geography.id][0]+'<hr/>Choice Diversity: '+cc[1][geography.id][1]+'</strong></div>';
			  	}
			 		if(vistype == 4){ //index
						return '<div class=hoverinfo><strong><b>' + geography.properties.name +'</b><hr/>Journals: '+popup_info[geography.id]["journal"]+'<br>Documents: '+popup_info[geography.id]["documents"]+'</strong></div>';
			  	}			  	
			  	else{
				  	return '<div class=hoverinfo><strong><b>' + geography.properties.name +'</strong></div>';	
			  	}
			  }  
			},	
			fills: {
			 defaultFill: '#EFEFEF',
			 collectionFill : '#B9381A'
			},
			data: {	'CHL':{fillKey:'collectionFill'},
							'BRA':{fillKey:'collectionFill'},
							'ARG':{fillKey:'collectionFill'},
							'ESP':{fillKey:'collectionFill'},
							'COL':{fillKey:'collectionFill'},
							'MEX':{fillKey:'collectionFill'},
							'PRT':{fillKey:'collectionFill'},
							'CRI':{fillKey:'collectionFill'},
							'VEN':{fillKey:'collectionFill'},
							'URY':{fillKey:'collectionFill'},
							'PER':{fillKey:'collectionFill'},
							'ZAF':{fillKey:'collectionFill'},
							'BOL':{fillKey:'collectionFill'},
							'PRY':{fillKey:'collectionFill'}
			},
			done: function(datamap) {
				datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
				});
				datamap.svg.call(d3.behavior.zoom().on("zoom", redraw));
		    function redraw() {
		      datamap.svg.selectAll("g").attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
		    }
			}
		});
  return map
}
		
function color(json, type)
{
	$("#world_legend").empty();
	//~ CALCULAR EL MÁXIMO VALOR PARA GRAFICAR
	maxval = 0; minval = 0
	for(i in json){
		if (maxval < json[i])
			maxval = json[i]
	}
	//DEFINIMOS TAMAÑO DE LA LEYENDA
	var block_width = Math.round(0.34*$('#world-map').width());
	if(block_width <= 204){
		var block_height = 30;
		var font_size = '0.5em';
	}else if(block_width <= 338){
		var block_height = 50;
		var font_size = '0.7em';
	}else{
		var block_height = 70;
		var font_size = '0.75em';
	}

	var rect_w = 0.85*block_width, rect_h = 0.3*block_height;
	var margin_w = (block_width-rect_w)/2
	//~ GRAFICAMOS LEYENDA
	var key = d3.select("#world_legend").append("svg").attr("width", block_width).attr("height", block_height);
	var legend = key.append("defs").append("svg:linearGradient").attr("id", "gradient").attr("x1", "100%").attr("y1", "100%").attr("x2", "0%").attr("y2", "100%").attr("spreadMethod", "pad");
	legend.append("stop").attr("offset", "0%").attr("stop-color", color2).attr("stop-opacity", 1);
	legend.append("stop").attr("offset", "100%").attr("stop-color", color1).attr("stop-opacity", 1);
	//Titulo leyenda
	key.append("text").attr("y", 8).attr("x", margin_w).style("font-size", font_size).text(type);
	//Rectangulo de color
	key.append("rect").attr("width", rect_w).attr("height", rect_h).attr("transform", "translate("+ margin_w +","+ 0.3*block_height +")").style("fill", "url(#gradient)");
	//Valores leyenda
	var y = d3.scale.linear().range([0, rect_w]).domain([minval, maxval]);
	var yAxis = d3.svg.axis().scale(y).orient("bottom").tickValues([minval, (minval+maxval)/2, maxval]);
	key.append("g").attr("class", "y axis").attr("transform", "translate("+ margin_w +","+ 0.48*block_height +")").style("font-size", font_size).call(yAxis);
	$("path.domain").remove();
}


//~ GRAFICAR CITAS
function citation(map,json,collection)
{
	map.updateChoropleth(null, {reset: true});
	map.updateChoropleth(color(json,"Citations"));
	$('.datamaps-subunit').unbind('click');
	$('.datamaps-subunit').on("click",function(){		
		source = $(this).attr("class").split(" ")[1];
		if(source in collection){
			$('#country-graph').html('<p>'+source+'<img src="/static/img/flags/'+source.toLowerCase()+'.gif"/></p>');			
			stacked(collection[source],'#graph svg',1); //nuevo json		
			$('#graph-container').show();
		}		
	})	
}
//~ GRAFICAR DOCUMENTOS
function publication(map,json,collection)
{
	map.updateChoropleth(null, {reset: true});
	map.updateChoropleth(color(json,"Documents"));
	$('.datamaps-subunit').unbind('click');
	$('.datamaps-subunit').on("click",function(){	
		source = $(this).attr("class").split(" ")[1]
		if(source in collection){
			$('#country-graph').html('<p>'+source+'<img src="/static/img/flags/'+source.toLowerCase()+'.gif"/></p>');			
			stacked(collection[source],'#graph svg',1); //nuevo json
			$('#graph-container').show();
		}		
	})		
}
//~ GRAFICAR REDES DE COAUTORÍA
function coauthor_network(map,json)
{
	map.updateChoropleth(null, {reset: true});
//	map.updateChoropleth(color(json[1],"Co-authors relationships"));
	$('.datamaps-subunit').on("mouseover",function(){
		map.updateChoropleth(null, {reset: true});
		source = $(this).attr("class").split(" ")[1];
		data_aux = [];
		for (i in json[0][source]){
			data_aux.push({'origin':source,'destination':i,'options':{'strokeWidth':max_edge_size(Math.log(json[0][source][i])+1),'arcSharpness':1.4}});
		}
		map.arc(data_aux);
	})
	$('.datamaps-subunit').on("mouseout",function(){
		map.updateChoropleth(null, {reset: true})
//		map.updateChoropleth(color(json[1],"Co-authors relationships"))
	})
	$('.datamaps-subunit').unbind('click');
	$('.datamaps-subunit').on("click",function(){		
		source = $(this).attr("class").split(" ")[1]
		stacked(data['Collection'],'#graph svg',1) //nuevo json		
	})		
}
//~ GRAFICAR REDES DE COLLECTIONS
function collection_network(map,json)
{
	map.updateChoropleth(null, {reset: true})
	map.updateChoropleth(color(json[1],"Collection choices"))
	$('.datamaps-subunit').on("mouseover",function(){
		map.updateChoropleth(null, {reset: true})
		source = $(this).attr("class").split(" ")[1]
		data_aux = []
		for (i in json[0][source]){
			data_aux.push({'origin':source,'destination':i,'options':{'strokeWidth':max_edge_size(Math.log(json[0][source][i])+1),'arcSharpness':1.4}})
		}
		map.arc(data_aux)
	})

	$('.datamaps-subunit').on("mouseout",function(){
		map.updateChoropleth(null, {reset: true})
		map.updateChoropleth(color(json[1],"Collection choices"))
	})
	$('.datamaps-subunit').unbind('click');
	$('.datamaps-subunit').on("click",function(){		
		source = $(this).attr("class").split(" ")[1]
		stacked(data['Collection'],'#graph svg',1) //nuevo json		
	})	
}

//~ CREAR y MOSTRAR/OCULTAR RED COAUTORIA
$("#transform-to-network").click(function(){
	if(!created_networks){				
		sigma_network(coauthor[2]);	
		created_networks =  true;
	}
	$("#network").css("height","700px");
	$("#world-map").hide();		
	$("#transform-to-network").hide();
	$("#change_vis1").show();	
	$("#change_vis2").hide();		
	$("#transform-to-map").show();			
	$("#network").show();	
})

$("#transform-to-map").click(function(){
	$("#network").hide();
	$("#world-map").show();	
	$("#change_vis1").hide();	
	$("#change_vis2").hide();		
	$("#transform-to-network").show();	
	$("#transform-to-map").hide();			
})		

$("#change_vis1").click(function(){
	$("#change_vis1").hide();		
	$("#change_vis2").show();			
	s.stopForceAtlas2();
	var R = 100,
		i = 0,
		L = s.graph.nodes().length
	s.graph.nodes().forEach(function(n){
		n.x = Math.cos(Math.PI*(i++)/L)*R;
		n.y = Math.sin(Math.PI*(i++)/L)*R;
	})		
	s.refresh()
})

$("#change_vis2").click(function(){
	$("#change_vis2").hide();		
	$("#change_vis1").show();				
	s.startForceAtlas2({iterationsPerRender:1,gravity:1,slowDown:10,strongGravityMode:true});
	s.refresh()
})


$(document).ready(function(){
	sigma_neighbors() //~ LLAMAR SIEMPRE, ES PARA INICIALIZAR UNA FUNCIÓN ALTERNATIVA DE SIGMA
})
//~ VALOR MÁXIMO PARA EL TAMAÑO DEL ENLACE
function max_edge_size(val)
{
	if (val > 30)
		val = 30
	return val
}
    

function sigma_neighbors()
{
	sigma.classes.graph.addMethod('neighbors', function(nodeId) {
		var k,
			neighbors = {},
			index = this.allNeighborsIndex[nodeId] || {};
		for (k in index)
		  neighbors[k] = this.nodesIndex[k];
		return neighbors;
	});
}
		
function sigma_network(G)
{
	s = new sigma({ 
			graph: G,
			container: 'network',
			renderer: {
						container: document.getElementById('network'),
						type: 'canvas'
					  },
			settings: {
				defaultNodeColor: '#673AB7',
				defaultEdgeColor: '#DDDDDD',
				edgeColor:'#DDDDDD',
				defaultLabelColor:'#000',
				//~ labelSize: 'proportional',
				labelThreshold: 8,
				minNodeSize: 4,
				maxNodeSize: 15,
				minEdgeSize: 1,
				maxEdgeSize:10,
				zoomingRatio: 1.2,
				defaultEdgeType:'curve',
				font: 'sans-serif',
			}
	});
	s.bind('overNode', function(e) {
		var nodeId = e.data.node.id,
			toKeep = s.graph.neighbors(nodeId);
			toKeep[nodeId] = e.data.node;
	
		s.graph.nodes().forEach(function(n) {
			if (toKeep[n.id]){
				n.color = n.originalColor;
				n.hidden = 0
			}else{
				n.color = '#eee';
				n.hidden = 1;
			}
		});
		s.graph.edges().forEach(function(e) {
			if (toKeep[e.source] && toKeep[e.target]){
				e.color = e.originalColor;
				e.hidden = 0;
			}else{
				e.color = '#eee';
				e.hidden = 1
			}
		});
		s.refresh();
	});
	s.bind('outNode', function(e) {
			s.graph.nodes().forEach(function(n) {
				n.hidden = 0
				n.color = n.originalColor;
		  
			});
			s.graph.edges().forEach(function(e) {
			  e.hidden = 0
			  e.color = e.originalColor;
			});
		s.refresh();
	  });
	s.startForceAtlas2({iterationsPerRender:1,gravity:1,slowDown:10,strongGravityMode:true});
	s.refresh()
	setTimeout(function() { s.stopForceAtlas2(); },5000);

	$("#change_vis").click(function(){
		if ($("#change_vis").attr("flag") == "0"){
			s.stopForceAtlas2();
			var R = 100,
				i = 0,
				L = s.graph.nodes().length
			s.graph.nodes().forEach(function(n){
				n.x = Math.cos(Math.PI*(i++)/L)*R;
				n.y = Math.sin(Math.PI*(i++)/L)*R;
			})		
			$("#change_vis").attr("flag","1")
		}else{
			s.startForceAtlas2({iterationsPerRender:1,gravity:1,slowDown:10,strongGravityMode:true});
			$("#change_vis").attr("flag","0")
		}
		s.refresh()
	});

	$( "select" ).change(function() {
		country = $(this).find("option:selected").val()
		s.graph.nodes().forEach(function(n){
			if (n.label == country){
				n.color = "#ff0000";
			}else{
				n.color = '#673AB7'
			}
		})
		s.refresh()
	});
}