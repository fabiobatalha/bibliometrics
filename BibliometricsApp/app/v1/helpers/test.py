#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from ISO_3166_ALPHA2 import ISO_3166_ALPHA2
from ISO_3166_ALPHA2_DEMONYM import ISO_3166_ALPHA2_DEMONYM

for k,v in ISO_3166_ALPHA2.items():
	ISO_3166_ALPHA2[k]["demonym"] = ISO_3166_ALPHA2_DEMONYM[k]["demonym"]
print json.dumps(ISO_3166_ALPHA2)