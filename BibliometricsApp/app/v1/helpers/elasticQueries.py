elasticQueries = {
    "1": {
        "text": "Documents",
        "tabulations": {
            "1": {
                "text": "by country of publication",
                "query": {
                    "aggs": {
                        "filtered": {
                            "filter": {
                            },
                             "aggs": {
                                "by_collection": {
                                    "terms": {
                                      "field": "collection",
                                      "size": 1000000000
                                    },
                                    "aggs": {
                                        "by_year": {
                                            "terms": {
                                              "field": "publication_year",
                                              "size": 0
                                            },
                                            "aggs": {
                                              "by_language": {
                                                "terms": {
                                                  "field": "languages",
                                                  "size": 0
                                                }
                                              },
                                              "by_subject_area": {
                                                "terms": {
                                                  "field": "subject_areas",
                                                  "size": 0
                                                }
                                              },
                                              "by_citable_docs": {
                                                "filter": {
                                                  "terms": {
                                                    "document_type": [
                                                      "article-commentary",
                                                      "brief-report",
                                                      "case-report",
                                                      "rapid-communication",
                                                      "research-article",
                                                      "review-article"
                                                    ]
                                                  }
                                                },
                                                "aggs": {
                                                  "by_citable_docs2": {
                                                    "terms": {
                                                      "field": "document_type",
                                                      "size": 0
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                        }
                                    }
                               }
                             }
                        }
                    }
                }
            },
            "2": {
                "text": "by thematic area",
                "query": {
                    "aggs": {
                        "filtered": {
                            "filter": {
                            },
                            "aggs":{ 
                                "by_area": {
                                    "terms": {
                                        "field": "subject_areas",
                                        "size": 1000000000
                                    },
                                    "aggs": {
                                        "by_collection": {
                                            "terms": {
                                                "field": "collection",
                                                "size": 0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_year": {
                                            "terms": {
                                              "field": "publication_year",
                                              "size": 0
                                            },
                                            "aggs": {
                                              "by_language": {
                                                "terms": {
                                                  "field": "languages",
                                                  "size": 0
                                                }
                                              },
                                              "by_subject_area": {
                                                "terms": {
                                                  "field": "subject_areas",
                                                  "size": 0
                                                }
                                              },
                                              "by_citable_docs": {
                                                "filter": {
                                                  "terms": {
                                                    "document_type": [
                                                      "article-commentary",
                                                      "brief-report",
                                                      "case-report",
                                                      "rapid-communication",
                                                      "research-article",
                                                      "review-article"
                                                    ]
                                                  }
                                                },
                                                "aggs": {
                                                  "by_citable_docs2": {
                                                    "terms": {
                                                      "field": "document_type",
                                                      "size": 0
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "3": {
                "text": "by language",
                "query": {
                    "aggs": {
                        "filtered": {
                            "filter": {
                            },
                            "aggs":{ 
                                "by_language": {
                                    "terms": {
                                        "field": "languages",
                                        "size": 1000000000
                                    },
                                    "aggs": {
                                        "by_collection": {
                                            "terms": {
                                                "field": "collection",
                                                "size": 0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_year": {
                                            "terms": {
                                              "field": "publication_year",
                                              "size": 0
                                            },
                                            "aggs": {
                                              "by_language": {
                                                "terms": {
                                                  "field": "languages",
                                                  "size": 0
                                                }
                                              },
                                              "by_subject_area": {
                                                "terms": {
                                                  "field": "subject_areas",
                                                  "size": 0
                                                }
                                              },
                                              "by_citable_docs": {
                                                "filter": {
                                                  "terms": {
                                                    "document_type": [
                                                      "article-commentary",
                                                      "brief-report",
                                                      "case-report",
                                                      "rapid-communication",
                                                      "research-article",
                                                      "review-article"
                                                    ]
                                                  }
                                                },
                                                "aggs": {
                                                  "by_citable_docs2": {
                                                    "terms": {
                                                      "field": "document_type",
                                                      "size": 0
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "4": {
                "text": "by country of author's affiliation",
                "query": {
                    "aggs": {
                        "filtered": {
                            "filter": {
                            },
                            "aggs":{ 
                                "by_affiliation": {
                                    "terms": {
                                        "field": "aff_countries",
                                        "size": 1000000000
                                    },
                                    "aggs": {
                                        "by_collection": {
                                            "terms": {
                                                "field": "collection",
                                                "size": 0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_year": {
                                            "terms": {
                                              "field": "publication_year",
                                              "size": 0
                                            },
                                            "aggs": {
                                              "by_language": {
                                                "terms": {
                                                  "field": "languages",
                                                  "size": 0
                                                }
                                              },
                                              "by_subject_area": {
                                                "terms": {
                                                  "field": "subject_areas",
                                                  "size": 0
                                                }
                                              },
                                              "by_citable_docs": {
                                                "filter": {
                                                  "terms": {
                                                    "document_type": [
                                                      "article-commentary",
                                                      "brief-report",
                                                      "case-report",
                                                      "rapid-communication",
                                                      "research-article",
                                                      "review-article"
                                                    ]
                                                  }
                                                },
                                                "aggs": {
                                                  "by_citable_docs2": {
                                                    "terms": {
                                                      "field": "document_type",
                                                      "size": 0
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "5": {
                "text": "by journals",
                "query": {
                    "aggs": {
                        "filtered": {
                            "filter": {
                            },
                            "aggs":{ 
                                "by_journal": {
                                    "terms": {
                                        "field": "journal_title",
                                        "size": 1000000000
                                    },
                                    "aggs": {
                                        "by_collection": {
                                            "terms": {
                                                "field": "collection",
                                                "size": 0
                                            },
                                            "aggs":{
                                                "by_year":{
                                                    "terms":{
                                                        "field":"publication_year",
                                                        "size":0
                                                    }
                                                }
                                            }
                                        },
                                        "by_year": {
                                            "terms": {
                                              "field": "publication_year",
                                              "size": 0
                                            },
                                            "aggs": {
                                              "by_language": {
                                                "terms": {
                                                  "field": "languages",
                                                  "size": 0
                                                }
                                              },
                                              "by_subject_area": {
                                                "terms": {
                                                  "field": "subject_areas",
                                                  "size": 0
                                                }
                                              },
                                              "by_citable_docs": {
                                                "filter": {
                                                  "terms": {
                                                    "document_type": [
                                                      "article-commentary",
                                                      "brief-report",
                                                      "case-report",
                                                      "rapid-communication",
                                                      "research-article",
                                                      "review-article"
                                                    ]
                                                  }
                                                },
                                                "aggs": {
                                                  "by_citable_docs2": {
                                                    "terms": {
                                                      "field": "document_type",
                                                      "size": 0
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },

    "2": {
        "text": "Journals",
        "tabulations": {
            "1": {
              "text": "by country of publication",
              "query": {
                "aggs": {
                  "filtered": {
                    "filter": {},
                    "aggs": {
                      "by_collection": {
                        "terms": {
                          "field": "collection",
                          "size": 1000000000
                        },
                        "aggs": {
                          "journal_count": {
                            "cardinality": {
                              "field": "journal_title"
                            }
                          },
                          "by_year": {
                            "terms": {
                              "field": "publication_year",
                              "size": 0
                            },
                            "aggs": {
                              "journal_count": {
                                "cardinality": {
                                  "field": "journal_title"
                                }
                              },
                              "by_language": {
                                "terms": {
                                  "field": "languages",
                                  "size": 0
                                },
                                "aggs": {
                                  "journal_count": {
                                    "cardinality": {
                                      "field": "journal_title"
                                    }
                                  }
                                }
                              },
                              "by_subject_area": {
                                "terms": {
                                  "field": "subject_areas",
                                  "size": 0
                                },
                                "aggs": {
                                  "journal_count": {
                                    "cardinality": {
                                      "field": "journal_title"
                                    }
                                  }
                                }
                              },
                              "by_citable_docs": {
                                "filter": {
                                  "terms": {
                                    "document_type": [
                                      "article-commentary",
                                      "brief-report",
                                      "case-report",
                                      "rapid-communication",
                                      "research-article",
                                      "review-article"
                                    ]
                                  }
                                },
                                "aggs": {
                                  "by_citable_docs2": {
                                    "terms": {
                                      "field": "document_type",
                                      "size": 0
                                    },
                                    "aggs": {
                                      "journal_count": {
                                        "cardinality": {
                                          "field": "journal_title"
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            },
            "2": {
                "text": "by thematic area",
                "query": {
                    "aggs": {
                        "filtered": {
                            "filter": {
                            },
                            "aggs":{
                              "by_collection": {
                                "terms": {
                                  "field": "collection",
                                  "size": 1000000000
                                },
                                "aggs": {
                                  "journal_count": {
                                    "cardinality": {
                                      "field": "journal_title"
                                    }
                                  }
                                }
                              },
                              "by_area": {
                                "terms": {
                                    "field": "subject_areas",
                                    "size": 1000000000
                                },
                                "aggs": {
                                  "journal_count": {
                                    "cardinality": {
                                      "field": "journal_title"
                                    }
                                  },
                                  "by_year": {
                                    "terms": {
                                      "field": "publication_year",
                                      "size": 0
                                    },
                                    "aggs": {
                                      "journal_count": {
                                        "cardinality": {
                                          "field": "journal_title"
                                        }
                                      },
                                      "by_language": {
                                        "terms": {
                                          "field": "languages",
                                          "size": 0
                                        },
                                        "aggs": {
                                          "journal_count": {
                                            "cardinality": {
                                              "field": "journal_title"
                                            }
                                          }
                                        }
                                      },
                                      "by_subject_area": {
                                        "terms": {
                                          "field": "subject_areas",
                                          "size": 0
                                        },
                                        "aggs": {
                                          "journal_count": {
                                            "cardinality": {
                                              "field": "journal_title"
                                            }
                                          }
                                        }
                                      },
                                      "by_citable_docs": {
                                        "filter": {
                                          "terms": {
                                            "document_type": [
                                              "article-commentary",
                                              "brief-report",
                                              "case-report",
                                              "rapid-communication",
                                              "research-article",
                                              "review-article"
                                            ]
                                          }
                                        },
                                        "aggs": {
                                          "by_citable_docs2": {
                                            "terms": {
                                              "field": "document_type",
                                              "size": 0
                                            },
                                            "aggs": {
                                              "journal_count": {
                                                "cardinality": {
                                                  "field": "journal_title"
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                        }
                    }
                }
            },
            "3": {
                "text": "by language",
                "query": {
                    "aggs": {
                        "filtered": {
                            "filter": {
                            },
                            "aggs":{
                              "by_collection": {
                                "terms": {
                                  "field": "collection",
                                  "size": 1000000000
                                },
                                "aggs": {
                                  "journal_count": {
                                    "cardinality": {
                                      "field": "journal_title"
                                    }
                                  }
                                }
                              }, 
                              "by_language": {
                                  "terms": {
                                      "field": "languages",
                                      "size": 1000000000
                                  },
                                  "aggs": {
                                      "journal_count": {
                                        "cardinality": {
                                          "field": "journal_title"
                                        }
                                      },
                                      "by_year": {
                                          "terms": {
                                            "field": "publication_year",
                                            "size": 0
                                          },
                                          "aggs": {
                                            "journal_count": {
                                              "cardinality": {
                                                "field": "journal_title"
                                              }
                                            },
                                            "by_language": {
                                              "terms": {
                                                "field": "languages",
                                                "size": 0
                                              },
                                              "aggs": {
                                                "journal_count": {
                                                  "cardinality": {
                                                    "field": "journal_title"
                                                  }
                                                }
                                              }
                                            },
                                            "by_subject_area": {
                                              "terms": {
                                                "field": "subject_areas",
                                                "size": 0
                                              },
                                              "aggs": {
                                                "journal_count": {
                                                  "cardinality": {
                                                    "field": "journal_title"
                                                  }
                                                }
                                              }
                                            },
                                            "by_citable_docs": {
                                              "filter": {
                                                "terms": {
                                                  "document_type": [
                                                    "article-commentary",
                                                    "brief-report",
                                                    "case-report",
                                                    "rapid-communication",
                                                    "research-article",
                                                    "review-article"
                                                  ]
                                                }
                                              },
                                              "aggs": {
                                                "by_citable_docs2": {
                                                  "terms": {
                                                    "field": "document_type",
                                                    "size": 0
                                                  },
                                                  "aggs": {
                                                    "journal_count": {
                                                      "cardinality": {
                                                        "field": "journal_title"
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
        }
    },


    "7": {
        "text": "Relationships",
        "tabulations": {
            "1": {
                "text": "Network graph",
                "query": {
                    "fields":[
                        "aff_countries",
                        "publication_year"
                    ]
                }
            },
            "2": {
                "text": "Network collection",
                "query": {
                    "fields":[
                        "aff_countries",
                        "subject_areas"
                    ]
                }
            },
            "3": {
                "text": "Network graph rings",
                "query": {
                    "fields":[
                        "aff_countries",
                        "collection"
                    ]
                }
            },
        }
    }
}