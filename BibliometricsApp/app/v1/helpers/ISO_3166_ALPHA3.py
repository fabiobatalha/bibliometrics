#!/usr/bin/env python
# -*- coding: utf-8 -*-
ISO_3166_ALPHA3 = {
	'DZA': {
		'alpha2': 'DZ',
		'code': 'afdza',
		'en': 'Algeria',
		'fullname': 'Algeria',
		'continent': 'af',
		'es': 'Algeria',
		'pt': 'Algeria'
	},
	'AGO': {
		'alpha2': 'AO',
		'code': 'afago',
		'en': 'Angola',
		'fullname': 'Angola',
		'continent': 'af',
		'es': 'Angola',
		'pt': 'Angola'
	},
	'BGD': {
		'alpha2': 'BD',
		'code': 'asbgd',
		'en': 'Bangladesh',
		'fullname': 'Bangladesh',
		'continent': 'as',
		'es': 'Bangladesh',
		'pt': 'Bangladesh'
	},
	'QAT': {
		'alpha2': 'QA',
		'code': 'asqat',
		'en': 'Qatar',
		'fullname': 'Qatar',
		'continent': 'as',
		'es': 'Qatar',
		'pt': 'Qatar'
	},
	'BGR': {
		'alpha2': 'BG',
		'code': 'eubgr',
		'en': 'Bulgaria',
		'fullname': 'Bulgaria',
		'continent': 'eu',
		'es': 'Bulgaria',
		'pt': 'Bulgaria'
	},
	'PAK': {
		'alpha2': 'PK',
		'code': 'aspak',
		'en': 'Pakistan',
		'fullname': 'Pakistan',
		'continent': 'as',
		'es': 'Pakistán',
		'pt': 'Pakistán'
	},
	'CPV': {
		'alpha2': 'CV',
		'code': 'afcpv',
		'en': 'Cape Verde',
		'fullname': 'Cape Verde Islands',
		'continent': 'af',
		'es': 'Cabo Verde',
		'pt': 'Cabo Verde'
	},
	'VNM': {
		'alpha2': 'VN',
		'code': 'asvnm',
		'en': 'Vietnam',
		'fullname': 'Vietnam',
		'continent': 'as',
		'es': 'Vietnam',
		'pt': 'Vietnam'
	},
	'PSE': {
		'alpha2': 'PS',
		'code': 'aspse',
		'en': 'Palestine',
		'fullname': 'Palestine',
		'continent': 'as',
		'es': 'Palestina',
		'pt': 'Palestina'
	},
	'TZA': {
		'alpha2': 'TZ',
		'code': 'aftza',
		'en': 'Tanzania',
		'fullname': 'Zanzibar',
		'continent': 'af',
		'es': 'Tanzania',
		'pt': 'Tanzania'
	},
	'BWA': {
		'alpha2': 'BW',
		'code': 'afbwa',
		'en': 'Botswana',
		'fullname': 'Botswana',
		'continent': 'af',
		'es': 'Botsuana',
		'pt': 'Botsuana'
	},
	'SPM': {
		'alpha2': 'PM',
		'code': 'naspm',
		'en': 'Saint Pierre and Miquelon',
		'fullname': 'St. Pierre and Miquelon',
		'continent': 'na',
		'es': 'San Pedro y Miquelón',
		'pt': 'San Pedro y Miquelón'
	},
	'KNA': {
		'alpha2': 'KN',
		'code': 'nakna',
		'en': 'Saint Kitts and Nevis',
		'fullname': 'St. Kitts',
		'continent': 'na',
		'es': 'San Cristóbal y Nieves',
		'pt': 'San Cristóbal y Nieves'
	},
	'GIB': {
		'alpha2': 'GI',
		'code': 'eugib',
		'en': 'Gibraltar',
		'fullname': 'Gibraltar',
		'continent': 'eu',
		'es': 'Gibraltar',
		'pt': 'Gibraltar'
	},
	'DJI': {
		'alpha2': 'DJ',
		'code': 'afdji',
		'en': 'Djibouti',
		'fullname': 'Djibouti',
		'continent': 'af',
		'es': 'Yibuti',
		'pt': 'Yibuti'
	},
	'GIN': {
		'alpha2': 'GN',
		'code': 'afgin',
		'en': 'Guinea',
		'fullname': 'Guinea',
		'continent': 'af',
		'es': 'Guinea',
		'pt': 'Guinea'
	},
	'FIN': {
		'alpha2': 'FI',
		'code': 'eufin',
		'en': 'Finland',
		'fullname': 'Finland',
		'continent': 'eu',
		'es': 'Finlandia',
		'pt': 'Finlandia'
	},
	'VAT': {
		'alpha2': 'VA',
		'code': 'euvat',
		'en': 'Vatican City State',
		'fullname': 'Vatican city',
		'continent': 'eu',
		'es': 'Ciudad del Vaticano',
		'pt': 'Ciudad del Vaticano'
	},
	'SYC': {
		'alpha2': 'SC',
		'code': 'afsyc',
		'en': 'Seychelles',
		'fullname': 'Seychelles',
		'continent': 'af',
		'es': 'Seychelles',
		'pt': 'Seychelles'
	},
	'NPL': {
		'alpha2': 'NP',
		'code': 'asnpl',
		'en': 'Nepal',
		'fullname': 'Nepal',
		'continent': 'as',
		'es': 'Nepal',
		'pt': 'Nepal'
	},
	'MAR': {
		'alpha2': 'MA',
		'code': 'afmar',
		'en': 'Morocco',
		'fullname': 'Morocco',
		'continent': 'af',
		'es': 'Marruecos',
		'pt': 'Marruecos'
	},
	'YEM': {
		'alpha2': 'YE',
		'code': 'asyem',
		'en': 'Yemen',
		'fullname': 'Yemen',
		'continent': 'as',
		'es': 'Yemen',
		'pt': 'Yemen'
	},
	'PHL': {
		'alpha2': 'PH',
		'code': 'asphl',
		'en': 'Philippines',
		'fullname': 'Philippines',
		'continent': 'as',
		'es': 'Filipinas',
		'pt': 'Filipinas'
	},
	'ZAF': {
		'alpha2': 'ZA',
		'code': 'afzaf',
		'en': 'South Africa',
		'fullname': 'South Africa',
		'continent': 'af',
		'es': 'Sudáfrica',
		'pt': 'Sudáfrica'
	},
	'NIC': {
		'alpha2': 'NI',
		'code': 'nanic',
		'en': 'Nicaragua',
		'fullname': 'Nicaragua',
		'continent': 'na',
		'es': 'Nicaragua',
		'pt': 'Nicaragua'
	},
	'ROU': {
		'alpha2': 'RO',
		'code': 'eurou',
		'en': 'Romania',
		'fullname': 'Romania',
		'continent': 'eu',
		'es': 'Rumanía',
		'pt': 'Rumanía'
	},
	'SYR': {
		'alpha2': 'SY',
		'code': 'assyr',
		'en': 'Syria',
		'fullname': 'Syria',
		'continent': 'as',
		'es': 'Siria',
		'pt': 'Siria'
	},
	'MAC': {
		'alpha2': 'MO',
		'code': 'asmac',
		'en': 'Macao',
		'fullname': 'Macau',
		'continent': 'as',
		'es': 'Macao',
		'pt': 'Macao'
	},
	'NIU': {
		'alpha2': 'NU',
		'code': 'ocniu',
		'en': 'Niue',
		'fullname': 'Niue',
		'continent': 'oc',
		'es': 'Niue',
		'pt': 'Niue'
	},
	'DMA': {
		'alpha2': 'DM',
		'code': 'nadma',
		'en': 'Dominica',
		'fullname': 'Dominica',
		'continent': 'na',
		'es': 'Dominica',
		'pt': 'Dominica'
	},
	'BEN': {
		'alpha2': 'BJ',
		'code': 'afben',
		'en': 'Benin',
		'fullname': 'Benin',
		'continent': 'af',
		'es': 'Benín',
		'pt': 'Benín'
	},
	'GUF': {
		'alpha2': 'GY',
		'code': 'saguf',
		'en': 'French Guiana',
		'fullname': 'Guiana',
		'continent': 'sa',
		'es': 'Guayana Francesa',
		'pt': 'Guayana Francesa'
	},
	'BEL': {
		'alpha2': 'BE',
		'code': 'eubel',
		'en': 'Belgium',
		'fullname': 'Belgium',
		'continent': 'eu',
		'es': 'Bélgica',
		'pt': 'Bélgica'
	},
	'GUM': {
		'alpha2': 'GU',
		'code': 'ocgum',
		'en': 'Guam',
		'fullname': 'Guam',
		'continent': 'oc',
		'es': 'Guam',
		'pt': 'Guam'
	},
	'GBR': {
		'alpha2': 'GB',
		'code': 'eugbr',
		'en': 'United Kingdom',
		'fullname': 'United Kingdom',
		'continent': 'eu',
		'es': 'Reino Unido',
		'pt': 'Reino Unido'
	},
	'BES': {
		'alpha2': 'SE',
		'fullname': 'Sint Eustatius',
		'code': 'nabes',
		'continent': 'na'
	},
	'GUY': {
		'alpha2': 'GY',
		'code': 'saguy',
		'en': 'Guyana',
		'fullname': 'Guyana',
		'continent': 'sa',
		'es': 'Guyana',
		'pt': 'Guyana'
	},
	'CRI': {
		'alpha2': 'CR',
		'code': 'nacri',
		'en': 'Costa Rica',
		'fullname': 'Costa Rica',
		'continent': 'na',
		'es': 'Costa Rica',
		'pt': 'Costa Rica'
	},
	'CMR': {
		'alpha2': 'CM',
		'code': 'afcmr',
		'en': 'Cameroon',
		'fullname': 'Cameroon',
		'continent': 'af',
		'es': 'Camerún',
		'pt': 'Camerún'
	},
	'LSO': {
		'alpha2': 'LS',
		'code': 'aflso',
		'en': 'Lesotho',
		'fullname': 'Lesotho',
		'continent': 'af',
		'es': 'Lesoto',
		'pt': 'Lesoto'
	},
	'HUN': {
		'alpha2': 'HU',
		'code': 'euhun',
		'en': 'Hungary',
		'fullname': 'Hungary',
		'continent': 'eu',
		'es': 'Hungría',
		'pt': 'Hungría'
	},
	'TTO': {
		'alpha2': 'TT',
		'code': 'natto',
		'en': 'Trinidad and Tobago',
		'fullname': 'Trinidad and Tobago',
		'continent': 'na',
		'es': 'Trinidad y Tobago',
		'pt': 'Trinidad y Tobago'
	},
	'PAN': {
		'alpha2': 'PA',
		'code': 'napan',
		'en': 'Panama',
		'fullname': 'Panama',
		'continent': 'na',
		'es': 'Panamá',
		'pt': 'Panamá'
	},
	'TCD': {
		'alpha2': 'TD',
		'code': 'aftcd',
		'en': 'Chad',
		'fullname': 'Chad Republic',
		'continent': 'af',
		'es': 'Chad',
		'pt': 'Chad'
	},
	'GEO': {
		'alpha2': 'GE',
		'code': 'asgeo',
		'en': 'Georgia',
		'fullname': 'Georgia',
		'continent': 'as',
		'es': 'Georgia',
		'pt': 'Georgia'
	},
	'SXM': {
		'alpha2': 'SX',
		'fullname': 'Sint Maarten',
		'code': 'nasxm',
		'continent': 'na'
	},
	'TCA': {
		'alpha2': 'TC',
		'code': 'natca',
		'en': 'Turks and Caicos Islands',
		'fullname': 'Turks and Caicos Islands',
		'continent': 'na',
		'es': 'Islas Turcas y Caicos',
		'pt': 'Islas Turcas y Caicos'
	},
	'MHL': {
		'alpha2': 'MH',
		'code': 'ocmhl',
		'en': 'Marshall Islands',
		'fullname': 'Marshall Islands',
		'continent': 'oc',
		'es': 'Islas Marshall',
		'pt': 'Islas Marshall'
	},
	'BLZ': {
		'alpha2': 'BZ',
		'code': 'nablz',
		'en': 'Belize',
		'fullname': 'Belize',
		'continent': 'na',
		'es': 'Belice',
		'pt': 'Belice'
	},
	'NFK': {
		'alpha2': 'NF',
		'code': 'ocnfk',
		'en': 'Norfolk Island',
		'fullname': 'Norfolk Island',
		'continent': 'oc',
		'es': 'Isla Norfolk',
		'pt': 'Isla Norfolk'
	},
	'VGB': {
		'alpha2': 'VG',
		'fullname': 'British Virgin Islands',
		'code': 'navgb',
		'continent': 'na'
	},
	'BLR': {
		'alpha2': 'BY',
		'code': 'eublr',
		'en': 'Belarus',
		'fullname': 'Belarus',
		'continent': 'eu',
		'es': 'Bielorrusia',
		'pt': 'Bielorrusia'
	},
	'GRD': {
		'alpha2': 'GD',
		'code': 'nagrd',
		'en': 'Grenada',
		'fullname': 'Grenada and Carriacuou',
		'continent': 'na',
		'es': 'Granada',
		'pt': 'Granada'
	},
	'GRC': {
		'alpha2': 'GR',
		'code': 'eugrc',
		'en': 'Greece',
		'fullname': 'Greece',
		'continent': 'eu',
		'es': 'Grecia',
		'pt': 'Grecia'
	},
	'GRL': {
		'alpha2': 'GL',
		'code': 'nagrl',
		'en': 'Greenland',
		'fullname': 'Greenland',
		'continent': 'na',
		'es': 'Groenlandia',
		'pt': 'Groenlandia'
	},
	'AND': {
		'alpha2': 'AD',
		'code': 'euand',
		'en': 'Andorra',
		'fullname': 'Andorra',
		'continent': 'eu',
		'es': 'Andorra',
		'pt': 'Andorra'
	},
	'RWA': {
		'alpha2': 'RW',
		'code': 'afrwa',
		'en': 'Rwanda',
		'fullname': 'Rwanda',
		'continent': 'af',
		'es': 'Ruanda',
		'pt': 'Ruanda'
	},
	'TJK': {
		'alpha2': 'TJ',
		'code': 'astjk',
		'en': 'Tajikistan',
		'fullname': 'Tajikistan',
		'continent': 'as',
		'es': 'Tadjikistán',
		'pt': 'Tadjikistán'
	},
	'MEX': {
		'alpha2': 'MX',
		'code': 'namex',
		'en': 'Mexico',
		'fullname': 'Mexico',
		'continent': 'na',
		'es': 'México',
		'pt': 'México'
	},
	'ANT': {
		'alpha2': 'CW',
		'code': 'naant',
		'en': 'Netherlands Antilles',
		'fullname': 'Netherlands Antilles',
		'continent': 'na',
		'es': 'Antillas Neerlandesas',
		'pt': 'Antillas Neerlandesas'
	},
	'LCA': {
		'alpha2': 'LC',
		'code': 'nalca',
		'en': 'Saint Lucia',
		'fullname': 'St. Lucia',
		'continent': 'na',
		'es': 'Santa Lucía',
		'pt': 'Santa Lucía'
	},
	'IND': {
		'alpha2': 'IN',
		'code': 'asind',
		'en': 'India',
		'fullname': 'India',
		'continent': 'as',
		'es': 'India',
		'pt': 'India'
	},
	'NOR': {
		'alpha2': 'NO',
		'code': 'eunor',
		'en': 'Norway',
		'fullname': 'Norway',
		'continent': 'eu',
		'es': 'Noruega',
		'pt': 'Noruega'
	},
	'FJI': {
		'alpha2': 'FJ',
		'code': 'ocfji',
		'en': 'Fiji',
		'fullname': 'Fiji Islands',
		'continent': 'oc',
		'es': 'Fiyi',
		'pt': 'Fiyi'
	},
	'HND': {
		'alpha2': 'HN',
		'code': 'nahnd',
		'en': 'Honduras',
		'fullname': 'Honduras',
		'continent': 'na',
		'es': 'Honduras',
		'pt': 'Honduras'
	},
	'DOM': {
		'alpha2': 'DO',
		'code': 'nadom',
		'en': 'Dominican Republic',
		'fullname': 'Dominican Republic',
		'continent': 'na',
		'es': 'República Dominicana',
		'pt': 'República Dominicana'
	},
	'SMR': {
		'alpha2': 'SM',
		'code': 'eusmr',
		'en': 'San Marino',
		'fullname': 'San Marino',
		'continent': 'eu',
		'es': 'San Marino',
		'pt': 'San Marino'
	},
	'PER': {
		'alpha2': 'PE',
		'code': 'saper',
		'en': 'Peru',
		'fullname': 'Peru',
		'continent': 'sa',
		'es': 'Perú',
		'pt': 'Perú'
	},
	'REU': {
		'alpha2': 'RE',
		'code': 'afreu',
		'en': 'Réunion',
		'fullname': 'Reunion Island',
		'continent': 'af',
		'es': 'Reunión',
		'pt': 'Reunión'
	},
	'VUT': {
		'alpha2': 'VU',
		'code': 'ocvut',
		'en': 'Vanuatu',
		'fullname': 'Vanuatu',
		'continent': 'oc',
		'es': 'Vanuatu',
		'pt': 'Vanuatu'
	},
	'GNQ': {
		'alpha2': 'GQ',
		'code': 'afgnq',
		'en': 'Equatorial Guinea',
		'fullname': 'Equatorial Guinea',
		'continent': 'af',
		'es': 'Guinea Ecuatorial',
		'pt': 'Guinea Ecuatorial'
	},
	'COD': {
		'alpha2': 'CD',
		'code': 'afcod',
		'en': 'Congo',
		'fullname': 'Dem. Republic of the Congo',
		'continent': 'af',
		'es': 'Congo',
		'pt': 'Congo'
	},
	'COG': {
		'alpha2': 'CG',
		'code': 'afcog',
		'en': 'Congo',
		'fullname': 'Congo',
		'continent': 'af',
		'es': 'Congo',
		'pt': 'Congo'
	},
	'GLP': {
		'alpha2': 'GP',
		'code': 'naglp',
		'en': 'Guadeloupe',
		'fullname': 'Guadeloupe',
		'continent': 'na',
		'es': 'Guadalupe',
		'pt': 'Guadalupe'
	},
	'ETH': {
		'alpha2': 'ET',
		'code': 'afeth',
		'en': 'Ethiopia',
		'fullname': 'Ethiopia',
		'continent': 'af',
		'es': 'Etiopía',
		'pt': 'Etiopía'
	},
	'COM': {
		'alpha2': 'KM',
		'code': 'afcom',
		'en': 'Comoros',
		'fullname': 'Comoros',
		'continent': 'af',
		'es': 'Comoras',
		'pt': 'Comoras'
	},
	'COL': {
		'alpha2': 'CO',
		'code': 'sacol',
		'en': 'Colombia',
		'fullname': 'Colombia',
		'continent': 'sa',
		'es': 'Colombia',
		'pt': 'Colombia'
	},
	'MDA': {
		'alpha2': 'MD',
		'code': 'eumda',
		'en': 'Moldova',
		'fullname': 'Moldova',
		'continent': 'eu',
		'es': 'Moldavia',
		'pt': 'Moldavia'
	},
	'MDG': {
		'alpha2': 'MG',
		'code': 'afmdg',
		'en': 'Madagascar',
		'fullname': 'Madagascar',
		'continent': 'af',
		'es': 'Madagascar',
		'pt': 'Madagascar'
	},
	'MDV': {
		'alpha2': 'MV',
		'code': 'asmdv',
		'en': 'Maldives',
		'fullname': 'Maldives',
		'continent': 'as',
		'es': 'Islas Maldivas',
		'pt': 'Islas Maldivas'
	},
	'SRB': {
		'alpha2': 'RS',
		'code': 'eusrb',
		'en': 'Serbia',
		'fullname': 'Serbia',
		'continent': 'eu',
		'es': 'Serbia',
		'pt': 'Serbia'
	},
	'LTU': {
		'alpha2': 'LT',
		'code': 'eultu',
		'en': 'Lithuania',
		'fullname': 'Lithuania',
		'continent': 'eu',
		'es': 'Lituania',
		'pt': 'Lituania'
	},
	'MOZ': {
		'alpha2': 'MZ',
		'code': 'afmoz',
		'en': 'Mozambique',
		'fullname': 'Mozambique',
		'continent': 'af',
		'es': 'Mozambique',
		'pt': 'Mozambique'
	},
	'ZMB': {
		'alpha2': 'ZM',
		'code': 'afzmb',
		'en': 'Zambia',
		'fullname': 'Zambia',
		'continent': 'af',
		'es': 'Zambia',
		'pt': 'Zambia'
	},
	'TWN': {
		'alpha2': 'TW',
		'code': 'astwn',
		'en': 'Taiwan',
		'fullname': 'Taiwan',
		'continent': 'as',
		'es': 'Taiwán',
		'pt': 'Taiwán'
	},
	'WLF': {
		'alpha2': 'WF',
		'code': 'ocwlf',
		'en': 'Wallis and Futuna',
		'fullname': 'Wallis and Futuna Islands',
		'continent': 'oc',
		'es': 'Wallis y Futuna',
		'pt': 'Wallis y Futuna'
	},
	'AZE': {
		'alpha2': 'AZ',
		'code': 'asaze',
		'en': 'Azerbaijan',
		'fullname': 'Azerbaijan',
		'continent': 'as',
		'es': 'Azerbayán',
		'pt': 'Azerbayán'
	},
	'AUS': {
		'alpha2': 'AU',
		'code': 'ocaus',
		'en': 'Australia',
		'fullname': 'Australia',
		'continent': 'oc',
		'es': 'Australia',
		'pt': 'Australia'
	},
	'AUT': {
		'alpha2': 'AT',
		'code': 'euaut',
		'en': 'Austria',
		'fullname': 'Austria',
		'continent': 'eu',
		'es': 'Austria',
		'pt': 'Austria'
	},
	'VEN': {
		'alpha2': 'VE',
		'code': 'saven',
		'en': 'Venezuela',
		'fullname': 'Venezuela',
		'continent': 'sa',
		'es': 'Venezuela',
		'pt': 'Venezuela'
	},
	'PLW': {
		'alpha2': 'PW',
		'code': 'ocplw',
		'en': 'Palau',
		'fullname': 'Palau',
		'continent': 'oc',
		'es': 'Palau',
		'pt': 'Palau'
	},
	'ALB': {
		'alpha2': 'AL',
		'code': 'eualb',
		'en': 'Albania',
		'fullname': 'Albania',
		'continent': 'eu',
		'es': 'Albania',
		'pt': 'Albania'
	},
	'MMR': {
		'alpha2': 'MM',
		'code': 'asmmr',
		'en': 'Myanmar',
		'fullname': 'Myanmar',
		'continent': 'as',
		'es': 'Birmania',
		'pt': 'Birmania'
	},
	'RUS': {
		'alpha2': 'RU',
		'code': 'eurus',
		'en': 'Russia',
		'fullname': 'Russia',
		'continent': 'eu',
		'es': 'Rusia',
		'pt': 'Rusia'
	},
	'MKD': {
		'alpha2': 'MK',
		'code': 'eumkd',
		'en': 'Macedonia',
		'fullname': 'Macedonia',
		'continent': 'eu',
		'es': 'Macedonia',
		'pt': 'Macedonia'
	},
	'LVA': {
		'alpha2': 'LV',
		'code': 'eulva',
		'en': 'Latvia',
		'fullname': 'Latvia',
		'continent': 'eu',
		'es': 'Letonia',
		'pt': 'Letonia'
	},
	'UKR': {
		'alpha2': 'UA',
		'code': 'euukr',
		'en': 'Ukraine',
		'fullname': 'Ukraine',
		'continent': 'eu',
		'es': 'Ucrania',
		'pt': 'Ucrania'
	},
	'GNB': {
		'alpha2': 'GW',
		'code': 'afgnb',
		'en': 'Guinea-Bissau',
		'fullname': 'Guinea-Bissau',
		'continent': 'af',
		'es': 'Guinea-Bissau',
		'pt': 'Guinea-Bissau'
	},
	'TON': {
		'alpha2': 'TO',
		'code': 'octon',
		'en': 'Tonga',
		'fullname': 'Tonga',
		'continent': 'oc',
		'es': 'Tonga',
		'pt': 'Tonga'
	},
	'CAN': {
		'alpha2': 'CA',
		'code': 'nacan',
		'en': 'Canada',
		'fullname': 'Canada',
		'continent': 'na',
		'es': 'Canadá',
		'pt': 'Canadá'
	},
	'KOR': {
		'alpha2': 'KR',
		'code': 'askor',
		'en': 'South Korea',
		'fullname': 'South Korea',
		'continent': 'as',
		'es': 'Corea del Sur',
		'pt': 'Corea del Sur'
	},
	'AIA': {
		'alpha2': 'AI',
		'code': 'naaia',
		'en': 'Anguilla',
		'fullname': 'Anguilla',
		'continent': 'na',
		'es': 'Anguila',
		'pt': 'Anguila'
	},
	'CAF': {
		'alpha2': 'CF',
		'code': 'afcaf',
		'en': 'Central African Republic',
		'fullname': 'Central African Republic',
		'continent': 'af',
		'es': 'República Centroafricana',
		'pt': 'República Centroafricana'
	},
	'CHE': {
		'alpha2': 'CH',
		'code': 'euche',
		'en': 'Switzerland',
		'fullname': 'Switzerland',
		'continent': 'eu',
		'es': 'Suiza',
		'pt': 'Suiza'
	},
	'SOM': {
		'alpha2': 'SO',
		'code': 'afsom',
		'en': 'Somalia',
		'fullname': 'Somalia Republic',
		'continent': 'af',
		'es': 'Somalia',
		'pt': 'Somalia'
	},
	'ERI': {
		'alpha2': 'ER',
		'code': 'aferi',
		'en': 'Eritrea',
		'fullname': 'Eritrea',
		'continent': 'af',
		'es': 'Eritrea',
		'pt': 'Eritrea'
	},
	'GAB': {
		'alpha2': 'GA',
		'code': 'afgab',
		'en': 'Gabon',
		'fullname': 'Gabon Republic',
		'continent': 'af',
		'es': 'Gabón',
		'pt': 'Gabón'
	},
	'IRQ': {
		'alpha2': 'IQ',
		'code': 'asirq',
		'en': 'Iraq',
		'fullname': 'Iraq',
		'continent': 'as',
		'es': 'Irak',
		'pt': 'Irak'
	},
	'MTQ': {
		'alpha2': 'MQ',
		'code': 'namtq',
		'en': 'Martinique',
		'fullname': 'Martinique',
		'continent': 'na',
		'es': 'Martinica',
		'pt': 'Martinica'
	},
	'STP': {
		'alpha2': 'ST',
		'code': 'afstp',
		'en': 'Sao Tome and Principe',
		'fullname': 'Sao Tome',
		'continent': 'af',
		'es': 'Santo Tomé y Príncipe',
		'pt': 'Santo Tomé y Príncipe'
	},
	'IRN': {
		'alpha2': 'IR',
		'code': 'asirn',
		'en': 'Iran',
		'fullname': 'Iran',
		'continent': 'as',
		'es': 'Irán',
		'pt': 'Irán'
	},
	'ABW': {
		'alpha2': 'AW',
		'code': 'naabw',
		'en': 'Aruba',
		'fullname': 'Aruba',
		'continent': 'na',
		'es': 'Aruba',
		'pt': 'Aruba'
	},
	'NZL': {
		'alpha2': 'NZ',
		'code': 'ocnzl',
		'en': 'New Zealand',
		'fullname': 'New Zealand',
		'continent': 'oc',
		'es': 'Nueva Zelanda',
		'pt': 'Nueva Zelanda'
	},
	'RSB': {
		'alpha2': 'RS',
		'fullname': 'Yugoslavia',
		'code': 'eursb',
		'continent': 'eu'
	},
	'NCL': {
		'alpha2': 'NC',
		'code': 'ocncl',
		'en': 'New Caledonia',
		'fullname': 'New Caledonia',
		'continent': 'oc',
		'es': 'Nueva Caledonia',
		'pt': 'Nueva Caledonia'
	},
	'ARE': {
		'alpha2': 'AE',
		'code': 'asare',
		'en': 'United Arab Emirates',
		'fullname': 'United Arab Emirates',
		'continent': 'as',
		'es': 'Emiratos Árabes Unidos',
		'pt': 'Emiratos Árabes Unidos'
	},
	'ARG': {
		'alpha2': 'AR',
		'code': 'saarg',
		'en': 'Argentina',
		'fullname': 'Argentina',
		'continent': 'sa',
		'es': 'Argentina',
		'pt': 'Argentina'
	},
	'BHS': {
		'alpha2': 'BS',
		'code': 'nabhs',
		'en': 'Bahamas',
		'fullname': 'Bahamas',
		'continent': 'na',
		'es': 'Bahamas',
		'pt': 'Bahamas'
	},
	'BHR': {
		'alpha2': 'BH',
		'code': 'asbhr',
		'en': 'Bahrain',
		'fullname': 'Bahrain',
		'continent': 'as',
		'es': 'Bahrein',
		'pt': 'Bahrein'
	},
	'ARM': {
		'alpha2': 'AM',
		'code': 'asarm',
		'en': 'Armenia',
		'fullname': 'Armenia',
		'continent': 'as',
		'es': 'Armenia',
		'pt': 'Armenia'
	},
	'PNG': {
		'alpha2': 'PG',
		'code': 'ocpng',
		'en': 'Papua New Guinea',
		'fullname': 'Papua New Guinea',
		'continent': 'oc',
		'es': 'Papúa Nueva Guinea',
		'pt': 'Papúa Nueva Guinea'
	},
	'LIE': {
		'alpha2': 'LI',
		'code': 'eulie',
		'en': 'Liechtenstein',
		'fullname': 'Liechtenstein',
		'continent': 'eu',
		'es': 'Liechtenstein',
		'pt': 'Liechtenstein'
	},
	'EGY': {
		'alpha2': 'EG',
		'code': 'afegy',
		'en': 'Egypt',
		'fullname': 'Egypt',
		'continent': 'af',
		'es': 'Egipto',
		'pt': 'Egipto'
	},
	'NAM': {
		'alpha2': 'NA',
		'code': 'afnam',
		'en': 'Namibia',
		'fullname': 'Namibia',
		'continent': 'af',
		'es': 'Namibia',
		'pt': 'Namibia'
	},
	'BOL': {
		'alpha2': 'BO',
		'code': 'sabol',
		'en': 'Bolivia',
		'fullname': 'Bolivia',
		'continent': 'sa',
		'es': 'Bolivia',
		'pt': 'Bolivia'
	},
	'GHA': {
		'alpha2': 'GH',
		'code': 'afgha',
		'en': 'Ghana',
		'fullname': 'Ghana',
		'continent': 'af',
		'es': 'Ghana',
		'pt': 'Ghana'
	},
	'CCK': {
		'alpha2': 'CC',
		'code': 'ascck',
		'en': 'Cocos (Keeling) Islands',
		'fullname': 'Cocos Islands',
		'continent': 'as',
		'es': 'Islas Cocos (Keeling)',
		'pt': 'Islas Cocos (Keeling)'
	},
	'JOR': {
		'alpha2': 'JO',
		'code': 'asjor',
		'en': 'Jordan',
		'fullname': 'Jordan',
		'continent': 'as',
		'es': 'Jordania',
		'pt': 'Jordania'
	},
	'LBR': {
		'alpha2': 'LR',
		'code': 'aflbr',
		'en': 'Liberia',
		'fullname': 'Liberia',
		'continent': 'af',
		'es': 'Liberia',
		'pt': 'Liberia'
	},
	'LBY': {
		'alpha2': 'LY',
		'code': 'aflby',
		'en': 'Libya',
		'fullname': 'Libya',
		'continent': 'af',
		'es': 'Libia',
		'pt': 'Libia'
	},
	'MYS': {
		'alpha2': 'MY',
		'code': 'asmys',
		'en': 'Malaysia',
		'fullname': 'Malaysia',
		'continent': 'as',
		'es': 'Malasia',
		'pt': 'Malasia'
	},
	'IOT': {
		'alpha2': 'IO',
		'code': 'asiot',
		'en': 'British Indian Ocean Territory',
		'fullname': 'Diego Garcia',
		'continent': 'as',
		'es': 'Territorio Británico del Océano Índico',
		'pt': 'Territorio Británico del Océano Índico'
	},
	'PRI': {
		'alpha2': 'PR',
		'code': 'napri',
		'en': 'Puerto Rico',
		'fullname': 'Puerto Rico',
		'continent': 'na',
		'es': 'Puerto Rico',
		'pt': 'Puerto Rico'
	},
	'MYT': {
		'alpha2': 'YT',
		'code': 'afmyt',
		'en': 'Mayotte',
		'fullname': 'Mayotte Island',
		'continent': 'af',
		'es': 'Mayotte',
		'pt': 'Mayotte'
	},
	'PRK': {
		'alpha2': 'KP',
		'code': 'asprk',
		'en': 'North Korea',
		'fullname': 'North Korea',
		'continent': 'as',
		'es': 'Corea del Norte',
		'pt': 'Corea del Norte'
	},
	'PRT': {
		'alpha2': 'PT',
		'code': 'euprt',
		'en': 'Portugal',
		'fullname': 'Portugal',
		'continent': 'eu',
		'es': 'Portugal',
		'pt': 'Portugal'
	},
	'KHM': {
		'alpha2': 'KH',
		'code': 'askhm',
		'en': 'Cambodia',
		'fullname': 'Cambodia',
		'continent': 'as',
		'es': 'Camboya',
		'pt': 'Camboya'
	},
	'PRY': {
		'alpha2': 'PY',
		'code': 'sapry',
		'en': 'Paraguay',
		'fullname': 'Paraguay',
		'continent': 'sa',
		'es': 'Paraguay',
		'pt': 'Paraguay'
	},
	'HKG': {
		'alpha2': 'HK',
		'code': 'ashkg',
		'en': 'Hong Kong',
		'fullname': 'Hong Kong',
		'continent': 'as',
		'es': 'Hong kong',
		'pt': 'Hong kong'
	},
	'SAU': {
		'alpha2': 'SA',
		'code': 'assau',
		'en': 'Saudi Arabia',
		'fullname': 'Saudi Arabia',
		'continent': 'as',
		'es': 'Arabia Saudita',
		'pt': 'Arabia Saudita'
	},
	'LBN': {
		'alpha2': 'LB',
		'code': 'aslbn',
		'en': 'Lebanon',
		'fullname': 'Lebanon',
		'continent': 'as',
		'es': 'Líbano',
		'pt': 'Líbano'
	},
	'SVN': {
		'alpha2': 'SI',
		'code': 'eusvn',
		'en': 'Slovenia',
		'fullname': 'Slovenia',
		'continent': 'eu',
		'es': 'Eslovenia',
		'pt': 'Eslovenia'
	},
	'BFA': {
		'alpha2': 'BF',
		'code': 'afbfa',
		'en': 'Burkina Faso',
		'fullname': 'Burkina Faso',
		'continent': 'af',
		'es': 'Burkina Faso',
		'pt': 'Burkina Faso'
	},
	'SVK': {
		'alpha2': 'SK',
		'code': 'eusvk',
		'en': 'Slovakia',
		'fullname': 'Slovakia',
		'continent': 'eu',
		'es': 'Eslovaquia',
		'pt': 'Eslovaquia'
	},
	'MRT': {
		'alpha2': 'MR',
		'code': 'afmrt',
		'en': 'Mauritania',
		'fullname': 'Mauritania',
		'continent': 'af',
		'es': 'Mauritania',
		'pt': 'Mauritania'
	},
	'HRV': {
		'alpha2': 'HR',
		'code': 'euhrv',
		'en': 'Croatia',
		'fullname': 'Croatia',
		'continent': 'eu',
		'es': 'Croacia',
		'pt': 'Croacia'
	},
	'CHL': {
		'alpha2': 'CL',
		'code': 'sachl',
		'en': 'Chile',
		'fullname': 'Chile',
		'continent': 'sa',
		'es': 'Chile',
		'pt': 'Chile'
	},
	'CHN': {
		'alpha2': 'CN',
		'code': 'aschn',
		'en': 'China',
		'fullname': 'China',
		'continent': 'as',
		'es': 'China',
		'pt': 'China'
	},
	'FSM': {
		'alpha2': 'FM',
		'code': 'ocfsm',
		'en': 'Estados Federados de',
		'fullname': 'Federated States of Micronesia',
		'continent': 'oc',
		'es': 'Micronesia',
		'pt': 'Micronesia'
	},
	'JAM': {
		'alpha2': 'JM',
		'code': 'najam',
		'en': 'Jamaica',
		'fullname': 'Jamaica',
		'continent': 'na',
		'es': 'Jamaica',
		'pt': 'Jamaica'
	},
	'URY': {
		'alpha2': 'UY',
		'code': 'saury',
		'en': 'Uruguay',
		'fullname': 'Uruguay',
		'continent': 'sa',
		'es': 'Uruguay',
		'pt': 'Uruguai'
	},
	'CXR': {
		'alpha2': 'CX',
		'code': 'ascxr',
		'en': 'Christmas Island',
		'fullname': 'Christmas Island',
		'continent': 'as',
		'es': 'Isla de Navidad',
		'pt': 'Isla de Navidad'
	},
	'VIR': {
		'alpha2': 'VI',
		'code': 'navir',
		'en': 'United States Virgin Islands',
		'fullname': 'US Virgin Islands',
		'continent': 'na',
		'es': 'Islas Vírgenes de los Estados Unidos',
		'pt': 'Islas Vírgenes de los Estados Unidos'
	},
	'KIR': {
		'alpha2': 'KI',
		'code': 'ockir',
		'en': 'Kiribati',
		'fullname': 'Kiribati',
		'continent': 'oc',
		'es': 'Kiribati',
		'pt': 'Kiribati'
	},
	'KAZ': {
		'alpha2': 'KZ',
		'code': 'askaz',
		'en': 'Kazakhstan',
		'fullname': 'Kazakhstan',
		'continent': 'as',
		'es': 'Kazajistán',
		'pt': 'Kazajistán'
	},
	'PYF': {
		'alpha2': 'PF',
		'code': 'ocpyf',
		'en': 'French Polynesia',
		'fullname': 'French Polynesia',
		'continent': 'oc',
		'es': 'Polinesia Francesa',
		'pt': 'Polinesia Francesa'
	},
	'NGA': {
		'alpha2': 'NG',
		'code': 'afnga',
		'en': 'Nigeria',
		'fullname': 'Nigeria',
		'continent': 'af',
		'es': 'Nigeria',
		'pt': 'Nigeria'
	},
	'DEU': {
		'alpha2': 'DE',
		'code': 'eudeu',
		'en': 'Germany',
		'fullname': 'Germany',
		'continent': 'eu',
		'es': 'Alemania',
		'pt': 'Alemania'
	},
	'LKA': {
		'alpha2': 'LK',
		'code': 'aslka',
		'en': 'Sri Lanka',
		'fullname': 'Sri Lanka',
		'continent': 'as',
		'es': 'Sri lanka',
		'pt': 'Sri lanka'
	},
	'FLK': {
		'alpha2': 'FK',
		'code': 'saflk',
		'en': 'Falkland Islands (Malvinas)',
		'fullname': 'Falkland Islands',
		'continent': 'sa',
		'es': 'Islas Malvinas',
		'pt': 'Islas Malvinas'
	},
	'MWI': {
		'alpha2': 'MW',
		'code': 'afmwi',
		'en': 'Malawi',
		'fullname': 'Malawi',
		'continent': 'af',
		'es': 'Malawi',
		'pt': 'Malawi'
	},
	'COK': {
		'alpha2': 'CK',
		'code': 'occok',
		'en': 'Cook Islands',
		'fullname': 'Cook Islands',
		'continent': 'oc',
		'es': 'Islas Cook',
		'pt': 'Islas Cook'
	},
	'MNP': {
		'alpha2': 'MP',
		'code': 'ocmnp',
		'en': 'Northern Mariana Islands',
		'fullname': 'Saipan',
		'continent': 'oc',
		'es': 'Islas Marianas del Norte',
		'pt': 'Islas Marianas del Norte'
	},
	'UMI': {
		'alpha2': 'UM',
		'code': 'ocumi',
		'en': 'United States Minor Outlying Islands',
		'fullname': 'Wake Island',
		'continent': 'oc',
		'es': 'Islas Ultramarinas Menores de Estados Unidos',
		'pt': 'Islas Ultramarinas Menores de Estados Unidos'
	},
	'UGA': {
		'alpha2': 'UG',
		'code': 'afuga',
		'en': 'Uganda',
		'fullname': 'Uganda',
		'continent': 'af',
		'es': 'Uganda',
		'pt': 'Uganda'
	},
	'TKM': {
		'alpha2': 'TM',
		'code': 'astkm',
		'en': 'Turkmenistan',
		'fullname': 'Turkmenistan',
		'continent': 'as',
		'es': 'Turkmenistán',
		'pt': 'Turkmenistán'
	},
	'NLD': {
		'alpha2': 'NL',
		'code': 'eunld',
		'en': 'Netherlands',
		'fullname': 'Netherlands',
		'continent': 'eu',
		'es': 'Países Bajos',
		'pt': 'Países Bajos'
	},
	'BMU': {
		'alpha2': 'BM',
		'code': 'nabmu',
		'en': 'Bermuda Islands',
		'fullname': 'Bermuda',
		'continent': 'na',
		'es': 'Islas Bermudas',
		'pt': 'Islas Bermudas'
	},
	'MNE': {
		'alpha2': 'ME',
		'code': 'eumne',
		'en': 'Montenegro',
		'fullname': 'Montenegro',
		'continent': 'eu',
		'es': 'Montenegro',
		'pt': 'Montenegro'
	},
	'MNG': {
		'alpha2': 'MN',
		'code': 'asmng',
		'en': 'Mongolia',
		'fullname': 'Mongolia',
		'continent': 'as',
		'es': 'Mongolia',
		'pt': 'Mongolia'
	},
	'AFG': {
		'alpha2': 'AF',
		'code': 'asafg',
		'en': 'Afghanistan',
		'fullname': 'Afghanistan',
		'continent': 'as',
		'es': 'Afganistán',
		'pt': 'Afganistán'
	},
	'BDI': {
		'alpha2': 'BI',
		'code': 'afbdi',
		'en': 'Burundi',
		'fullname': 'Burundi',
		'continent': 'af',
		'es': 'Burundi',
		'pt': 'Burundi'
	},
	'SHN': {
		'alpha2': 'SH',
		'code': 'afshn',
		'en': 'Ascensión y Tristán de Acuña',
		'fullname': 'St. Helena',
		'continent': 'af',
		'es': 'Santa Elena',
		'pt': 'Santa Elena'
	},
	'ZWE': {
		'alpha2': 'ZW',
		'code': 'afzwe',
		'en': 'Zimbabwe',
		'fullname': 'Zimbabwe',
		'continent': 'af',
		'es': 'Zimbabue',
		'pt': 'Zimbabue'
	},
	'THA': {
		'alpha2': 'TH',
		'code': 'astha',
		'en': 'Thailand',
		'fullname': 'Thailand',
		'continent': 'as',
		'es': 'Tailandia',
		'pt': 'Tailandia'
	},
	'HTI': {
		'alpha2': 'HT',
		'code': 'nahti',
		'en': 'Haiti',
		'fullname': 'Haiti',
		'continent': 'na',
		'es': 'Haití',
		'pt': 'Haití'
	},
	'IRL': {
		'alpha2': 'IE',
		'code': 'euirl',
		'en': 'Ireland',
		'fullname': 'Ireland',
		'continent': 'eu',
		'es': 'Irlanda',
		'pt': 'Irlanda'
	},
	'BTN': {
		'alpha2': 'BT',
		'code': 'asbtn',
		'en': 'Bhutan',
		'fullname': 'Bhutan',
		'continent': 'as',
		'es': 'Bhután',
		'pt': 'Bhután'
	},
	'CZE': {
		'alpha2': 'CZ',
		'code': 'eucze',
		'en': 'Czech Republic',
		'fullname': 'Czech Republic',
		'continent': 'eu',
		'es': 'República Checa',
		'pt': 'República Checa'
	},
	'ATG': {
		'alpha2': 'AG',
		'code': 'naatg',
		'en': 'Antigua and Barbuda',
		'fullname': 'Antigua and Barbuda',
		'continent': 'na',
		'es': 'Antigua y Barbuda',
		'pt': 'Antigua y Barbuda'
	},
	'MUS': {
		'alpha2': 'MU',
		'code': 'afmus',
		'en': 'Mauritius',
		'fullname': 'Mauritius',
		'continent': 'af',
		'es': 'Mauricio',
		'pt': 'Mauricio'
	},
	'ATA': {
		'alpha2': 'AQ',
		'code': 'anata',
		'en': 'Antarctica',
		'fullname': 'Antarctica',
		'continent': 'an',
		'es': 'Antártida',
		'pt': 'Antártida'
	},
	'LUX': {
		'alpha2': 'LU',
		'code': 'eulux',
		'en': 'Luxembourg',
		'fullname': 'Luxembourg',
		'continent': 'eu',
		'es': 'Luxemburgo',
		'pt': 'Luxemburgo'
	},
	'ISR': {
		'alpha2': 'IL',
		'code': 'asisr',
		'en': 'Israel',
		'fullname': 'Israel',
		'continent': 'as',
		'es': 'Israel',
		'pt': 'Israel'
	},
	'IDN': {
		'alpha2': 'ID',
		'code': 'asidn',
		'en': 'Indonesia',
		'fullname': 'Indonesia',
		'continent': 'as',
		'es': 'Indonesia',
		'pt': 'Indonesia'
	},
	'SUR': {
		'alpha2': 'SR',
		'code': 'sasur',
		'en': 'Suriname',
		'fullname': 'Suriname',
		'continent': 'sa',
		'es': 'Surinám',
		'pt': 'Surinám'
	},
	'ISL': {
		'alpha2': 'IS',
		'code': 'euisl',
		'en': 'Iceland',
		'fullname': 'Iceland',
		'continent': 'eu',
		'es': 'Islandia',
		'pt': 'Islandia'
	},
	'WSM': {
		'alpha2': 'WS',
		'code': 'ocwsm',
		'en': 'Samoa',
		'fullname': 'Samoa',
		'continent': 'oc',
		'es': 'Samoa',
		'pt': 'Samoa'
	},
	'NER': {
		'alpha2': 'NE',
		'code': 'afner',
		'en': 'Niger',
		'fullname': 'Niger Republic',
		'continent': 'af',
		'es': 'Niger',
		'pt': 'Niger'
	},
	'ECU': {
		'alpha2': 'EC',
		'code': 'saecu',
		'en': 'Ecuador',
		'fullname': 'Ecuador',
		'continent': 'sa',
		'es': 'Ecuador',
		'pt': 'Ecuador'
	},
	'SEN': {
		'alpha2': 'SN',
		'code': 'afsen',
		'en': 'Senegal',
		'fullname': 'Senegal Republic',
		'continent': 'af',
		'es': 'Senegal',
		'pt': 'Senegal'
	},
	'ASM': {
		'alpha2': 'AS',
		'code': 'ocasm',
		'en': 'American Samoa',
		'fullname': 'American Samoa',
		'continent': 'oc',
		'es': 'Samoa Americana',
		'pt': 'Samoa Americana'
	},
	'CUW': {
		'alpha2': 'CW',
		'fullname': 'Curacao',
		'code': 'nacuw',
		'continent': 'na'
	},
	'FRA': {
		'alpha2': 'FR',
		'code': 'eufra',
		'en': 'France',
		'fullname': 'France',
		'continent': 'eu',
		'es': 'Francia',
		'pt': 'Francia'
	},
	'GMB': {
		'alpha2': 'GM',
		'code': 'afgmb',
		'en': 'Gambia',
		'fullname': 'Gambia',
		'continent': 'af',
		'es': 'Gambia',
		'pt': 'Gambia'
	},
	'SSD': {
		'alpha2': 'SS',
		'fullname': 'South Sudan',
		'code': 'afssd',
		'continent': 'af'
	},
	'FRO': {
		'alpha2': 'FO',
		'code': 'eufro',
		'en': 'Faroe Islands',
		'fullname': 'Faroe Islands',
		'continent': 'eu',
		'es': 'Islas Feroe',
		'pt': 'Islas Feroe'
	},
	'GTM': {
		'alpha2': 'GT',
		'code': 'nagtm',
		'en': 'Guatemala',
		'fullname': 'Guatemala',
		'continent': 'na',
		'es': 'Guatemala',
		'pt': 'Guatemala'
	},
	'DNK': {
		'alpha2': 'DK',
		'code': 'eudnk',
		'en': 'Denmark',
		'fullname': 'Denmark',
		'continent': 'eu',
		'es': 'Dinamarca',
		'pt': 'Dinamarca'
	},
	'IMN': {
		'alpha2': 'IM',
		'code': 'euimn',
		'en': 'Isle of Man',
		'fullname': 'Isle of Man',
		'continent': 'eu',
		'es': 'Isla de Man',
		'pt': 'Isla de Man'
	},
	'CUB': {
		'alpha2': 'CU',
		'code': 'nacub',
		'en': 'Cuba',
		'fullname': 'Cuba',
		'continent': 'na',
		'es': 'Cuba',
		'pt': 'Cuba'
	},
	'KEN': {
		'alpha2': 'KE',
		'code': 'afken',
		'en': 'Kenya',
		'fullname': 'Kenya',
		'continent': 'af',
		'es': 'Kenia',
		'pt': 'Kenia'
	},
	'LAO': {
		'alpha2': 'LA',
		'code': 'aslao',
		'en': 'Laos',
		'fullname': 'Laos',
		'continent': 'as',
		'es': 'Laos',
		'pt': 'Laos'
	},
	'TUR': {
		'alpha2': 'TR',
		'code': 'astur',
		'en': 'Turkey',
		'fullname': 'Turkey',
		'continent': 'as',
		'es': 'Turquía',
		'pt': 'Turquía'
	},
	'OMN': {
		'alpha2': 'OM',
		'code': 'asomn',
		'en': 'Oman',
		'fullname': 'Oman',
		'continent': 'as',
		'es': 'Omán',
		'pt': 'Omán'
	},
	'TUV': {
		'alpha2': 'TV',
		'code': 'octuv',
		'en': 'Tuvalu',
		'fullname': 'Tuvalu',
		'continent': 'oc',
		'es': 'Tuvalu',
		'pt': 'Tuvalu'
	},
	'ITA': {
		'alpha2': 'IT',
		'code': 'euita',
		'en': 'Italy',
		'fullname': 'Italy',
		'continent': 'eu',
		'es': 'Italia',
		'pt': 'Italia'
	},
	'BRN': {
		'alpha2': 'BN',
		'code': 'asbrn',
		'en': 'Brunei',
		'fullname': 'Brunei',
		'continent': 'as',
		'es': 'Brunéi',
		'pt': 'Brunéi'
	},
	'TUN': {
		'alpha2': 'TN',
		'code': 'aftun',
		'en': 'Tunisia',
		'fullname': 'Tunisia',
		'continent': 'af',
		'es': 'Tunez',
		'pt': 'Tunez'
	},
	'BRB': {
		'alpha2': 'BB',
		'code': 'nabrb',
		'en': 'Barbados',
		'fullname': 'Barbados',
		'continent': 'na',
		'es': 'Barbados',
		'pt': 'Barbados'
	},
	'BRA': {
		'alpha2': 'BR',
		'code': 'sabra',
		'en': 'Brazil',
		'fullname': 'Brazil',
		'continent': 'sa',
		'es': 'Brasil',
		'pt': 'Brasil'
	},
	'TLS': {
		'alpha2': 'TL',
		'code': 'octls',
		'en': 'East Timor',
		'fullname': 'East Timor',
		'continent': 'oc',
		'es': 'Timor Oriental',
		'pt': 'Timor Oriental'
	},
	'USA': {
		'alpha2': 'US',
		'code': 'nausa',
		'en': 'United States of America',
		'fullname': 'United States',
		'continent': 'na',
		'es': 'Estados Unidos de América',
		'pt': 'Estados Unidos de América'
	},
	'SWE': {
		'alpha2': 'SE',
		'code': 'euswe',
		'en': 'Sweden',
		'fullname': 'Sweden',
		'continent': 'eu',
		'es': 'Suecia',
		'pt': 'Suecia'
	},
	'MSR': {
		'alpha2': 'MS',
		'code': 'namsr',
		'en': 'Montserrat',
		'fullname': 'Montserrat',
		'continent': 'na',
		'es': 'Montserrat',
		'pt': 'Montserrat'
	},
	'SWZ': {
		'alpha2': 'SZ',
		'code': 'afswz',
		'en': 'Swaziland',
		'fullname': 'Swaziland',
		'continent': 'af',
		'es': 'Swazilandia',
		'pt': 'Swazilandia'
	},
	'CIV': {
		'alpha2': 'CI',
		'code': 'afciv',
		'en': 'Ivory Coast',
		'fullname': 'Ivory Coast',
		'continent': 'af',
		'es': 'Costa de Marfil',
		'pt': 'Costa de Marfil'
	},
	'CYP': {
		'alpha2': 'CY',
		'code': 'eucyp',
		'en': 'Cyprus',
		'fullname': 'Cyprus',
		'continent': 'eu',
		'es': 'Chipre',
		'pt': 'Chipre'
	},
	'BIH': {
		'alpha2': 'BA',
		'code': 'eubih',
		'en': 'Bosnia and Herzegovina',
		'fullname': 'Bosnia and Herzegovina',
		'continent': 'eu',
		'es': 'Bosnia y Herzegovina',
		'pt': 'Bosnia y Herzegovina'
	},
	'SGP': {
		'alpha2': 'SG',
		'code': 'assgp',
		'en': 'Singapore',
		'fullname': 'Singapore',
		'continent': 'as',
		'es': 'Singapur',
		'pt': 'Singapur'
	},
	'UZB': {
		'alpha2': 'UZ',
		'code': 'asuzb',
		'en': 'Uzbekistan',
		'fullname': 'Uzbekistan',
		'continent': 'as',
		'es': 'Uzbekistán',
		'pt': 'Uzbekistán'
	},
	'POL': {
		'alpha2': 'PL',
		'code': 'eupol',
		'en': 'Poland',
		'fullname': 'Poland',
		'continent': 'eu',
		'es': 'Polonia',
		'pt': 'Polonia'
	},
	'KWT': {
		'alpha2': 'KW',
		'code': 'askwt',
		'en': 'Kuwait',
		'fullname': 'Kuwait',
		'continent': 'as',
		'es': 'Kuwait',
		'pt': 'Kuwait'
	},
	'TGO': {
		'alpha2': 'TG',
		'code': 'aftgo',
		'en': 'Togo',
		'fullname': 'Togo',
		'continent': 'af',
		'es': 'Togo',
		'pt': 'Togo'
	},
	'CYM': {
		'alpha2': 'KY',
		'code': 'nacym',
		'en': 'Cayman Islands',
		'fullname': 'Cayman Islands',
		'continent': 'na',
		'es': 'Islas Caimán',
		'pt': 'Islas Caimán'
	},
	'EST': {
		'alpha2': 'EE',
		'code': 'euest',
		'en': 'Estonia',
		'fullname': 'Estonia',
		'continent': 'eu',
		'es': 'Estonia',
		'pt': 'Estonia'
	},
	'ESP': {
		'alpha2': 'ES',
		'code': 'euesp',
		'en': 'Spain',
		'fullname': 'Spain',
		'continent': 'eu',
		'es': 'España',
		'pt': 'España'
	},
	'SLV': {
		'alpha2': 'SV',
		'code': 'naslv',
		'en': 'El Salvador',
		'fullname': 'El Salvador',
		'continent': 'na',
		'es': 'El Salvador',
		'pt': 'El Salvador'
	},
	'MLI': {
		'alpha2': 'ML',
		'code': 'afmli',
		'en': 'Mali',
		'fullname': 'Mali Republic',
		'continent': 'af',
		'es': 'Mali',
		'pt': 'Mali'
	},
	'VCT': {
		'alpha2': 'VC',
		'code': 'navct',
		'en': 'Saint Vincent and the Grenadines',
		'fullname': 'St. Vincent',
		'continent': 'na',
		'es': 'San Vicente y las Granadinas',
		'pt': 'San Vicente y las Granadinas'
	},
	'MLT': {
		'alpha2': 'MT',
		'code': 'eumlt',
		'en': 'Malta',
		'fullname': 'Malta',
		'continent': 'eu',
		'es': 'Malta',
		'pt': 'Malta'
	},
	'SLE': {
		'alpha2': 'SL',
		'code': 'afsle',
		'en': 'Sierra Leone',
		'fullname': 'Sierra Leone',
		'continent': 'af',
		'es': 'Sierra Leona',
		'pt': 'Sierra Leona'
	},
	'SDN': {
		'alpha2': 'SD',
		'code': 'afsdn',
		'en': 'Sudan',
		'fullname': 'Sudan',
		'continent': 'af',
		'es': 'Sudán',
		'pt': 'Sudán'
	},
	'SLB': {
		'alpha2': 'SB',
		'code': 'ocslb',
		'en': 'Solomon Islands',
		'fullname': 'Solomon Islands',
		'continent': 'oc',
		'es': 'Islas Salomón',
		'pt': 'Islas Salomón'
	},
	'MCO': {
		'alpha2': 'MC',
		'code': 'eumco',
		'en': 'Monaco',
		'fullname': 'Monaco',
		'continent': 'eu',
		'es': 'Mónaco',
		'pt': 'Mónaco'
	},
	'JPN': {
		'alpha2': 'JP',
		'code': 'asjpn',
		'en': 'Japan',
		'fullname': 'Japan',
		'continent': 'as',
		'es': 'Japón',
		'pt': 'Japón'
	},
	'KGZ': {
		'alpha2': 'KG',
		'code': 'askgz',
		'en': 'Kyrgyzstan',
		'fullname': 'Kyrgyzstan',
		'continent': 'as',
		'es': 'Kirgizstán',
		'pt': 'Kirgizstán'
	},
	'TKL': {
		'alpha2': 'TK',
		'code': 'octkl',
		'en': 'Tokelau',
		'fullname': 'Tokelau',
		'continent': 'oc',
		'es': 'Tokelau',
		'pt': 'Tokelau'
	},
	'NRU': {
		'alpha2': 'NR',
		'code': 'ocnru',
		'en': 'Nauru',
		'fullname': 'Nauru',
		'continent': 'oc',
		'es': 'Nauru',
		'pt': 'Nauru'
	}
}