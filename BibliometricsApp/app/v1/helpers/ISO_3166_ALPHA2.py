#!/usr/bin/env python
# -*- coding: utf-8 -*-
ISO_3166_ALPHA2 = {
	'WF': {
		'alpha3': 'WLF',
		'code': 'ocwlf',
		'demonym': 'Wallisian',
		'en': 'Wallis and Futuna',
		'fullname': 'Wallis and Futuna Islands',
		'continent': 'oc',
		'es': 'Wallis y Futuna',
		'pt': 'Wallis y Futuna'
	},
	'JP': {
		'alpha3': 'JPN',
		'code': 'asjpn',
		'demonym': 'Japanese',
		'en': 'Japan',
		'fullname': 'Japan',
		'continent': 'as',
		'es': 'Japón',
		'pt': 'Japón'
	},
	'JM': {
		'alpha3': 'JAM',
		'code': 'najam',
		'demonym': 'Jamaican',
		'en': 'Jamaica',
		'fullname': 'Jamaica',
		'continent': 'na',
		'es': 'Jamaica',
		'pt': 'Jamaica'
	},
	'JO': {
		'alpha3': 'JOR',
		'code': 'asjor',
		'demonym': 'Jordanian',
		'en': 'Jordan',
		'fullname': 'Jordan',
		'continent': 'as',
		'es': 'Jordania',
		'pt': 'Jordania'
	},
	'WS': {
		'alpha3': 'WSM',
		'code': 'ocwsm',
		'demonym': 'Samoan',
		'en': 'Samoa',
		'fullname': 'Samoa',
		'continent': 'oc',
		'es': 'Samoa',
		'pt': 'Samoa'
	},
	'GW': {
		'alpha3': 'GNB',
		'code': 'afgnb',
		'demonym': 'Guinean',
		'en': 'Guinea-Bissau',
		'fullname': 'Guinea-Bissau',
		'continent': 'af',
		'es': 'Guinea-Bissau',
		'pt': 'Guinea-Bissau'
	},
	'GU': {
		'alpha3': 'GUM',
		'code': 'ocgum',
		'demonym': 'Guamanian',
		'en': 'Guam',
		'fullname': 'Guam',
		'continent': 'oc',
		'es': 'Guam',
		'pt': 'Guam'
	},
	'GT': {
		'alpha3': 'GTM',
		'code': 'nagtm',
		'demonym': 'Guatemalan',
		'en': 'Guatemala',
		'fullname': 'Guatemala',
		'continent': 'na',
		'es': 'Guatemala',
		'pt': 'Guatemala'
	},
	'GR': {
		'alpha3': 'GRC',
		'code': 'eugrc',
		'demonym': 'Greek',
		'en': 'Greece',
		'fullname': 'Greece',
		'continent': 'eu',
		'es': 'Grecia',
		'pt': 'Grecia'
	},
	'GQ': {
		'alpha3': 'GNQ',
		'code': 'afgnq',
		'demonym': 'Equatorial Guinean',
		'en': 'Equatorial Guinea',
		'fullname': 'Equatorial Guinea',
		'continent': 'af',
		'es': 'Guinea Ecuatorial',
		'pt': 'Guinea Ecuatorial'
	},
	'GP': {
		'alpha3': 'GLP',
		'code': 'naglp',
		'demonym': 'Guadeloupean',
		'en': 'Guadeloupe',
		'fullname': 'Guadeloupe',
		'continent': 'na',
		'es': 'Guadalupe',
		'pt': 'Guadalupe'
	},
	'GY': {
		'alpha3': 'GUY',
		'code': 'saguy',
		'demonym': 'Guyanese',
		'en': 'Guyana',
		'fullname': 'Guyana',
		'continent': 'sa',
		'es': 'Guyana',
		'pt': 'Guyana'
	},
	'GF': {
		'alpha3': 'GUF',
		'code': 'saguf',
		'demonym': 'French Guianese',
		'en': 'French Guiana',
		'fullname': 'French Guiana',
		'continent': 'sa',
		'es': 'Guayana Francesa',
		'pt': 'Guayana Francesa'
	},
	'GE': {
		'alpha3': 'GEO',
		'code': 'asgeo',
		'demonym': 'Georgian',
		'en': 'Georgia',
		'fullname': 'Georgia',
		'continent': 'as',
		'es': 'Georgia',
		'pt': 'Georgia'
	},
	'GD': {
		'alpha3': 'GRD',
		'code': 'nagrd',
		'demonym': 'Grenadian',
		'en': 'Grenada',
		'fullname': 'Grenada and Carriacuou',
		'continent': 'na',
		'es': 'Granada',
		'pt': 'Granada'
	},
	'GB': {
		'alpha3': 'GBR',
		'code': 'eugbr',
		'demonym': 'British',
		'en': 'United Kingdom',
		'fullname': 'United Kingdom',
		'continent': 'eu',
		'es': 'Reino Unido',
		'pt': 'Reino Unido'
	},
	'GA': {
		'alpha3': 'GAB',
		'code': 'afgab',
		'demonym': 'Gabonese',
		'en': 'Gabon',
		'fullname': 'Gabon Republic',
		'continent': 'af',
		'es': 'Gabón',
		'pt': 'Gabón'
	},
	'GN': {
		'alpha3': 'GIN',
		'code': 'afgin',
		'demonym': 'Guinean',
		'en': 'Guinea',
		'fullname': 'Guinea',
		'continent': 'af',
		'es': 'Guinea',
		'pt': 'Guinea'
	},
	'GM': {
		'alpha3': 'GMB',
		'code': 'afgmb',
		'demonym': 'Gambian',
		'en': 'Gambia',
		'fullname': 'Gambia',
		'continent': 'af',
		'es': 'Gambia',
		'pt': 'Gambia'
	},
	'GL': {
		'alpha3': 'GRL',
		'code': 'nagrl',
		'demonym': 'Greenlander',
		'en': 'Greenland',
		'fullname': 'Greenland',
		'continent': 'na',
		'es': 'Groenlandia',
		'pt': 'Groenlandia'
	},
	'GI': {
		'alpha3': 'GIB',
		'code': 'eugib',
		'demonym': 'Gibralterian',
		'en': 'Gibraltar',
		'fullname': 'Gibraltar',
		'continent': 'eu',
		'es': 'Gibraltar',
		'pt': 'Gibraltar'
	},
	'GH': {
		'alpha3': 'GHA',
		'code': 'afgha',
		'demonym': 'Ghanaian',
		'en': 'Ghana',
		'fullname': 'Ghana',
		'continent': 'af',
		'es': 'Ghana',
		'pt': 'Ghana'
	},
	'PR': {
		'alpha3': 'PRI',
		'code': 'napri',
		'demonym': 'Puerto Rican',
		'en': 'Puerto Rico',
		'fullname': 'Puerto Rico',
		'continent': 'na',
		'es': 'Puerto Rico',
		'pt': 'Puerto Rico'
	},
	'PS': {
		'alpha3': 'PSE',
		'code': 'aspse',
		'demonym': 'Palestinian',
		'en': 'Palestine',
		'fullname': 'Palestine',
		'continent': 'as',
		'es': 'Palestina',
		'pt': 'Palestina'
	},
	'PW': {
		'alpha3': 'PLW',
		'code': 'ocplw',
		'demonym': 'Palauan',
		'en': 'Palau',
		'fullname': 'Palau',
		'continent': 'oc',
		'es': 'Palau',
		'pt': 'Palau'
	},
	'PT': {
		'alpha3': 'PRT',
		'code': 'euprt',
		'demonym': 'Portuguese',
		'en': 'Portugal',
		'fullname': 'Portugal',
		'continent': 'eu',
		'es': 'Portugal',
		'pt': 'Portugal'
	},
	'PY': {
		'alpha3': 'PRY',
		'code': 'sapry',
		'demonym': 'Paraguayan',
		'en': 'Paraguay',
		'fullname': 'Paraguay',
		'continent': 'sa',
		'es': 'Paraguay',
		'pt': 'Paraguay'
	},
	'PA': {
		'alpha3': 'PAN',
		'code': 'napan',
		'demonym': 'Panamanian',
		'en': 'Panama',
		'fullname': 'Panama',
		'continent': 'na',
		'es': 'Panamá',
		'pt': 'Panamá'
	},
	'PF': {
		'alpha3': 'PYF',
		'code': 'ocpyf',
		'demonym': 'French Polynesian',
		'en': 'French Polynesia',
		'fullname': 'French Polynesia',
		'continent': 'oc',
		'es': 'Polinesia Francesa',
		'pt': 'Polinesia Francesa'
	},
	'PG': {
		'alpha3': 'PNG',
		'code': 'ocpng',
		'demonym': 'Papua New Guinean',
		'en': 'Papua New Guinea',
		'fullname': 'Papua New Guinea',
		'continent': 'oc',
		'es': 'Papúa Nueva Guinea',
		'pt': 'Papúa Nueva Guinea'
	},
	'PE': {
		'alpha3': 'PER',
		'code': 'saper',
		'demonym': 'Peruvian',
		'en': 'Peru',
		'fullname': 'Peru',
		'continent': 'sa',
		'es': 'Perú',
		'pt': 'Perú'
	},
	'PK': {
		'alpha3': 'PAK',
		'code': 'aspak',
		'demonym': 'Pakistani',
		'en': 'Pakistan',
		'fullname': 'Pakistan',
		'continent': 'as',
		'es': 'Pakistán',
		'pt': 'Pakistán'
	},
	'PH': {
		'alpha3': 'PHL',
		'code': 'asphl',
		'demonym': 'Filipino',
		'en': 'Philippines',
		'fullname': 'Philippines',
		'continent': 'as',
		'es': 'Filipinas',
		'pt': 'Filipinas'
	},
	'PL': {
		'alpha3': 'POL',
		'code': 'eupol',
		'demonym': 'Polish',
		'en': 'Poland',
		'fullname': 'Poland',
		'continent': 'eu',
		'es': 'Polonia',
		'pt': 'Polonia'
	},
	'PM': {
		'alpha3': 'SPM',
		'code': 'naspm',
		'demonym': 'Saint-Pierrais',
		'en': 'Saint Pierre and Miquelon',
		'fullname': 'St. Pierre and Miquelon',
		'continent': 'na',
		'es': 'San Pedro y Miquelón',
		'pt': 'San Pedro y Miquelón'
	},
	'ZM': {
		'alpha3': 'ZMB',
		'code': 'afzmb',
		'demonym': 'Zambian',
		'en': 'Zambia',
		'fullname': 'Zambia',
		'continent': 'af',
		'es': 'Zambia',
		'pt': 'Zambia'
	},
	'ZA': {
		'alpha3': 'ZAF',
		'code': 'afzaf',
		'demonym': 'South African',
		'en': 'South Africa',
		'fullname': 'South Africa',
		'continent': 'af',
		'es': 'Sudáfrica',
		'pt': 'Sudáfrica'
	},
	'ZW': {
		'alpha3': 'ZWE',
		'code': 'afzwe',
		'demonym': 'Zimbabwean',
		'en': 'Zimbabwe',
		'fullname': 'Zimbabwe',
		'continent': 'af',
		'es': 'Zimbabue',
		'pt': 'Zimbabue'
	},
	'ZR': {
		'alpha3': 'COD',
		'fullname': 'Dem. Republic of the Congo',
		'code': 'afcod',
		'demonym': 'Congolese',
		'continent': 'af'
	},
	'ME': {
		'alpha3': 'MNE',
		'code': 'eumne',
		'demonym': 'Montenegrin',
		'en': 'Montenegro',
		'fullname': 'Montenegro',
		'continent': 'eu',
		'es': 'Montenegro',
		'pt': 'Montenegro'
	},
	'MD': {
		'alpha3': 'MDA',
		'code': 'eumda',
		'demonym': 'Moldovan',
		'en': 'Moldova',
		'fullname': 'Moldova',
		'continent': 'eu',
		'es': 'Moldavia',
		'pt': 'Moldavia'
	},
	'MG': {
		'alpha3': 'MDG',
		'code': 'afmdg',
		'demonym': 'Malagasy',
		'en': 'Madagascar',
		'fullname': 'Madagascar',
		'continent': 'af',
		'es': 'Madagascar',
		'pt': 'Madagascar'
	},
	'MA': {
		'alpha3': 'MAR',
		'code': 'afmar',
		'demonym': 'Moroccan',
		'en': 'Morocco',
		'fullname': 'Morocco',
		'continent': 'af',
		'es': 'Marruecos',
		'pt': 'Marruecos'
	},
	'MC': {
		'alpha3': 'MCO',
		'code': 'eumco',
		'demonym': 'Monacan',
		'en': 'Monaco',
		'fullname': 'Monaco',
		'continent': 'eu',
		'es': 'Mónaco',
		'pt': 'Mónaco'
	},
	'MM': {
		'alpha3': 'MMR',
		'code': 'asmmr',
		'demonym': 'Myanmarese',
		'en': 'Myanmar',
		'fullname': 'Myanmar',
		'continent': 'as',
		'es': 'Birmania',
		'pt': 'Birmania'
	},
	'ML': {
		'alpha3': 'MLI',
		'code': 'afmli',
		'demonym': 'Malian',
		'en': 'Mali',
		'fullname': 'Mali Republic',
		'continent': 'af',
		'es': 'Mali',
		'pt': 'Mali'
	},
	'MO': {
		'alpha3': 'MAC',
		'code': 'asmac',
		'demonym': 'Macanese',
		'en': 'Macao',
		'fullname': 'Macau',
		'continent': 'as',
		'es': 'Macao',
		'pt': 'Macao'
	},
	'MN': {
		'alpha3': 'MNG',
		'code': 'asmng',
		'demonym': 'Mongolian',
		'en': 'Mongolia',
		'fullname': 'Mongolia',
		'continent': 'as',
		'es': 'Mongolia',
		'pt': 'Mongolia'
	},
	'MH': {
		'alpha3': 'MHL',
		'code': 'ocmhl',
		'demonym': 'Marshallese',
		'en': 'Marshall Islands',
		'fullname': 'Marshall Islands',
		'continent': 'oc',
		'es': 'Islas Marshall',
		'pt': 'Islas Marshall'
	},
	'MK': {
		'alpha3': 'MKD',
		'code': 'eumkd',
		'demonym': 'Macedonian',
		'en': 'Macedonia',
		'fullname': 'Macedonia',
		'continent': 'eu',
		'es': 'Macedonia',
		'pt': 'Macedonia'
	},
	'MU': {
		'alpha3': 'MUS',
		'code': 'afmus',
		'demonym': 'Mauritian',
		'en': 'Mauritius',
		'fullname': 'Mauritius',
		'continent': 'af',
		'es': 'Mauricio',
		'pt': 'Mauricio'
	},
	'MT': {
		'alpha3': 'MLT',
		'code': 'eumlt',
		'demonym': 'Maltese',
		'en': 'Malta',
		'fullname': 'Malta',
		'continent': 'eu',
		'es': 'Malta',
		'pt': 'Malta'
	},
	'MW': {
		'alpha3': 'MWI',
		'code': 'afmwi',
		'demonym': 'Malawian',
		'en': 'Malawi',
		'fullname': 'Malawi',
		'continent': 'af',
		'es': 'Malawi',
		'pt': 'Malawi'
	},
	'MV': {
		'alpha3': 'MDV',
		'code': 'asmdv',
		'demonym': 'Maldivan',
		'en': 'Maldives',
		'fullname': 'Maldives',
		'continent': 'as',
		'es': 'Islas Maldivas',
		'pt': 'Islas Maldivas'
	},
	'MQ': {
		'alpha3': 'MTQ',
		'code': 'namtq',
		'demonym': 'Martinican',
		'en': 'Martinique',
		'fullname': 'Martinique',
		'continent': 'na',
		'es': 'Martinica',
		'pt': 'Martinica'
	},
	'MP': {
		'alpha3': 'MNP',
		'code': 'ocmnp',
		'demonym': 'Northern Mariana Islander',
		'en': 'Northern Mariana Islands',
		'fullname': 'Saipan',
		'continent': 'oc',
		'es': 'Islas Marianas del Norte',
		'pt': 'Islas Marianas del Norte'
	},
	'MS': {
		'alpha3': 'MSR',
		'code': 'namsr',
		'demonym': 'Montserratian',
		'en': 'Montserrat',
		'fullname': 'Montserrat',
		'continent': 'na',
		'es': 'Montserrat',
		'pt': 'Montserrat'
	},
	'MR': {
		'alpha3': 'MRT',
		'code': 'afmrt',
		'demonym': 'Mauritanian',
		'en': 'Mauritania',
		'fullname': 'Mauritania',
		'continent': 'af',
		'es': 'Mauritania',
		'pt': 'Mauritania'
	},
	'MY': {
		'alpha3': 'MYS',
		'code': 'asmys',
		'demonym': 'Malaysian',
		'en': 'Malaysia',
		'fullname': 'Malaysia',
		'continent': 'as',
		'es': 'Malasia',
		'pt': 'Malasia'
	},
	'MX': {
		'alpha3': 'MEX',
		'code': 'namex',
		'demonym': 'Mexican',
		'en': 'Mexico',
		'fullname': 'Mexico',
		'continent': 'na',
		'es': 'México',
		'pt': 'México'
	},
	'MZ': {
		'alpha3': 'MOZ',
		'code': 'afmoz',
		'demonym': 'Mozambican',
		'en': 'Mozambique',
		'fullname': 'Mozambique',
		'continent': 'af',
		'es': 'Mozambique',
		'pt': 'Mozambique'
	},
	'FR': {
		'alpha3': 'FRA',
		'code': 'eufra',
		'demonym': 'French',
		'en': 'France',
		'fullname': 'France',
		'continent': 'eu',
		'es': 'Francia',
		'pt': 'Francia'
	},
	'FI': {
		'alpha3': 'FIN',
		'code': 'eufin',
		'demonym': 'Finnish',
		'en': 'Finland',
		'fullname': 'Finland',
		'continent': 'eu',
		'es': 'Finlandia',
		'pt': 'Finlandia'
	},
	'FJ': {
		'alpha3': 'FJI',
		'code': 'ocfji',
		'demonym': 'Fijian',
		'en': 'Fiji',
		'fullname': 'Fiji Islands',
		'continent': 'oc',
		'es': 'Fiyi',
		'pt': 'Fiyi'
	},
	'FK': {
		'alpha3': 'FLK',
		'code': 'saflk',
		'demonym': 'Falkland Islander',
		'en': 'Falkland Islands (Malvinas)',
		'fullname': 'Falkland Islands',
		'continent': 'sa',
		'es': 'Islas Malvinas',
		'pt': 'Islas Malvinas'
	},
	'FM': {
		'alpha3': 'FSM',
		'code': 'ocfsm',
		'demonym': 'Micronesian',
		'en': 'Estados Federados de',
		'fullname': 'Federated States of Micronesia',
		'continent': 'oc',
		'es': 'Micronesia',
		'pt': 'Micronesia'
	},
	'FO': {
		'alpha3': 'FRO',
		'code': 'eufro',
		'demonym': 'Faroese',
		'en': 'Faroe Islands',
		'fullname': 'Faroe Islands',
		'continent': 'eu',
		'es': 'Islas Feroe',
		'pt': 'Islas Feroe'
	},
	'CK': {
		'alpha3': 'COK',
		'code': 'occok',
		'demonym': 'Cook Islander',
		'en': 'Cook Islands',
		'fullname': 'Cook Islands',
		'continent': 'oc',
		'es': 'Islas Cook',
		'pt': 'Islas Cook'
	},
	'CI': {
		'alpha3': 'CIV',
		'code': 'afciv',
		'demonym': 'Ivorian',
		'en': 'Ivory Coast',
		'fullname': 'Ivory Coast',
		'continent': 'af',
		'es': 'Costa de Marfil',
		'pt': 'Costa de Marfil'
	},
	'CH': {
		'alpha3': 'CHE',
		'code': 'euche',
		'demonym': 'Swiss',
		'en': 'Switzerland',
		'fullname': 'Switzerland',
		'continent': 'eu',
		'es': 'Suiza',
		'pt': 'Suiza'
	},
	'CO': {
		'alpha3': 'COL',
		'code': 'sacol',
		'demonym': 'Colombian',
		'en': 'Colombia',
		'fullname': 'Colombia',
		'continent': 'sa',
		'es': 'Colombia',
		'pt': 'Colombia'
	},
	'CN': {
		'alpha3': 'CHN',
		'code': 'aschn',
		'demonym': 'Chinese',
		'en': 'China',
		'fullname': 'China',
		'continent': 'as',
		'es': 'China',
		'pt': 'China'
	},
	'CM': {
		'alpha3': 'CMR',
		'code': 'afcmr',
		'demonym': 'Cameroonian',
		'en': 'Cameroon',
		'fullname': 'Cameroon',
		'continent': 'af',
		'es': 'Camerún',
		'pt': 'Camerún'
	},
	'CL': {
		'alpha3': 'CHL',
		'code': 'sachl',
		'demonym': 'Chilean',
		'en': 'Chile',
		'fullname': 'Chile',
		'continent': 'sa',
		'es': 'Chile',
		'pt': 'Chile'
	},
	'CC': {
		'alpha3': 'CCK',
		'code': 'ascck',
		'demonym': 'Cocossian',
		'en': 'Cocos (Keeling) Islands',
		'fullname': 'Cocos Islands',
		'continent': 'as',
		'es': 'Islas Cocos (Keeling)',
		'pt': 'Islas Cocos (Keeling)'
	},
	'CA': {
		'alpha3': 'CAN',
		'code': 'nacan',
		'demonym': 'Canadian',
		'en': 'Canada',
		'fullname': 'Canada',
		'continent': 'na',
		'es': 'Canadá',
		'pt': 'Canadá'
	},
	'CG': {
		'alpha3': 'COG',
		'code': 'afcog',
		'demonym': 'Congolese',
		'en': 'Congo',
		'fullname': 'Congo',
		'continent': 'af',
		'es': 'Congo',
		'pt': 'Congo'
	},
	'CF': {
		'alpha3': 'CAF',
		'code': 'afcaf',
		'demonym': 'Central African',
		'en': 'Central African Republic',
		'fullname': 'Central African Republic',
		'continent': 'af',
		'es': 'República Centroafricana',
		'pt': 'República Centroafricana'
	},
	'CD': {
		'alpha3': 'COD',
		'code': 'afcod',
		'demonym': 'Congolese',
		'en': 'Congo',
		'fullname': 'Dem. Republic of the Congo',
		'continent': 'af',
		'es': 'Congo',
		'pt': 'Congo'
	},
	'CZ': {
		'alpha3': 'CZE',
		'code': 'eucze',
		'demonym': 'Czech',
		'en': 'Czech Republic',
		'fullname': 'Czech Republic',
		'continent': 'eu',
		'es': 'República Checa',
		'pt': 'República Checa'
	},
	'CY': {
		'alpha3': 'CYP',
		'code': 'eucyp',
		'demonym': 'Cypriot',
		'en': 'Cyprus',
		'fullname': 'Cyprus',
		'continent': 'eu',
		'es': 'Chipre',
		'pt': 'Chipre'
	},
	'CX': {
		'alpha3': 'CXR',
		'code': 'ascxr',
		'demonym': 'Christmas Islander',
		'en': 'Christmas Island',
		'fullname': 'Christmas Island',
		'continent': 'as',
		'es': 'Isla de Navidad',
		'pt': 'Isla de Navidad'
	},
	'CR': {
		'alpha3': 'CRI',
		'code': 'nacri',
		'demonym': 'Costa Rican',
		'en': 'Costa Rica',
		'fullname': 'Costa Rica',
		'continent': 'na',
		'es': 'Costa Rica',
		'pt': 'Costa Rica'
	},
	'CW': {
		'alpha3': 'ANT',
		'fullname': 'Netherlands Antilles',
		'code': 'naant',
		'demonym': 'Cura\\u00e7aoan',
		'continent': 'na'
	},
	'CV': {
		'alpha3': 'CPV',
		'code': 'afcpv',
		'demonym': 'Cape Verdean',
		'en': 'Cape Verde',
		'fullname': 'Cape Verde Islands',
		'continent': 'af',
		'es': 'Cabo Verde',
		'pt': 'Cabo Verde'
	},
	'CU': {
		'alpha3': 'CUB',
		'code': 'nacub',
		'demonym': 'Cuban',
		'en': 'Cuba',
		'fullname': 'Cuba',
		'continent': 'na',
		'es': 'Cuba',
		'pt': 'Cuba'
	},
	'SZ': {
		'alpha3': 'SWZ',
		'code': 'afswz',
		'demonym': 'Swazi',
		'en': 'Swaziland',
		'fullname': 'Swaziland',
		'continent': 'af',
		'es': 'Swazilandia',
		'pt': 'Swazilandia'
	},
	'SY': {
		'alpha3': 'SYR',
		'code': 'assyr',
		'demonym': 'Syrian',
		'en': 'Syria',
		'fullname': 'Syria',
		'continent': 'as',
		'es': 'Siria',
		'pt': 'Siria'
	},
	'SX': {
		'alpha3': 'SXM',
		'fullname': 'Sint Maarten',
		'code': 'nasxm',
		'demonym': 'Saint Martin (Netherlands)',
		'continent': 'na'
	},
	'SS': {
		'alpha3': 'SSD',
		'fullname': 'South Sudan',
		'code': 'afssd',
		'demonym': 'Sudanese',
		'continent': 'af'
	},
	'SR': {
		'alpha3': 'SUR',
		'code': 'sasur',
		'demonym': 'Surinamer',
		'en': 'Suriname',
		'fullname': 'Suriname',
		'continent': 'sa',
		'es': 'Surinám',
		'pt': 'Surinám'
	},
	'SV': {
		'alpha3': 'SLV',
		'code': 'naslv',
		'demonym': 'Salvadorean',
		'en': 'El Salvador',
		'fullname': 'El Salvador',
		'continent': 'na',
		'es': 'El Salvador',
		'pt': 'El Salvador'
	},
	'ST': {
		'alpha3': 'STP',
		'code': 'afstp',
		'demonym': 'S\\u00e3o Tomean',
		'en': 'Sao Tome and Principe',
		'fullname': 'Sao Tome',
		'continent': 'af',
		'es': 'Santo Tomé y Príncipe',
		'pt': 'Santo Tomé y Príncipe'
	},
	'SK': {
		'alpha3': 'SVK',
		'code': 'eusvk',
		'demonym': 'Slovakian',
		'en': 'Slovakia',
		'fullname': 'Slovakia',
		'continent': 'eu',
		'es': 'Eslovaquia',
		'pt': 'Eslovaquia'
	},
	'SI': {
		'alpha3': 'SVN',
		'code': 'eusvn',
		'demonym': 'Slovenian',
		'en': 'Slovenia',
		'fullname': 'Slovenia',
		'continent': 'eu',
		'es': 'Eslovenia',
		'pt': 'Eslovenia'
	},
	'SH': {
		'alpha3': 'SHN',
		'code': 'afshn',
		'demonym': 'Saint Helenian',
		'en': 'Ascensión y Tristán de Acuña',
		'fullname': 'St. Helena',
		'continent': 'af',
		'es': 'Santa Elena',
		'pt': 'Santa Elena'
	},
	'SO': {
		'alpha3': 'SOM',
		'code': 'afsom',
		'demonym': 'Somali',
		'en': 'Somalia',
		'fullname': 'Somalia Republic',
		'continent': 'af',
		'es': 'Somalia',
		'pt': 'Somalia'
	},
	'SN': {
		'alpha3': 'SEN',
		'code': 'afsen',
		'demonym': 'Senegalese',
		'en': 'Senegal',
		'fullname': 'Senegal Republic',
		'continent': 'af',
		'es': 'Senegal',
		'pt': 'Senegal'
	},
	'SM': {
		'alpha3': 'SMR',
		'code': 'eusmr',
		'demonym': 'Sanmarinese',
		'en': 'San Marino',
		'fullname': 'San Marino',
		'continent': 'eu',
		'es': 'San Marino',
		'pt': 'San Marino'
	},
	'SL': {
		'alpha3': 'SLE',
		'code': 'afsle',
		'demonym': 'Sierra Leonean',
		'en': 'Sierra Leone',
		'fullname': 'Sierra Leone',
		'continent': 'af',
		'es': 'Sierra Leona',
		'pt': 'Sierra Leona'
	},
	'SC': {
		'alpha3': 'SYC',
		'code': 'afsyc',
		'demonym': 'Seychellois',
		'en': 'Seychelles',
		'fullname': 'Seychelles',
		'continent': 'af',
		'es': 'Seychelles',
		'pt': 'Seychelles'
	},
	'SB': {
		'alpha3': 'SLB',
		'code': 'ocslb',
		'demonym': 'Solomon Islander',
		'en': 'Solomon Islands',
		'fullname': 'Solomon Islands',
		'continent': 'oc',
		'es': 'Islas Salomón',
		'pt': 'Islas Salomón'
	},
	'SA': {
		'alpha3': 'SAU',
		'code': 'assau',
		'demonym': 'Saudi Arabian',
		'en': 'Saudi Arabia',
		'fullname': 'Saudi Arabia',
		'continent': 'as',
		'es': 'Arabia Saudita',
		'pt': 'Arabia Saudita'
	},
	'SG': {
		'alpha3': 'SGP',
		'code': 'assgp',
		'demonym': 'Singaporean',
		'en': 'Singapore',
		'fullname': 'Singapore',
		'continent': 'as',
		'es': 'Singapur',
		'pt': 'Singapur'
	},
	'SE': {
		'alpha3': 'BES',
		'code': 'nabes',
		'demonym': 'Swedish',
		'en': 'Sweden',
		'fullname': 'Sint Eustatius',
		'continent': 'na',
		'es': 'Suecia',
		'pt': 'Suecia'
	},
	'SD': {
		'alpha3': 'SDN',
		'code': 'afsdn',
		'demonym': 'Sudanese',
		'en': 'Sudan',
		'fullname': 'Sudan',
		'continent': 'af',
		'es': 'Sudán',
		'pt': 'Sudán'
	},
	'YE': {
		'alpha3': 'YEM',
		'code': 'asyem',
		'demonym': 'Yemeni',
		'en': 'Yemen',
		'fullname': 'Yemen',
		'continent': 'as',
		'es': 'Yemen',
		'pt': 'Yemen'
	},
	'YT': {
		'alpha3': 'MYT',
		'code': 'afmyt',
		'demonym': 'Mahoran',
		'en': 'Mayotte',
		'fullname': 'Mayotte Island',
		'continent': 'af',
		'es': 'Mayotte',
		'pt': 'Mayotte'
	},
	'LB': {
		'alpha3': 'LBN',
		'code': 'aslbn',
		'demonym': 'Lebanese',
		'en': 'Lebanon',
		'fullname': 'Lebanon',
		'continent': 'as',
		'es': 'Líbano',
		'pt': 'Líbano'
	},
	'LC': {
		'alpha3': 'LCA',
		'code': 'nalca',
		'demonym': 'Saint Lucian',
		'en': 'Saint Lucia',
		'fullname': 'St. Lucia',
		'continent': 'na',
		'es': 'Santa Lucía',
		'pt': 'Santa Lucía'
	},
	'LA': {
		'alpha3': 'LAO',
		'code': 'aslao',
		'demonym': 'Laotian',
		'en': 'Laos',
		'fullname': 'Laos',
		'continent': 'as',
		'es': 'Laos',
		'pt': 'Laos'
	},
	'LK': {
		'alpha3': 'LKA',
		'code': 'aslka',
		'demonym': 'Sri Lankan',
		'en': 'Sri Lanka',
		'fullname': 'Sri Lanka',
		'continent': 'as',
		'es': 'Sri lanka',
		'pt': 'Sri lanka'
	},
	'LI': {
		'alpha3': 'LIE',
		'code': 'eulie',
		'demonym': 'Liechtensteiner',
		'en': 'Liechtenstein',
		'fullname': 'Liechtenstein',
		'continent': 'eu',
		'es': 'Liechtenstein',
		'pt': 'Liechtenstein'
	},
	'LV': {
		'alpha3': 'LVA',
		'code': 'eulva',
		'demonym': 'Latvian',
		'en': 'Latvia',
		'fullname': 'Latvia',
		'continent': 'eu',
		'es': 'Letonia',
		'pt': 'Letonia'
	},
	'LT': {
		'alpha3': 'LTU',
		'code': 'eultu',
		'demonym': 'Lithunian',
		'en': 'Lithuania',
		'fullname': 'Lithuania',
		'continent': 'eu',
		'es': 'Lituania',
		'pt': 'Lituania'
	},
	'LU': {
		'alpha3': 'LUX',
		'code': 'eulux',
		'demonym': 'Luxembourger',
		'en': 'Luxembourg',
		'fullname': 'Luxembourg',
		'continent': 'eu',
		'es': 'Luxemburgo',
		'pt': 'Luxemburgo'
	},
	'LR': {
		'alpha3': 'LBR',
		'code': 'aflbr',
		'demonym': 'Liberian',
		'en': 'Liberia',
		'fullname': 'Liberia',
		'continent': 'af',
		'es': 'Liberia',
		'pt': 'Liberia'
	},
	'LS': {
		'alpha3': 'LSO',
		'code': 'aflso',
		'demonym': 'Mosotho',
		'en': 'Lesotho',
		'fullname': 'Lesotho',
		'continent': 'af',
		'es': 'Lesoto',
		'pt': 'Lesoto'
	},
	'LY': {
		'alpha3': 'LBY',
		'code': 'aflby',
		'demonym': 'Libyan',
		'en': 'Libya',
		'fullname': 'Libya',
		'continent': 'af',
		'es': 'Libia',
		'pt': 'Libia'
	},
	'VA': {
		'alpha3': 'VAT',
		'code': 'euvat',
		'demonym': 'Vatican',
		'en': 'Vatican City State',
		'fullname': 'Vatican city',
		'continent': 'eu',
		'es': 'Ciudad del Vaticano',
		'pt': 'Ciudad del Vaticano'
	},
	'VC': {
		'alpha3': 'VCT',
		'code': 'navct',
		'demonym': 'Saint Vincentian',
		'en': 'Saint Vincent and the Grenadines',
		'fullname': 'St. Vincent',
		'continent': 'na',
		'es': 'San Vicente y las Granadinas',
		'pt': 'San Vicente y las Granadinas'
	},
	'VE': {
		'alpha3': 'VEN',
		'code': 'saven',
		'demonym': 'Venezuelan',
		'en': 'Venezuela',
		'fullname': 'Venezuela',
		'continent': 'sa',
		'es': 'Venezuela',
		'pt': 'Venezuela'
	},
	'VG': {
		'alpha3': 'VGB',
		'code': 'navgb',
		'demonym': 'Virgin Islander',
		'en': 'Virgin Islands',
		'fullname': 'British Virgin Islands',
		'continent': 'na',
		'es': 'Islas Vírgenes Británicas',
		'pt': 'Islas Vírgenes Británicas'
	},
	'IQ': {
		'alpha3': 'IRQ',
		'code': 'asirq',
		'demonym': 'Iraqi',
		'en': 'Iraq',
		'fullname': 'Iraq',
		'continent': 'as',
		'es': 'Irak',
		'pt': 'Irak'
	},
	'VI': {
		'alpha3': 'VIR',
		'code': 'navir',
		'demonym': 'Virgin Islander',
		'en': 'United States Virgin Islands',
		'fullname': 'US Virgin Islands',
		'continent': 'na',
		'es': 'Islas Vírgenes de los Estados Unidos',
		'pt': 'Islas Vírgenes de los Estados Unidos'
	},
	'IS': {
		'alpha3': 'ISL',
		'code': 'euisl',
		'demonym': 'Iceland',
		'en': 'Iceland',
		'fullname': 'Iceland',
		'continent': 'eu',
		'es': 'Islandia',
		'pt': 'Islandia'
	},
	'IR': {
		'alpha3': 'IRN',
		'code': 'asirn',
		'demonym': 'Iranian',
		'en': 'Iran',
		'fullname': 'Iran',
		'continent': 'as',
		'es': 'Irán',
		'pt': 'Irán'
	},
	'IT': {
		'alpha3': 'ITA',
		'code': 'euita',
		'demonym': 'Italian',
		'en': 'Italy',
		'fullname': 'Italy',
		'continent': 'eu',
		'es': 'Italia',
		'pt': 'Italia'
	},
	'VN': {
		'alpha3': 'VNM',
		'code': 'asvnm',
		'demonym': 'Vietnamese',
		'en': 'Vietnam',
		'fullname': 'Vietnam',
		'continent': 'as',
		'es': 'Vietnam',
		'pt': 'Vietnam'
	},
	'IM': {
		'alpha3': 'IMN',
		'code': 'euimn',
		'demonym': 'Manx',
		'en': 'Isle of Man',
		'fullname': 'Isle of Man',
		'continent': 'eu',
		'es': 'Isla de Man',
		'pt': 'Isla de Man'
	},
	'IL': {
		'alpha3': 'ISR',
		'code': 'asisr',
		'demonym': 'Israeli',
		'en': 'Israel',
		'fullname': 'Israel',
		'continent': 'as',
		'es': 'Israel',
		'pt': 'Israel'
	},
	'IO': {
		'alpha3': 'IOT',
		'code': 'asiot',
		'demonym': 'British Indian Ocean Territory',
		'en': 'British Indian Ocean Territory',
		'fullname': 'Diego Garcia',
		'continent': 'as',
		'es': 'Territorio Británico del Océano Índico',
		'pt': 'Territorio Británico del Océano Índico'
	},
	'IN': {
		'alpha3': 'IND',
		'code': 'asind',
		'demonym': 'Indian',
		'en': 'India',
		'fullname': 'India',
		'continent': 'as',
		'es': 'India',
		'pt': 'India'
	},
	'IE': {
		'alpha3': 'IRL',
		'code': 'euirl',
		'demonym': 'Irish',
		'en': 'Ireland',
		'fullname': 'Ireland',
		'continent': 'eu',
		'es': 'Irlanda',
		'pt': 'Irlanda'
	},
	'ID': {
		'alpha3': 'IDN',
		'code': 'asidn',
		'demonym': 'Indonesian',
		'en': 'Indonesia',
		'fullname': 'Indonesia',
		'continent': 'as',
		'es': 'Indonesia',
		'pt': 'Indonesia'
	},
	'BD': {
		'alpha3': 'BGD',
		'code': 'asbgd',
		'demonym': 'Bangladeshi',
		'en': 'Bangladesh',
		'fullname': 'Bangladesh',
		'continent': 'as',
		'es': 'Bangladesh',
		'pt': 'Bangladesh'
	},
	'BE': {
		'alpha3': 'BEL',
		'code': 'eubel',
		'demonym': 'Belgian',
		'en': 'Belgium',
		'fullname': 'Belgium',
		'continent': 'eu',
		'es': 'Bélgica',
		'pt': 'Bélgica'
	},
	'BF': {
		'alpha3': 'BFA',
		'code': 'afbfa',
		'demonym': 'Burkinabe',
		'en': 'Burkina Faso',
		'fullname': 'Burkina Faso',
		'continent': 'af',
		'es': 'Burkina Faso',
		'pt': 'Burkina Faso'
	},
	'BG': {
		'alpha3': 'BGR',
		'code': 'eubgr',
		'demonym': 'Bulgarian',
		'en': 'Bulgaria',
		'fullname': 'Bulgaria',
		'continent': 'eu',
		'es': 'Bulgaria',
		'pt': 'Bulgaria'
	},
	'BA': {
		'alpha3': 'BIH',
		'code': 'eubih',
		'demonym': 'Bosnian',
		'en': 'Bosnia and Herzegovina',
		'fullname': 'Bosnia and Herzegovina',
		'continent': 'eu',
		'es': 'Bosnia y Herzegovina',
		'pt': 'Bosnia y Herzegovina'
	},
	'BB': {
		'alpha3': 'BRB',
		'code': 'nabrb',
		'demonym': 'Barbadian',
		'en': 'Barbados',
		'fullname': 'Barbados',
		'continent': 'na',
		'es': 'Barbados',
		'pt': 'Barbados'
	},
	'BM': {
		'alpha3': 'BMU',
		'code': 'nabmu',
		'demonym': 'Bermudan',
		'en': 'Bermuda Islands',
		'fullname': 'Bermuda',
		'continent': 'na',
		'es': 'Islas Bermudas',
		'pt': 'Islas Bermudas'
	},
	'BN': {
		'alpha3': 'BRN',
		'code': 'asbrn',
		'demonym': 'Bruneian',
		'en': 'Brunei',
		'fullname': 'Brunei',
		'continent': 'as',
		'es': 'Brunéi',
		'pt': 'Brunéi'
	},
	'BO': {
		'alpha3': 'BOL',
		'code': 'sabol',
		'demonym': 'Bolivian',
		'en': 'Bolivia',
		'fullname': 'Bolivia',
		'continent': 'sa',
		'es': 'Bolivia',
		'pt': 'Bolivia'
	},
	'BH': {
		'alpha3': 'BHR',
		'code': 'asbhr',
		'demonym': 'Bahrainian',
		'en': 'Bahrain',
		'fullname': 'Bahrain',
		'continent': 'as',
		'es': 'Bahrein',
		'pt': 'Bahrein'
	},
	'BI': {
		'alpha3': 'BDI',
		'code': 'afbdi',
		'demonym': 'Burundian',
		'en': 'Burundi',
		'fullname': 'Burundi',
		'continent': 'af',
		'es': 'Burundi',
		'pt': 'Burundi'
	},
	'BJ': {
		'alpha3': 'BEN',
		'code': 'afben',
		'demonym': 'Beninese',
		'en': 'Benin',
		'fullname': 'Benin',
		'continent': 'af',
		'es': 'Benín',
		'pt': 'Benín'
	},
	'BT': {
		'alpha3': 'BTN',
		'code': 'asbtn',
		'demonym': 'Bhutanese',
		'en': 'Bhutan',
		'fullname': 'Bhutan',
		'continent': 'as',
		'es': 'Bhután',
		'pt': 'Bhután'
	},
	'BW': {
		'alpha3': 'BWA',
		'code': 'afbwa',
		'demonym': 'Motswana',
		'en': 'Botswana',
		'fullname': 'Botswana',
		'continent': 'af',
		'es': 'Botsuana',
		'pt': 'Botsuana'
	},
	'BQ': {
		'alpha3': 'BES',
		'fullname': 'Bonaire',
		'code': 'nabes',
		'demonym': '',
		'continent': 'na'
	},
	'BR': {
		'alpha3': 'BRA',
		'code': 'sabra',
		'demonym': 'Brazilian',
		'en': 'Brazil',
		'fullname': 'Brazil',
		'continent': 'sa',
		'es': 'Brasil',
		'pt': 'Brasil'
	},
	'BS': {
		'alpha3': 'BHS',
		'code': 'nabhs',
		'demonym': 'Bahameese',
		'en': 'Bahamas',
		'fullname': 'Bahamas',
		'continent': 'na',
		'es': 'Bahamas',
		'pt': 'Bahamas'
	},
	'BY': {
		'alpha3': 'BLR',
		'code': 'eublr',
		'demonym': 'Belarusian',
		'en': 'Belarus',
		'fullname': 'Belarus',
		'continent': 'eu',
		'es': 'Bielorrusia',
		'pt': 'Bielorrusia'
	},
	'BZ': {
		'alpha3': 'BLZ',
		'code': 'nablz',
		'demonym': 'Belizean',
		'en': 'Belize',
		'fullname': 'Belize',
		'continent': 'na',
		'es': 'Belice',
		'pt': 'Belice'
	},
	'RU': {
		'alpha3': 'RUS',
		'code': 'eurus',
		'demonym': 'Russian',
		'en': 'Russia',
		'fullname': 'Russia',
		'continent': 'eu',
		'es': 'Rusia',
		'pt': 'Rusia'
	},
	'RW': {
		'alpha3': 'RWA',
		'code': 'afrwa',
		'demonym': 'Rwandan',
		'en': 'Rwanda',
		'fullname': 'Rwanda',
		'continent': 'af',
		'es': 'Ruanda',
		'pt': 'Ruanda'
	},
	'RS': {
		'alpha3': 'RSB',
		'code': 'eursb',
		'demonym': 'Serbian',
		'en': 'Serbia',
		'fullname': 'Yugoslavia',
		'continent': 'eu',
		'es': 'Serbia',
		'pt': 'Serbia'
	},
	'RE': {
		'alpha3': 'REU',
		'code': 'afreu',
		'demonym': '',
		'en': 'Réunion',
		'fullname': 'Reunion Island',
		'continent': 'af',
		'es': 'Reunión',
		'pt': 'Reunión'
	},
	'RO': {
		'alpha3': 'ROU',
		'code': 'eurou',
		'demonym': 'Romanian',
		'en': 'Romania',
		'fullname': 'Romania',
		'continent': 'eu',
		'es': 'Rumanía',
		'pt': 'Rumanía'
	},
	'OM': {
		'alpha3': 'OMN',
		'code': 'asomn',
		'demonym': 'Omani',
		'en': 'Oman',
		'fullname': 'Oman',
		'continent': 'as',
		'es': 'Omán',
		'pt': 'Omán'
	},
	'HR': {
		'alpha3': 'HRV',
		'code': 'euhrv',
		'demonym': 'Croatian',
		'en': 'Croatia',
		'fullname': 'Croatia',
		'continent': 'eu',
		'es': 'Croacia',
		'pt': 'Croacia'
	},
	'HT': {
		'alpha3': 'HTI',
		'code': 'nahti',
		'demonym': 'Haitian',
		'en': 'Haiti',
		'fullname': 'Haiti',
		'continent': 'na',
		'es': 'Haití',
		'pt': 'Haití'
	},
	'HU': {
		'alpha3': 'HUN',
		'code': 'euhun',
		'demonym': 'Hungarian',
		'en': 'Hungary',
		'fullname': 'Hungary',
		'continent': 'eu',
		'es': 'Hungría',
		'pt': 'Hungría'
	},
	'HK': {
		'alpha3': 'HKG',
		'code': 'ashkg',
		'demonym': 'Hong Konger',
		'en': 'Hong Kong',
		'fullname': 'Hong Kong',
		'continent': 'as',
		'es': 'Hong kong',
		'pt': 'Hong kong'
	},
	'HN': {
		'alpha3': 'HND',
		'code': 'nahnd',
		'demonym': 'Honduran',
		'en': 'Honduras',
		'fullname': 'Honduras',
		'continent': 'na',
		'es': 'Honduras',
		'pt': 'Honduras'
	},
	'EE': {
		'alpha3': 'EST',
		'code': 'euest',
		'demonym': 'Estonian',
		'en': 'Estonia',
		'fullname': 'Estonia',
		'continent': 'eu',
		'es': 'Estonia',
		'pt': 'Estonia'
	},
	'EG': {
		'alpha3': 'EGY',
		'code': 'afegy',
		'demonym': 'Egyptian',
		'en': 'Egypt',
		'fullname': 'Egypt',
		'continent': 'af',
		'es': 'Egipto',
		'pt': 'Egipto'
	},
	'EC': {
		'alpha3': 'ECU',
		'code': 'saecu',
		'demonym': 'Ecuadorean',
		'en': 'Ecuador',
		'fullname': 'Ecuador',
		'continent': 'sa',
		'es': 'Ecuador',
		'pt': 'Ecuador'
	},
	'ET': {
		'alpha3': 'ETH',
		'code': 'afeth',
		'demonym': 'Ethiopian',
		'en': 'Ethiopia',
		'fullname': 'Ethiopia',
		'continent': 'af',
		'es': 'Etiopía',
		'pt': 'Etiopía'
	},
	'ES': {
		'alpha3': 'ESP',
		'code': 'euesp',
		'demonym': 'Spanish',
		'en': 'Spain',
		'fullname': 'Spain',
		'continent': 'eu',
		'es': 'España',
		'pt': 'España'
	},
	'ER': {
		'alpha3': 'ERI',
		'code': 'aferi',
		'demonym': 'Eritrean',
		'en': 'Eritrea',
		'fullname': 'Eritrea',
		'continent': 'af',
		'es': 'Eritrea',
		'pt': 'Eritrea'
	},
	'UY': {
		'alpha3': 'URY',
		'code': 'saury',
		'demonym': 'Uruguayan',
		'en': 'Uruguay',
		'fullname': 'Uruguay',
		'continent': 'sa',
		'es': 'Uruguay',
		'pt': 'Uruguay'
	},
	'UZ': {
		'alpha3': 'UZB',
		'code': 'asuzb',
		'demonym': 'Uzbekistani',
		'en': 'Uzbekistan',
		'fullname': 'Uzbekistan',
		'continent': 'as',
		'es': 'Uzbekistán',
		'pt': 'Uzbekistán'
	},
	'US': {
		'alpha3': 'USA',
		'code': 'nausa',
		'demonym': 'American',
		'en': 'United States of America',
		'fullname': 'United States',
		'continent': 'na',
		'es': 'Estados Unidos de América',
		'pt': 'Estados Unidos de América'
	},
	'UM': {
		'alpha3': 'UMI',
		'code': 'ocumi',
		'demonym': 'United States Minor Outlying Islands',
		'en': 'United States Minor Outlying Islands',
		'fullname': 'Wake Island',
		'continent': 'oc',
		'es': 'Islas Ultramarinas Menores de Estados Unidos',
		'pt': 'Islas Ultramarinas Menores de Estados Unidos'
	},
	'UG': {
		'alpha3': 'UGA',
		'code': 'afuga',
		'demonym': 'Ugandan',
		'en': 'Uganda',
		'fullname': 'Uganda',
		'continent': 'af',
		'es': 'Uganda',
		'pt': 'Uganda'
	},
	'UA': {
		'alpha3': 'UKR',
		'code': 'euukr',
		'demonym': 'Ukrainian',
		'en': 'Ukraine',
		'fullname': 'Ukraine',
		'continent': 'eu',
		'es': 'Ucrania',
		'pt': 'Ucrania'
	},
	'VU': {
		'alpha3': 'VUT',
		'code': 'ocvut',
		'demonym': 'Ni-Vanuatu',
		'en': 'Vanuatu',
		'fullname': 'Vanuatu',
		'continent': 'oc',
		'es': 'Vanuatu',
		'pt': 'Vanuatu'
	},
	'NI': {
		'alpha3': 'NIC',
		'code': 'nanic',
		'demonym': 'Nicaraguan',
		'en': 'Nicaragua',
		'fullname': 'Nicaragua',
		'continent': 'na',
		'es': 'Nicaragua',
		'pt': 'Nicaragua'
	},
	'NL': {
		'alpha3': 'NLD',
		'code': 'eunld',
		'demonym': 'Dutch',
		'en': 'Netherlands',
		'fullname': 'Netherlands',
		'continent': 'eu',
		'es': 'Países Bajos',
		'pt': 'Países Bajos'
	},
	'NO': {
		'alpha3': 'NOR',
		'code': 'eunor',
		'demonym': 'Norwegian',
		'en': 'Norway',
		'fullname': 'Norway',
		'continent': 'eu',
		'es': 'Noruega',
		'pt': 'Noruega'
	},
	'NA': {
		'alpha3': 'NAM',
		'code': 'afnam',
		'demonym': 'Namibian',
		'en': 'Namibia',
		'fullname': 'Namibia',
		'continent': 'af',
		'es': 'Namibia',
		'pt': 'Namibia'
	},
	'NC': {
		'alpha3': 'NCL',
		'code': 'ocncl',
		'demonym': 'New Caledonian',
		'en': 'New Caledonia',
		'fullname': 'New Caledonia',
		'continent': 'oc',
		'es': 'Nueva Caledonia',
		'pt': 'Nueva Caledonia'
	},
	'NE': {
		'alpha3': 'NER',
		'code': 'afner',
		'demonym': 'Nigerien',
		'en': 'Niger',
		'fullname': 'Niger Republic',
		'continent': 'af',
		'es': 'Niger',
		'pt': 'Niger'
	},
	'NF': {
		'alpha3': 'NFK',
		'code': 'ocnfk',
		'demonym': 'Norfolk Islander',
		'en': 'Norfolk Island',
		'fullname': 'Norfolk Island',
		'continent': 'oc',
		'es': 'Isla Norfolk',
		'pt': 'Isla Norfolk'
	},
	'NG': {
		'alpha3': 'NGA',
		'code': 'afnga',
		'demonym': 'Nigerian',
		'en': 'Nigeria',
		'fullname': 'Nigeria',
		'continent': 'af',
		'es': 'Nigeria',
		'pt': 'Nigeria'
	},
	'NZ': {
		'alpha3': 'NZL',
		'code': 'ocnzl',
		'demonym': 'New Zealander',
		'en': 'New Zealand',
		'fullname': 'New Zealand',
		'continent': 'oc',
		'es': 'Nueva Zelanda',
		'pt': 'Nueva Zelanda'
	},
	'NP': {
		'alpha3': 'NPL',
		'code': 'asnpl',
		'demonym': 'Nepalese',
		'en': 'Nepal',
		'fullname': 'Nepal',
		'continent': 'as',
		'es': 'Nepal',
		'pt': 'Nepal'
	},
	'NR': {
		'alpha3': 'NRU',
		'code': 'ocnru',
		'demonym': 'Nauruan',
		'en': 'Nauru',
		'fullname': 'Nauru',
		'continent': 'oc',
		'es': 'Nauru',
		'pt': 'Nauru'
	},
	'NU': {
		'alpha3': 'NIU',
		'code': 'ocniu',
		'demonym': 'Niuean',
		'en': 'Niue',
		'fullname': 'Niue',
		'continent': 'oc',
		'es': 'Niue',
		'pt': 'Niue'
	},
	'KG': {
		'alpha3': 'KGZ',
		'code': 'askgz',
		'demonym': 'Kyrgyzstani',
		'en': 'Kyrgyzstan',
		'fullname': 'Kyrgyzstan',
		'continent': 'as',
		'es': 'Kirgizstán',
		'pt': 'Kirgizstán'
	},
	'KE': {
		'alpha3': 'KEN',
		'code': 'afken',
		'demonym': 'Kenyan',
		'en': 'Kenya',
		'fullname': 'Kenya',
		'continent': 'af',
		'es': 'Kenia',
		'pt': 'Kenia'
	},
	'KI': {
		'alpha3': 'KIR',
		'code': 'ockir',
		'demonym': 'I-Kiribati',
		'en': 'Kiribati',
		'fullname': 'Kiribati',
		'continent': 'oc',
		'es': 'Kiribati',
		'pt': 'Kiribati'
	},
	'KH': {
		'alpha3': 'KHM',
		'code': 'askhm',
		'demonym': 'Cambodian',
		'en': 'Cambodia',
		'fullname': 'Cambodia',
		'continent': 'as',
		'es': 'Camboya',
		'pt': 'Camboya'
	},
	'KN': {
		'alpha3': 'KNA',
		'code': 'nakna',
		'demonym': 'Kittian',
		'en': 'Saint Kitts and Nevis',
		'fullname': 'St. Kitts',
		'continent': 'na',
		'es': 'San Cristóbal y Nieves',
		'pt': 'San Cristóbal y Nieves'
	},
	'KM': {
		'alpha3': 'COM',
		'code': 'afcom',
		'demonym': 'Comoran',
		'en': 'Comoros',
		'fullname': 'Comoros',
		'continent': 'af',
		'es': 'Comoras',
		'pt': 'Comoras'
	},
	'KR': {
		'alpha3': 'KOR',
		'code': 'askor',
		'demonym': 'South Korean',
		'en': 'South Korea',
		'fullname': 'South Korea',
		'continent': 'as',
		'es': 'Corea del Sur',
		'pt': 'Corea del Sur'
	},
	'KP': {
		'alpha3': 'PRK',
		'code': 'asprk',
		'demonym': 'North Korean',
		'en': 'North Korea',
		'fullname': 'North Korea',
		'continent': 'as',
		'es': 'Corea del Norte',
		'pt': 'Corea del Norte'
	},
	'KW': {
		'alpha3': 'KWT',
		'code': 'askwt',
		'demonym': 'Kuwaiti',
		'en': 'Kuwait',
		'fullname': 'Kuwait',
		'continent': 'as',
		'es': 'Kuwait',
		'pt': 'Kuwait'
	},
	'KZ': {
		'alpha3': 'KAZ',
		'code': 'askaz',
		'demonym': 'Kazakhstani',
		'en': 'Kazakhstan',
		'fullname': 'Kazakhstan',
		'continent': 'as',
		'es': 'Kazajistán',
		'pt': 'Kazajistán'
	},
	'KY': {
		'alpha3': 'CYM',
		'code': 'nacym',
		'demonym': 'Caymanian',
		'en': 'Cayman Islands',
		'fullname': 'Cayman Islands',
		'continent': 'na',
		'es': 'Islas Caimán',
		'pt': 'Islas Caimán'
	},
	'DO': {
		'alpha3': 'DOM',
		'code': 'nadom',
		'demonym': 'Dominican',
		'en': 'Dominican Republic',
		'fullname': 'Dominican Republic',
		'continent': 'na',
		'es': 'República Dominicana',
		'pt': 'República Dominicana'
	},
	'DM': {
		'alpha3': 'DMA',
		'code': 'nadma',
		'demonym': 'Dominican',
		'en': 'Dominica',
		'fullname': 'Dominica',
		'continent': 'na',
		'es': 'Dominica',
		'pt': 'Dominica'
	},
	'DJ': {
		'alpha3': 'DJI',
		'code': 'afdji',
		'demonym': 'Djiboutian',
		'en': 'Djibouti',
		'fullname': 'Djibouti',
		'continent': 'af',
		'es': 'Yibuti',
		'pt': 'Yibuti'
	},
	'DK': {
		'alpha3': 'DNK',
		'code': 'eudnk',
		'demonym': 'Danish',
		'en': 'Denmark',
		'fullname': 'Denmark',
		'continent': 'eu',
		'es': 'Dinamarca',
		'pt': 'Dinamarca'
	},
	'DE': {
		'alpha3': 'DEU',
		'code': 'eudeu',
		'demonym': 'German',
		'en': 'Germany',
		'fullname': 'Germany',
		'continent': 'eu',
		'es': 'Alemania',
		'pt': 'Alemania'
	},
	'DZ': {
		'alpha3': 'DZA',
		'code': 'afdza',
		'demonym': 'Algerian',
		'en': 'Algeria',
		'fullname': 'Algeria',
		'continent': 'af',
		'es': 'Algeria',
		'pt': 'Algeria'
	},
	'TZ': {
		'alpha3': 'TZA',
		'code': 'aftza',
		'demonym': 'Tanzanian',
		'en': 'Tanzania',
		'fullname': 'Zanzibar',
		'continent': 'af',
		'es': 'Tanzania',
		'pt': 'Tanzania'
	},
	'TV': {
		'alpha3': 'TUV',
		'code': 'octuv',
		'demonym': 'Tuvaluan',
		'en': 'Tuvalu',
		'fullname': 'Tuvalu',
		'continent': 'oc',
		'es': 'Tuvalu',
		'pt': 'Tuvalu'
	},
	'TW': {
		'alpha3': 'TWN',
		'code': 'astwn',
		'demonym': 'Taiwanese',
		'en': 'Taiwan',
		'fullname': 'Taiwan',
		'continent': 'as',
		'es': 'Taiwán',
		'pt': 'Taiwán'
	},
	'TT': {
		'alpha3': 'TTO',
		'code': 'natto',
		'demonym': 'Trinidadian',
		'en': 'Trinidad and Tobago',
		'fullname': 'Trinidad and Tobago',
		'continent': 'na',
		'es': 'Trinidad y Tobago',
		'pt': 'Trinidad y Tobago'
	},
	'TR': {
		'alpha3': 'TUR',
		'code': 'astur',
		'demonym': 'Turkish',
		'en': 'Turkey',
		'fullname': 'Turkey',
		'continent': 'as',
		'es': 'Turquía',
		'pt': 'Turquía'
	},
	'TN': {
		'alpha3': 'TUN',
		'code': 'aftun',
		'demonym': 'Tunisian',
		'en': 'Tunisia',
		'fullname': 'Tunisia',
		'continent': 'af',
		'es': 'Tunez',
		'pt': 'Tunez'
	},
	'TO': {
		'alpha3': 'TON',
		'code': 'octon',
		'demonym': 'Tongan',
		'en': 'Tonga',
		'fullname': 'Tonga',
		'continent': 'oc',
		'es': 'Tonga',
		'pt': 'Tonga'
	},
	'TL': {
		'alpha3': 'TLS',
		'code': 'octls',
		'demonym': 'Timorese',
		'en': 'East Timor',
		'fullname': 'East Timor',
		'continent': 'oc',
		'es': 'Timor Oriental',
		'pt': 'Timor Oriental'
	},
	'TM': {
		'alpha3': 'TKM',
		'code': 'astkm',
		'demonym': 'Turkmen',
		'en': 'Turkmenistan',
		'fullname': 'Turkmenistan',
		'continent': 'as',
		'es': 'Turkmenistán',
		'pt': 'Turkmenistán'
	},
	'TJ': {
		'alpha3': 'TJK',
		'code': 'astjk',
		'demonym': 'Tajikistani',
		'en': 'Tajikistan',
		'fullname': 'Tajikistan',
		'continent': 'as',
		'es': 'Tadjikistán',
		'pt': 'Tadjikistán'
	},
	'TK': {
		'alpha3': 'TKL',
		'code': 'octkl',
		'demonym': 'Tokelauan',
		'en': 'Tokelau',
		'fullname': 'Tokelau',
		'continent': 'oc',
		'es': 'Tokelau',
		'pt': 'Tokelau'
	},
	'TH': {
		'alpha3': 'THA',
		'code': 'astha',
		'demonym': 'Thai',
		'en': 'Thailand',
		'fullname': 'Thailand',
		'continent': 'as',
		'es': 'Tailandia',
		'pt': 'Tailandia'
	},
	'TG': {
		'alpha3': 'TGO',
		'code': 'aftgo',
		'demonym': 'Togolese',
		'en': 'Togo',
		'fullname': 'Togo',
		'continent': 'af',
		'es': 'Togo',
		'pt': 'Togo'
	},
	'TD': {
		'alpha3': 'TCD',
		'code': 'aftcd',
		'demonym': 'Chadian',
		'en': 'Chad',
		'fullname': 'Chad Republic',
		'continent': 'af',
		'es': 'Chad',
		'pt': 'Chad'
	},
	'TC': {
		'alpha3': 'TCA',
		'code': 'natca',
		'demonym': 'Turks and Caicos Islander',
		'en': 'Turks and Caicos Islands',
		'fullname': 'Turks and Caicos Islands',
		'continent': 'na',
		'es': 'Islas Turcas y Caicos',
		'pt': 'Islas Turcas y Caicos'
	},
	'AE': {
		'alpha3': 'ARE',
		'code': 'asare',
		'demonym': 'Emirian',
		'en': 'United Arab Emirates',
		'fullname': 'United Arab Emirates',
		'continent': 'as',
		'es': 'Emiratos Árabes Unidos',
		'pt': 'Emiratos Árabes Unidos'
	},
	'AD': {
		'alpha3': 'AND',
		'code': 'euand',
		'demonym': 'Andorran',
		'en': 'Andorra',
		'fullname': 'Andorra',
		'continent': 'eu',
		'es': 'Andorra',
		'pt': 'Andorra'
	},
	'AG': {
		'alpha3': 'ATG',
		'code': 'naatg',
		'demonym': 'Antiguan',
		'en': 'Antigua and Barbuda',
		'fullname': 'Antigua and Barbuda',
		'continent': 'na',
		'es': 'Antigua y Barbuda',
		'pt': 'Antigua y Barbuda'
	},
	'AF': {
		'alpha3': 'AFG',
		'code': 'asafg',
		'demonym': 'Afghani',
		'en': 'Afghanistan',
		'fullname': 'Afghanistan',
		'continent': 'as',
		'es': 'Afganistán',
		'pt': 'Afganistán'
	},
	'AI': {
		'alpha3': 'AIA',
		'code': 'naaia',
		'demonym': 'Anguillan',
		'en': 'Anguilla',
		'fullname': 'Anguilla',
		'continent': 'na',
		'es': 'Anguila',
		'pt': 'Anguila'
	},
	'AM': {
		'alpha3': 'ARM',
		'code': 'asarm',
		'demonym': 'Armenian',
		'en': 'Armenia',
		'fullname': 'Armenia',
		'continent': 'as',
		'es': 'Armenia',
		'pt': 'Armenia'
	},
	'AL': {
		'alpha3': 'ALB',
		'code': 'eualb',
		'demonym': 'Albanian',
		'en': 'Albania',
		'fullname': 'Albania',
		'continent': 'eu',
		'es': 'Albania',
		'pt': 'Albania'
	},
	'AO': {
		'alpha3': 'AGO',
		'code': 'afago',
		'demonym': 'Angolan',
		'en': 'Angola',
		'fullname': 'Angola',
		'continent': 'af',
		'es': 'Angola',
		'pt': 'Angola'
	},
	'AQ': {
		'alpha3': 'ATA',
		'code': 'anata',
		'demonym': 'Antarctic',
		'en': 'Antarctica',
		'fullname': 'Antarctica',
		'continent': 'an',
		'es': 'Antártida',
		'pt': 'Antártida'
	},
	'AS': {
		'alpha3': 'ASM',
		'code': 'ocasm',
		'demonym': 'Samoan',
		'en': 'American Samoa',
		'fullname': 'American Samoa',
		'continent': 'oc',
		'es': 'Samoa Americana',
		'pt': 'Samoa Americana'
	},
	'AR': {
		'alpha3': 'ARG',
		'code': 'saarg',
		'demonym': 'Argentine',
		'en': 'Argentina',
		'fullname': 'Argentina',
		'continent': 'sa',
		'es': 'Argentina',
		'pt': 'Argentina'
	},
	'AU': {
		'alpha3': 'AUS',
		'code': 'ocaus',
		'demonym': 'Australian',
		'en': 'Australia',
		'fullname': 'Australia',
		'continent': 'oc',
		'es': 'Australia',
		'pt': 'Australia'
	},
	'AT': {
		'alpha3': 'AUT',
		'code': 'euaut',
		'demonym': 'Austrian',
		'en': 'Austria',
		'fullname': 'Austria',
		'continent': 'eu',
		'es': 'Austria',
		'pt': 'Austria'
	},
	'AW': {
		'alpha3': 'ABW',
		'code': 'naabw',
		'demonym': 'Arubian',
		'en': 'Aruba',
		'fullname': 'Aruba',
		'continent': 'na',
		'es': 'Aruba',
		'pt': 'Aruba'
	},
	'AZ': {
		'alpha3': 'AZE',
		'code': 'asaze',
		'demonym': 'Azerbaijani',
		'en': 'Azerbaijan',
		'fullname': 'Azerbaijan',
		'continent': 'as',
		'es': 'Azerbayán',
		'pt': 'Azerbayán'
	},
	'QA': {
		'alpha3': 'QAT',
		'code': 'asqat',
		'demonym': 'Qatari',
		'en': 'Qatar',
		'fullname': 'Qatar',
		'continent': 'as',
		'es': 'Qatar',
		'pt': 'Qatar'
	}
}