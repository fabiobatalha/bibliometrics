#!/usr/bin/env python
# -*- coding: utf-8 -*-
WOS_THEMATIC_AREA = {
    "Health policy & services": {
      "en": "Health policy & services",
      "es": "Servicios y Política Sanitaria",
      "pt": "Políticas e serviços de saúde",
    },
    "Medicine, general & internal": {
      "en": "Medicine, general & internal",
      "es": "Medicina, General e Interna",
      "pt": "Medicina, geral e interna",
    },
    "Public, environmental & occupational health": {
      "en": "Public, environmental & occupational health",
      "es": "Salud Pública, Ambiental y Ocupacional",
      "pt": "Saúde pública, ambiental e ocupacional",
    },
    "Education & educational research": {
      "en": "Education & educational research",
      "es": "Educación e Investigación Educativa",
      "pt": "Educação e pesquisa educacional",
    },
    "Agriculture, multidisciplinary": {
      "en": "Agriculture, multidisciplinary",
      "es": "Agricultura, Multidisciplinar",
      "pt": "Agricultura, multidisciplinar",
    },
    "Biology": {
      "en": "Biology",
      "es": "Biología",
      "pt": "Biologia",
    },
    "Health care sciences & services": {
      "en": "Health care sciences & services",
      "es": "Servicios y Ciencias de la Salud",
      "pt": "Ciências e serviços da saúde",
    },
    "Sociology": {
      "en": "Sociology",
      "es": "Sociología",
      "pt": "Sociologia",
    },
    "Nursing": {
      "en": "Nursing",
      "es": "Enfermería",
      "pt": "Enfermagem",
    },
    "Tropical medicine": {
      "en": "Tropical medicine",
      "es": "Medicina Tropical",
      "pt": "Medicina tropical",
    },
    "Medicine, research & experimental": {
      "en": "Medicine, research & experimental",
      "es": "Medicina, Experimental y de Investigación",
      "pt": "Medicina, de pesquisa e experimental",
    },
    "Agronomy": {
      "en": "Agronomy",
      "es": "Agronomía",
      "pt": "Agronomia",
    },
    "Veterinary sciences": {
      "en": "Veterinary sciences",
      "es": "Ciencias Veterinarias",
      "pt": "Ciências veterinárias",
    },
    "Humanities, multidisciplinary": {
      "en": "Humanities, multidisciplinary",
      "es": "Humanidades, Multidisciplinar",
      "pt": "Humanidades, multidisciplinar",
    },
    "Agriculture, dairy & animal science": {
      "en": "Agriculture, dairy & animal science",
      "es": "Ciencia de los Animales, Productos Lácteos y Agricultura",
      "pt": "Agricultura, laticínios e ciências de animais",
    },
    "Psychiatry": {
      "en": "Psychiatry",
      "es": "Psiquiatría",
      "pt": "Psiquiatria",
    },
    "Surgery": {
      "en": "Surgery",
      "es": "Cirugía",
      "pt": "Cirurgia",
    },
    "Pediatrics": {
      "en": "Pediatrics",
      "es": "Pediatría",
      "pt": "Pediatria",
    },
    "Psychology, multidisciplinary": {
      "en": "Psychology, multidisciplinary",
      "es": "Psicología, Multidisciplinar",
      "pt": "Psicologia, multidisciplinar",
    },
    "Social sciences, interdisciplinary": {
      "en": "Social sciences, interdisciplinary",
      "es": "Ciencias Sociales, Interdisciplinar",
      "pt": "Ciências sociais, interdisciplinar",
    },
    "Plant sciences": {
      "en": "Plant sciences",
      "es": "Botánica",
      "pt": "Ciências das plantas",
    },
    "Chemistry, multidisciplinary": {
      "en": "Chemistry, multidisciplinary",
      "es": "Química, Multidisciplinar",
      "pt": "Química, multidisciplinar",
    },
    "Engineering, multidisciplinary": {
      "en": "Engineering, multidisciplinary",
      "es": "Ingeniería, Multidisciplinar",
      "pt": "Engenharia, multidisciplinar",
    },
    "Zoology": {
      "en": "Zoology",
      "es": "Zoología",
      "pt": "Zoologia",
    },
    "Cardiac & cardiovascular systems": {
      "en": "Cardiac & cardiovascular systems",
      "es": "Sistemas Cardiaco y Cardiovascular",
      "pt": "Sistema cardíaco e cardiovascular",
    },
    "Medicine, legal": {
      "en": "Medicine, legal",
      "es": "Medicina, Legal",
      "pt": "Medicina, legal",
    },
    "Parasitology": {
      "en": "Parasitology",
      "es": "Parasitología",
      "pt": "Parasitologia",
    },
    "Medical ethics": {
      "en": "Medical ethics",
      "es": "Ética Médica",
      "pt": "Ética médica",
    },
    "Dentistry, oral surgery & medicine": {
      "en": "Dentistry, oral surgery & medicine",
      "es": "Odontología, Medicina y Cirugía Oral",
      "pt": "Odontologia, cirurgia oral e medicina",
    },
    "Management": {
      "en": "Management",
      "es": "Gestión",
      "pt": "Gerenciamento",
    },
    "Neurosciences": {
      "en": "Neurosciences",
      "es": "Neurociencias",
      "pt": "Neurociências",
    },
    "History": {
      "en": "History",
      "es": "Historia",
      "pt": "História",
    },
    "Economics": {
      "en": "Economics",
      "es": "Economía",
      "pt": "Economia",
    },
    "Anthropology": {
      "en": "Anthropology",
      "es": "Antropología",
      "pt": "Antropologia",
    },
    "Law": {
      "en": "Law",
      "es": "Derecho",
      "pt": "Lei",
    },
    "Biodiversity conservation": {
      "en": "Biodiversity conservation",
      "es": "Conservación de la Biodiversidad",
      "pt": "Conservação da biodiversidade",
    },
    "Physics, multidisciplinary": {
      "en": "Physics, multidisciplinary",
      "es": "Física, Multidisciplinar",
      "pt": "Física, multidisciplinar",
    },
    "Obstetrics & gynecology": {
      "en": "Obstetrics & gynecology",
      "es": "Obstetricia y Ginecología",
      "pt": "Obstetrícia e ginecologia",
    },
    "Orthopedics": {
      "en": "Orthopedics",
      "es": "Ortopedia",
      "pt": "Ortopedia",
    },
    "Philosophy": {
      "en": "Philosophy",
      "es": "Filosofía",
      "pt": "Filosofia",
    },
    "Political science": {
      "en": "Political science",
      "es": "Ciencias Políticas",
      "pt": "Ciências políticas",
    },
    "Pharmacology & pharmacy": {
      "en": "Pharmacology & pharmacy",
      "es": "Farmacología y Farmacia",
      "pt": "Farmacologia e farmácia",
    },
    "Medical laboratory technology": {
      "en": "Medical laboratory technology",
      "es": "Tecnología Médica de Laboratorio",
      "pt": "Tecnologia laboratorial médica",
    },
    "Infectious diseases": {
      "en": "Infectious diseases",
      "es": "Enfermedades Infecciosas",
      "pt": "Doenças contagiosas",
    },
    "Rehabilitation": {
      "en": "Rehabilitation",
      "es": "Rehabilitación",
      "pt": "Reabilitação",
    },
    "Religion": {
      "en": "Religion",
      "es": "Religión",
      "pt": "Religião",
    },
    "Gastroenterology & hepatology": {
      "en": "Gastroenterology & hepatology",
      "es": "Gastroenterología y Hepatología",
      "pt": "Gastroenterologia e hepatologia",
    },
    "Cultural studies": {
      "en": "Cultural studies",
      "es": "Estudios Culturales",
      "pt": "Estudos culturais",
    },
    "Entomology": {
      "en": "Entomology",
      "es": "Entomología",
      "pt": "Entomologia",
    },
    "Nutrition & dietetics": {
      "en": "Nutrition & dietetics",
      "es": "Nutrición y Dietética",
      "pt": "Nutrição e dietética",
    },
    "Ophthalmology": {
      "en": "Ophthalmology",
      "es": "Oftalmología",
      "pt": "Oftalmologia",
    },
    "Language & linguistics": {
      "en": "Language & linguistics",
      "es": "Lengua y Lingüística",
      "pt": "Idiomas e linguística",
    },
    "Geosciences, multidisciplinary": {
      "en": "Geosciences, multidisciplinary",
      "es": "Ciencias de la Tierra, Multidisciplinar",
      "pt": "Geociências, multidisciplinar",
    },
    "Horticulture": {
      "en": "Horticulture",
      "es": "Horticultura",
      "pt": "Horticultura",
    },
    "Environmental sciences": {
      "en": "Environmental sciences",
      "es": "Ciencias Ambientales",
      "pt": "Ciências ambientais",
    },
    "Metallurgy & metallurgical engineering": {
      "en": "Metallurgy & metallurgical engineering",
      "es": "Metalurgia e Ingeniería Metalúrgica",
      "pt": "Metalurgia e engenharia metalúrgica",
    },
    "Linguistics": {
      "en": "Linguistics",
      "es": "Lingüística",
      "pt": "Linguística",
    },
    "Engineering, chemical": {
      "en": "Engineering, chemical",
      "es": "Ingeniería, Química",
      "pt": "Engenharia, química",
    },
    "Forestry": {
      "en": "Forestry",
      "es": "Ingeniería de Montes",
      "pt": "Silvicultura",
    },
    "Otorhinolaryngology": {
      "en": "Otorhinolaryngology",
      "es": "Otorrinolaringología",
      "pt": "Otorrinolaringologia",
    },
    "Agricultural engineering": {
      "en": "Agricultural engineering",
      "es": "Ingeniería Agrícola",
      "pt": "Engenharia agrícola",
    },
    "Genetics & heredity": {
      "en": "Genetics & heredity",
      "es": "Genética y Herencia Genética",
      "pt": "Genética e hereditariedade",
    },
    "Ecology": {
      "en": "Ecology",
      "es": "Ecology",
      "pt": "Ecologia",
    },
    "Microbiology": {
      "en": "Microbiology",
      "es": "Microbiología",
      "pt": "Microbiologia",
    },
    "Education, scientific disciplines": {
      "en": "Education, scientific disciplines",
      "es": "Education, scientific disciplines",
      "pt": "Educação, disciplinas científicas",
    },
    "Sport sciences": {
      "en": "Sport sciences",
      "es": "Ciencias de la Actividad Física",
      "pt": "Ciências do esporte",
    },
    "Medical informatics": {
      "en": "Medical informatics",
      "es": "Medical informatics",
      "pt": "Medical informatics",
    },
    "Anatomy & morphology": {
      "en": "Anatomy & morphology",
      "es": "Anatomía y Morfología",
      "pt": "Anatomia e morfologia",
    },
    "Endocrinology & metabolism": {
      "en": "Endocrinology & metabolism",
      "es": "Endocrinología y Metabolismo",
      "pt": "Endocrinologia e metabolismo",
    },
    "Urology & nephrology": {
      "en": "Urology & nephrology",
      "es": "Urología y Nefrología",
      "pt": "Urologia e nefrologia",
    },
    "Psychology": {
      "en": "Psychology",
      "es": "Psychology",
      "pt": "Psicologia",
    },
    "Soil science": {
      "en": "Soil science",
      "es": "Ciencia del Suelo",
      "pt": "Ciência do solo",
    },
    "Anesthesiology": {
      "en": "Anesthesiology",
      "es": "Anestesiología",
      "pt": "Anestesiologia",
    },
    "Geology": {
      "en": "Geology",
      "es": "Geología",
      "pt": "Geologia",
    },
    "Materials science, multidisciplinary": {
      "en": "Materials science, multidisciplinary",
      "es": "Ciencia de los Materiales, Multidisciplinar",
      "pt": "Ciências dos materiais, multidisciplinar",
    },
    "Environmental studies": {
      "en": "Environmental studies",
      "es": "Estudios Ambientales",
      "pt": "Estudos ambientais",
    },
    "Engineering, civil": {
      "en": "Engineering, civil",
      "es": "Ingeniería, Civil",
      "pt": "Engenharia, civil",
    },
    "Marine & freshwater biology": {
      "en": "Marine & freshwater biology",
      "es": "Biología Marina y de Agua Dulce",
      "pt": "Biologia marinha e de água doce",
    },
    "Communication": {
      "en": "Communication",
      "es": "Comunicación",
      "pt": "Comunicação",
    },
    "Physiology": {
      "en": "Physiology",
      "es": "Fisiología",
      "pt": "Fisiologia",
    },
    "Biochemistry & molecular biology": {
      "en": "Biochemistry & molecular biology",
      "es": "Bioquímica y Biología Molecular",
      "pt": "Bioquímica e biologia molecular",
    },
    "Dermatology": {
      "en": "Dermatology",
      "es": "Dermatología",
      "pt": "Dermatologia",
    },
    "Respiratory system": {
      "en": "Respiratory system",
      "es": "Sistema Respiratorio",
      "pt": "Sistema respiratório",
    },
    "Food science & technology": {
      "en": "Food science & technology",
      "es": "Ciencia y Tecnología de los Alimentos",
      "pt": "Ciência e tecnologia dos alimentos",
    },
    "Oceanography": {
      "en": "Oceanography",
      "es": "Oceanografía",
      "pt": "Oceanografia",
    },
    "Multidisciplinary sciences": {
      "en": "Multidisciplinary sciences",
      "es": "Ciencias Multidisciplinarias",
      "pt": "Ciências multidisciplinares",
    },
    "Geography": {
      "en": "Geography",
      "es": "Geografía",
      "pt": "Geografia",
    },
    "Hematology": {
      "en": "Hematology",
      "es": "Hematología",
      "pt": "Hematologia",
    },
    "Ornithology": {
      "en": "Ornithology",
      "es": "Ornithology",
      "pt": "Ornitologia",
    },
    "Literature": {
      "en": "Literature",
      "es": "Literatura",
      "pt": "Literatura",
    },
    "Radiology, nuclear medicine & medical imaging": {
      "en": "Radiology, nuclear medicine & medical imaging",
      "es": "Radiología, Imagen Médica y Medicina Nuclear",
      "pt": "Radiologia, medicina nuclear e imagiologia médica",
    },
    "Psychology, clinical": {
      "en": "Psychology, clinical",
      "es": "Psicología, Clínica",
      "pt": "Psicologia, clínica",
    },
    "Geriatrics & gerontology": {
      "en": "Geriatrics & gerontology",
      "es": "Geriatría y Gerontología",
      "pt": "Geriatria e gerontologia",
    },
    "Psychology, educational": {
      "en": "Psychology, educational",
      "es": "Psicología, Educativa",
      "pt": "Psicologia, educacional",
    },
    "Biotechnology & applied microbiology": {
      "en": "Biotechnology & applied microbiology",
      "es": "Biotecnología y Microbiología Aplicada",
      "pt": "Biotecnologia e microbiologia aplicada",
    },
    "Information science & library science": {
      "en": "Information science & library science",
      "es": "Ciencias de la Información y Biblioteconomía",
      "pt": "Ciência da informação e biblioteconomia",
    },
    "Oncology": {
      "en": "Oncology",
      "es": "Oncology",
      "pt": "Oncologia",
    },
    "Art": {
      "en": "Art",
      "es": "Arte",
      "pt": "Arte",
    },
    "Integrative & complementary medicine": {
      "en": "Integrative & complementary medicine",
      "es": "Medicina Complementaria e Integral",
      "pt": "Medicina integrativa e complementar",
    },
    "History & philosophy of science": {
      "en": "History & philosophy of science",
      "es": "Historia y Filosofía de la Ciencia",
      "pt": "História e filosofia da ciência",
    },
    "Psychology, applied": {
      "en": "Psychology, applied",
      "es": "Psicología, Aplicada",
      "pt": "Psicologia, aplicada",
    },
    "Primary health care": {
      "en": "Primary health care",
      "es": "Atención Primaria de Salud",
      "pt": "Atenção primária à saúde",
    },
    "Psychology, social": {
      "en": "Psychology, social",
      "es": "Psicología, Social",
      "pt": "Psicologia, social",
    },
    "Rheumatology": {
      "en": "Rheumatology",
      "es": "Reumatología",
      "pt": "Reumatologia",
    },
    "Audiology & speech-language pathology": {
      "en": "Audiology & speech-language pathology",
      "es": "Audiología y Patología del Habla y del Lenguaje",
      "pt": "Audiologia e Fonoaudiologia",
    },
    "Literature, romance": {
      "en": "Literature, romance",
      "es": "Literatura, Romance",
      "pt": "Literatura, romance",
    },
    "Women's studies": {
      "en": "Women's studies",
      "es": "Estudios de la Mujer",
      "pt": "Estudos sobre a mulher",
    },
    "International relations": {
      "en": "International relations",
      "es": "Relaciones Internacionales",
      "pt": "Relações internacionais",
    },
    "Clinical neurology": {
      "en": "Clinical neurology",
      "es": "Neurología Clínica",
      "pt": "Neurologia clínica",
    },
    "Critical care medicine": {
      "en": "Critical care medicine",
      "es": "Medicina Intensiva",
      "pt": "Medicina intensiva",
    },
    "Water resources": {
      "en": "Water resources",
      "es": "Recursos Hídricos",
      "pt": "Recursos hídricos",
    },
    "Pathology": {
      "en": "Pathology",
      "es": "Patología",
      "pt": "Patologia",
    },
    "Engineering, manufacturing": {
      "en": "Engineering, manufacturing",
      "es": "Ingeniería, Fabricación",
      "pt": "Engenharia, de produção",
    },
    "Architecture": {
      "en": "Architecture",
      "es": "Arquitectura",
      "pt": "Arquitetura",
    },
    "Immunology": {
      "en": "Immunology",
      "es": "Inmunología",
      "pt": "Imunologia",
    },
    "History of social sciences": {
      "en": "History of social sciences",
      "es": "Historia de las Ciencias Sociales",
      "pt": "História das ciências sociais",
    },
    "Engineering, mechanical": {
      "en": "Engineering, mechanical",
      "es": "Ingeniería, Mecánica",
      "pt": "Engenharia, mecânica",
    },
    "Ethics": {
      "en": "Ethics",
      "es": "Ética",
      "pt": "Ética",
    },
    "Chemistry, organic": {
      "en": "Chemistry, organic",
      "es": "Química, Orgánica",
      "pt": "Química orgânica",
    },
    "Computer science, information systems": {
      "en": "Computer science, information systems",
      "es": "Informática, Sistemas de Información",
      "pt": "Ciência da computação, sistemas da informação",
    },
    "Demography": {
      "en": "Demography",
      "es": "Demografía",
      "pt": "Demografia",
    },
    "Social issues": {
      "en": "Social issues",
      "es": "Asuntos Sociales",
      "pt": "Questões sociais",
    },
    "Social sciences, biomedical": {
      "en": "Social sciences, biomedical",
      "es": "Ciencias Sociales, Biomédica",
      "pt": "Ciências sociais, biomédicas",
    },
    "Archaeology": {
      "en": "Archaeology",
      "es": "Arqueología",
      "pt": "Arqueologia",
    },
    "Gerontology": {
      "en": "Gerontology",
      "es": "Gerontología",
      "pt": "Gerontologia",
    },
    "Chemistry, medicinal": {
      "en": "Chemistry, medicinal",
      "es": "Química, Medicinal",
      "pt": "Química, medicinal",
    },
    "Agricultural economics & policy": {
      "en": "Agricultural economics & policy",
      "es": "Política y Economía Agrícola",
      "pt": "Economia e política agrícola",
    },
    "Construction & building technology": {
      "en": "Construction & building technology",
      "es": "Construcción y Tecnología de la Edificación",
      "pt": "Tecnologia de construção e edificações",
    },
    "Education, special": {
      "en": "Education, special",
      "es": "Educación, Especial",
      "pt": "Educação, especial",
    },
    "Business, finance": {
      "en": "Business, finance",
      "es": "Negocios, Finanzas",
      "pt": "Negócios, finanças",
    },
    "Family studies": {
      "en": "Family studies",
      "es": "Estudios de Familia",
      "pt": "Estudos da família",
    },
    "Polymer science": {
      "en": "Polymer science",
      "es": "Ciencia de Polímeros",
      "pt": "Ciência dos polímeros",
    },
    "Hospitality, leisure, sport & tourism": {
      "en": "Hospitality, leisure, sport & tourism",
      "es": "Hospitalidad, Ocio, Deporte y Turismo",
      "pt": "Hospitalidade, lazer, esporte e turismo",
    },
    "Engineering, geological": {
      "en": "Engineering, geological",
      "es": "Ingeniería, Geológica",
      "pt": "Engineering, geological",
    },
    "Public administration": {
      "en": "Public administration",
      "es": "Administración Pública",
      "pt": "Administração pública",
    },
    "Geochemistry & geophysics": {
      "en": "Geochemistry & geophysics",
      "es": "Geoquímica y Geofísica",
      "pt": "Geoquímica e geofísica",
    },
    "Psychology, developmental": {
      "en": "Psychology, developmental",
      "es": "Psicología, del Desarrollo",
      "pt": "Psicologia, desenvolvimento",
    },
    "Chemistry, applied": {
      "en": "Chemistry, applied",
      "es": "Química, Aplicada",
      "pt": "Química aplicada",
    },
    "Materials science, ceramics": {
      "en": "Materials science, ceramics",
      "es": "Ciencia de los Materiales, Cerámica",
      "pt": "Ciências dos materiais, cerâmica",
    },
    "Literary theory & criticism": {
      "en": "Literary theory & criticism",
      "es": "Crítica y Teoría de la Literatura",
      "pt": "Teoria literária e crítica",
    },
    "Mining & mineral processing": {
      "en": "Mining & mineral processing",
      "es": "Minería y Tratamiento de Minerales",
      "pt": "Processamento mineral e mineração",
    },
    "Reproductive biology": {
      "en": "Reproductive biology",
      "es": "Biología Reproductora",
      "pt": "Biologia reprodutiva",
    },
    "Business": {
      "en": "Business",
      "es": "Negocios",
      "pt": "Negócios",
    },
    "Engineering, electrical & electronic": {
      "en": "Engineering, electrical & electronic",
      "es": "Ingeniería, Eléctrica y Electrónica",
      "pt": "Engenharia, eléctrico e electrónico",
    },
    "Criminology & penology": {
      "en": "Criminology & penology",
      "es": "Criminología y Penología",
      "pt": "Criminologia e Penologia",
    },
    "Urban studies": {
      "en": "Urban studies",
      "es": "Estudios Urbanos",
      "pt": "Estudos urbanos",
    },
    "Toxicology": {
      "en": "Toxicology",
      "es": "Toxicología",
      "pt": "Toxicologia",
    },
    "Mycology": {
      "en": "Mycology",
      "es": "Micología",
      "pt": "Micologia",
    },
    "Music": {
      "en": "Music",
      "es": "Música",
      "pt": "Música",
    },
    "Area studies": {
      "en": "Area studies",
      "es": "Estudios de Área",
      "pt": "Estudos de áreas",
    },
    "Acoustics": {
      "en": "Acoustics",
      "es": "Acústica",
      "pt": "Acústica",
    },
    "Chemistry, physical": {
      "en": "Chemistry, physical",
      "es": "Química, Física",
      "pt": "Química, física",
    },
    "Psychology, biological": {
      "en": "Psychology, biological",
      "es": "Psicología, Biológica",
      "pt": "Psicologia, biológica",
    },
    "Mathematics": {
      "en": "Mathematics",
      "es": "Matemáticas",
      "pt": "Matemática",
    },
    "Peripheral vascular disease": {
      "en": "Peripheral vascular disease",
      "es": "Enfermedad Vascular Periférica",
      "pt": "Doença vascular periférica",
    },
    "Social work": {
      "en": "Social work",
      "es": "Trabajo Social",
      "pt": "Serviço social",
    },
    "Allergy": {
      "en": "Allergy",
      "es": "Alergia",
      "pt": "Alergia",
    },
    "Psychology, psychoanalysis": {
      "en": "Psychology, psychoanalysis",
      "es": "Psicología, Psicoanálisis",
      "pt": "Psicologia, psicanálise",
    },
    "Engineering, environmental": {
      "en": "Engineering, environmental",
      "es": "Ingeniería, Ambiental",
      "pt": "Engenharia, ambiental",
    },
    "Mathematics, applied": {
      "en": "Mathematics, applied",
      "es": "Matemáticas, Aplicadas",
      "pt": "Matemática, aplicada",
    },
    "Meteorology & atmospheric sciences": {
      "en": "Meteorology & atmospheric sciences",
      "es": "Meteorología y Ciencias de la Atmósfera",
      "pt": "Meteorologia e ciências atmosféricas",
    },
    "Fisheries": {
      "en": "Fisheries",
      "es": "Pesquería",
      "pt": "Pesca",
    },
    "Biochemical research methods": {
      "en": "Biochemical research methods",
      "es": "Métodos de Investigación Bioquímica",
      "pt": "Métodos de pesquisa bioquímicos",
    },
    "Electrochemistry": {
      "en": "Electrochemistry",
      "es": "Electroquímica",
      "pt": "Eletroquímica",
    },
    "Chemistry, inorganic & nuclear": {
      "en": "Chemistry, inorganic & nuclear",
      "es": "Química, Inorgánica y Nuclear",
      "pt": "Química inorgânica e nuclear",
    },
    "Literature, african, australian, canadian": {
      "en": "Literature, african, australian, canadian",
      "es": "Literatura, Africana, Australiana, Canadiense",
      "pt": "Literatura, africano, australiano, canadense",
    },
    "Behavioral sciences": {
      "en": "Behavioral sciences",
      "es": "Ciencias del Comportamiento",
      "pt": "Ciência comportamental",
    },
    "Mechanics": {
      "en": "Mechanics",
      "es": "Mecánica",
      "pt": "Mecânica",
    },
    "Chemistry, analytical": {
      "en": "Chemistry, analytical",
      "es": "Química, Analítica",
      "pt": "Química analítica",
    },
    "Developmental biology": {
      "en": "Developmental biology",
      "es": "Biología del Desarrollo",
      "pt": "Biologia do desenvolvimento",
    },
    "Evolutionary biology": {
      "en": "Evolutionary biology",
      "es": "Biología Evolutiva",
      "pt": "Biologia evolutiva",
    },
    "Geography, physical": {
      "en": "Geography, physical",
      "es": "Geografía, Física",
      "pt": "Geografia, física",
    },
    "Automation & control systems": {
      "en": "Automation & control systems",
      "es": "Sistemas de Control y Automatización",
      "pt": "Sistemas de automação e controle",
    },
    "Literary reviews": {
      "en": "Literary reviews",
      "es": "Críticas Literarias",
      "pt": "Crítica literária",
    },
    "Nuclear science & technology": {
      "en": "Nuclear science & technology",
      "es": "Ciencia y Tecnología Nuclear",
      "pt": "Ciência e tecnologia nuclear",
    },
    "Andrology": {
      "en": "Andrology",
      "es": "Andrología",
      "pt": "Andrologia",
    },
    "Cell biology": {
      "en": "Cell biology",
      "es": "Biología Celular",
      "pt": "Biologia celular",
    },
    "Emergency medicine": {
      "en": "Emergency medicine",
      "es": "Medicina de Emergencia",
      "pt": "Medicamento de emergência",
    },
    "Computer science, software engineering": {
      "en": "Computer science, software engineering",
      "es": "Informática, Ingeniería de Software",
      "pt": "Ciência da computação, engenharia de software",
    },
    "Engineering, industrial": {
      "en": "Engineering, industrial",
      "es": "Ingeniería, Industrial",
      "pt": "Engenharia, industrial",
    },
    "Medieval & renaissance studies": {
      "en": "Medieval & renaissance studies",
      "es": "Estudios Renacentistas y Medievales",
      "pt": "Estudos medievais e renascentistas",
    },
    "Materials science, paper & wood": {
      "en": "Materials science, paper & wood",
      "es": "Ciencia de los Materiales, Papel y Madera",
      "pt": "Ciências dos materiais, papel e madeira",
    },
    "Psychology, experimental": {
      "en": "Psychology, experimental",
      "es": "Psicología, Experimental",
      "pt": "Psicologia, experimental",
    },
    "Engineering, biomedical": {
      "en": "Engineering, biomedical",
      "es": "Ingeniería, Biomédica",
      "pt": "Engenharia, biomédica",
    },
    "Literature, german, dutch, scandinavian": {
      "en": "Literature, german, dutch, scandinavian",
      "es": "Literatura, Alemana, Holandesa, Escandinava",
      "pt": "Literatura, alemã, holandesa, escandinava",
    },
    "Classics": {
      "en": "Classics",
      "es": "Clásicos",
      "pt": "Clássicos",
    },
    "Operations research & management science": {
      "en": "Operations research & management science",
      "es": "Ciencia de la Gestión e Investigación de Operaciones",
      "pt": "Pesquisa de operações e ciência do gerenciamento",
    },
    "Computer science, artificial intelligence": {
      "en": "Computer science, artificial intelligence",
      "es": "Informática, Inteligencia Artificial",
      "pt": "Ciência da computação, inteligência artificial",
    },
    "Computer science, theory & methods": {
      "en": "Computer science, theory & methods",
      "es": "Informática, Teoría y Métodos",
      "pt": "Ciência da computação, teoria e métodos",
    },
    "Energy & fuels": {
      "en": "Energy & fuels",
      "es": "Energía y Combustibles",
      "pt": "Energia e combustíveis",
    },
    "Transportation science & technology": {
      "en": "Transportation science & technology",
      "es": "Ciencia y Tecnología del Transporte",
      "pt": "Ciência e tecnologia de transporte",
    },
    "Ethnic studies": {
      "en": "Ethnic studies",
      "es": "Estudios Étnicos",
      "pt": "Estudos éticos",
    },
    "Remote sensing": {
      "en": "Remote sensing",
      "es": "Teledetección",
      "pt": "Sensoriamento remoto",
    },
    "Astronomy & astrophysics": {
      "en": "Astronomy & astrophysics",
      "es": "Astronomía y Astrofísica",
      "pt": "Astronomia e astrofísica",
    },
    "Poetry": {
      "en": "Poetry",
      "es": "Poesía",
      "pt": "Poesia",
    },
    "Psychology, mathematical": {
      "en": "Psychology, mathematical",
      "es": "Psicología, Matemática",
      "pt": "Psicologia, matemática",
    },
    "Computer science, interdisciplinary applications": {
      "en": "Computer science, interdisciplinary applications",
      "es": "Informática, Aplicaciones Interdisciplinares",
      "pt": "Ciência da computação, aplicações interdisciplinares",
    },
    "Materials science, biomaterials": {
      "en": "Materials science, biomaterials",
      "es": "Ciencia de los Materiales, Biomateriales",
      "pt": "Ciências dos materiais, biomateriais",
    },
    "Planning & development": {
      "en": "Planning & development",
      "es": "Planificación y Desarrollo",
      "pt": "Planejamento e desenvolvimento",
    },
    "Limnology": {
      "en": "Limnology",
      "es": "Limnología",
      "pt": "Limnologia",
    },
    "Industrial relations & labor": {
      "en": "Industrial relations & labor",
      "es": "Trabajo y Relaciones Laborales",
      "pt": "Relações industriais e trabalho",
    },
    "Engineering, petroleum": {
      "en": "Engineering, petroleum",
      "es": "Ingeniería, Petróleo",
      "pt": "Engenharia, petróleo",
    },
    "Statistics & probability": {
      "en": "Statistics & probability",
      "es": "Estadística y Probabilidad",
      "pt": "Estatística e probabilidade",
    },
    "Physics, applied": {
      "en": "Physics, applied",
      "es": "Física, Aplicada",
      "pt": "Física, aplicada",
    },
    "Nanoscience & nanotechnology": {
      "en": "Nanoscience & nanotechnology",
      "es": "Nanociencia y Nanotecnología",
      "pt": "Nanociência e nanotecnologia",
    },
    "Transportation": {
      "en": "Transportation",
      "es": "Transporte",
      "pt": "Transporte",
    },
    "Computer science, hardware & architecture": {
      "en": "Computer science, hardware & architecture",
      "es": "Informática, Hardware y Arquitectura",
      "pt": "Ciência da computação, hardware e arquitetura",
    },
    "Folklore": {
      "en": "Folklore",
      "es": "Folclore",
      "pt": "Folclore",
    },
    "Optics": {
      "en": "Optics",
      "es": "Óptica",
      "pt": "Óptica",
    },
    "Computer science, cybernetics": {
      "en": "Computer science, cybernetics",
      "es": "Informática, Cibernética",
      "pt": "Ciência da computação, cibernética",
    },
    "Paleontology": {
      "en": "Paleontology",
      "es": "Paleontología",
      "pt": "Paleontologia",
    },
    "Literature, american": {
      "en": "Literature, american",
      "es": "Literatura, Americana",
      "pt": "Literatura, americana",
    },
    "Literature, british isles": {
      "en": "Literature, british isles",
      "es": "Literature, british isles",
      "pt": "Literatura, Ilhas Britânicas",
    },
    "Physics, particles & fields": {
      "en": "Physics, particles & fields",
      "es": "Física, Partículas y Campos",
      "pt": "Física, partículas e campos",
    },
    "Materials science, characterization & testing": {
      "en": "Materials science, characterization & testing",
      "es": "Ciencia de los Materiales, Caracterización y Testeo",
      "pt": "Ciências dos materiais, caracterização e teste",
    },
    "Materials science, coatings & films": {
      "en": "Materials science, coatings & films",
      "es": "Ciencia de los Materiales, Revestimientos y Película",
      "pt": "Ciências dos materiais, revestimentos e películas",
    },
    "Materials science, composites": {
      "en": "Materials science, composites",
      "es": "Ciencia de los Materiales, Compuestos",
      "pt": "Ciências dos materiais, compostos",
    },
    "Materials science, textiles": {
      "en": "Materials science, textiles",
      "es": "Ciencia de los Materiales, Textil",
      "pt": "Ciência dos materiais, têxteis",
    },
    "Mathematics, interdisciplinary applications": {
      "en": "Mathematics, interdisciplinary applications",
      "es": "Matemáticas, Aplicaciones Interdisciplinares",
      "pt": "Matemática, aplicações interdisciplinares",
    },
    "Social sciences, mathematical methods": {
      "en": "Social sciences, mathematical methods",
      "es": "Ciencias Sociales, Métodos Matemáticos",
      "pt": "Ciências sociais, métodos matemáticos",
    },
    "Asian studies": {
      "en": "Asian studies",
      "es": "Estudios Asiáticos",
      "pt": "Estudos asiáticos",
    },
    "Robotics": {
      "en": "Robotics",
      "es": "Robótica",
      "pt": "Robótica",
    },
    "Telecommunications": {
      "en": "Telecommunications",
      "es": "Telecomunicaciones",
      "pt": "Telecomunicações",
    },
    "Engineering, marine": {
      "en": "Engineering, marine",
      "es": "Engineering, marine",
      "pt": "Engineering, marine",
    },
    "Engineering, ocean": {
      "en": "Engineering, ocean",
      "es": "Engineering, ocean",
      "pt": "Engineering, ocean",
    },
    "Dance": {
      "en": "Dance",
      "es": "Baile",
      "pt": "Dança",
    },
    "Engineering, aerospace": {
      "en": "Engineering, aerospace",
      "es": "Engineering, aerospace",
      "pt": "Engineering, aerospace",
    },
    "Crystallography": {
      "en": "Crystallography",
      "es": "Cristalografía",
      "pt": "Cristalografia",
    },
    "Physics, atomic, molecular & chemical": {
      "en": "Physics, atomic, molecular & chemical",
      "es": "Física, Atómica, Molecular y Química",
      "pt": "Física, atômica, molecular e química",
    },
    "Spectroscopy": {
      "en": "Spectroscopy",
      "es": "Espectroscopia",
      "pt": "Espectroscopia",
    },
    "Physics, mathematical": {
      "en": "Physics, mathematical",
      "es": "Physics, mathematical",
      "pt": "Physics, mathematical",
    },
    "Substance abuse": {
      "en": "Substance abuse",
      "es": "Abuso de Sustancias",
      "pt": "Abuso de substâncias químicas",
    }
}