#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app import app
if __name__ == '__main__':
        import logging, sys
        logging.basicConfig(stream=sys.stderr)
        app.run(host='0.0.0.0',port=80,threaded=True,debug=False)
