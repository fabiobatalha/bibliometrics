## Descripción
Repositorio público del proyecto Bibliometrics [VisualSciELO](https://bitbucket.org/net-works/bibliometrics/overview).

### Sobre la imagen Docker
Este proyecto fue desarrollado en Python 3.5 y utiliza gunicorn como servidor HTTP WSGI.

#### Cómo iniciar el contenedor

docker run \
--name visualscielo -p 8000:8000 \

-d net-works/bibliometrics

Variables de configuración del fichero BibliometricsApp/config.py
- HOSTNAME=<dirección web del servidor iniciado>

- SERVER=<dirección al servidor articlemeta api. Dirección por defecto: fractalnet-works.ddns.net> 

- ACCESSSTATS=<puerto articlemeta. Puerto por defecto: 11660> 

- PUBLICATIONSTATS=<puerto publicationstats. Puerto por defecto: 11620> 

- ARTICLEMETA=<puerto articlemeta rpc. Puerto por defecto: 11720> 

- CITEDBY=<puerto citedby. Puerto por defecto: 11>


#### Cómo acceder al sitio:

http://<dirección ip/dns>:<puerto>/v1

docker exec -ti <ID docker> bash